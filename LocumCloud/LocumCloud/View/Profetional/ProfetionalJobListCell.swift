//
//  ProfetionalJobListCell.swift
//  LocumCloud
//
//  Created by AmulCGM on 15/08/21.
//

import UIKit

class ProfetionalJobListCell: UITableViewCell {

    @IBOutlet weak var vwContainer:UIView!
    var arrColor:[String] = ["#FFFFFF","#B8DD52","#52DDCD","#F0BFFF","#DDAD52","#DDDD52","#DD52C2","#82DD52","#52DD89"]
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        vwContainer.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        vwContainer.backgroundColor = UIColor(hexString: arrColor.randomElement() ?? "#52DDCD")
        // Configure the view for the selected state
    }
    
}
