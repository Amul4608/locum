//
//  TabbarView.swift
//  masterapp
//
//  Created by Jaydeep on 29/09/19.
//  Copyright © 2019 swaminarayanbhagvanapps. All rights reserved.
//

import UIKit

@objc protocol TabDelegate: NSObjectProtocol {

    @objc optional func didClickHome()
    @objc optional func didClickCalendar()
    @objc optional func didCickLike()
    @objc optional func didClickMyTimeSheet()
    @objc optional func didClickProfile()

}


class TabbarView: UIView {


    @IBOutlet weak var btnFavourite:UIButton!
    @IBOutlet weak var btnInvoice:UIButton!
    @IBOutlet weak var btnCalender:UIButton!
    @IBOutlet weak var btnProfile:UIButton!
    @IBOutlet weak var btnHome:UIButton!
    
    weak var delegate: TabDelegate?
    static func CreateTabBarView() -> TabbarView? {
        let arr = Bundle.main.loadNibNamed("TabbarView", owner: self, options: nil)
        if arr != nil {
            if arr!.count > 0 {
                if let view = arr![0] as? TabbarView {
                    if appUser?.data?.role_id == 1{
                        view.btnInvoice.setImage(UIImage(named: "Icon_Tab_TimeSheet.png"), for: .normal)
                        view.btnFavourite.setImage(UIImage(named: "Icon_Tab_Like.png"), for: .normal)
                    }else{
                        view.btnInvoice.setImage(UIImage(named: "invoices-icon.png"), for: .normal)
                        view.btnFavourite.setImage(UIImage(named: "active-jobs.png"), for: .normal)
                    }
                    return view
                }
            }
        }

        return nil
    }

    override func awakeFromNib() {
      
    }

    func resetAll(){
        if appUser?.data?.role_id == 1{
            btnInvoice.setImage(UIImage(named: "Icon_Tab_TimeSheet.png"), for: .normal)
            btnFavourite.setImage(UIImage(named: "Icon_Tab_Like.png"), for: .normal)
        }else{
            btnInvoice.setImage(UIImage(named: "invoices-icon.png"), for: .normal)
            btnFavourite.setImage(UIImage(named: "active-jobs.png"), for: .normal)
        }
        btnHome.setImage(UIImage(named: "home-icon.png"), for: .normal)
        btnProfile.setImage(UIImage(named: "Icon_Tab_Profile.png"), for: .normal)
        btnCalender.setImage(UIImage(named: "Icon_Tab_Calendar.png"), for: .normal)
        
    }
   
    @IBAction func btnTabClicked(_ sender: UIButton) {

        switch sender.tag {
            case 0:
            currentTab = 0
            delegate?.didClickHome?()
        case 1:
            currentTab = 1
            delegate?.didClickCalendar?()
        case 2:
            currentTab = 2
            delegate?.didCickLike?()
        case 3:
            currentTab = 3
            delegate?.didClickMyTimeSheet?()
        case 4:
            currentTab = 4
            delegate?.didClickProfile?()
        default:
            break
        }
    }

}

extension UIImage {

    @available(iOS 10.0, *)
    func colored(_ color: UIColor) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: size)
        return renderer.image { context in
            color.setFill()
            self.draw(at: .zero)
            context.fill(CGRect(x: 0, y: 0, width: size.width, height: size.height), blendMode: .sourceAtop)
        }
    }

}
