//
//  UserInfoCell.swift
//  LocumCloud
//
//  Created by AmulCGM on 15/08/21.
//

import UIKit

class UserInfoCell: UITableViewCell {

    @IBOutlet weak var imgImage:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblRating:UILabel!
    @IBOutlet weak var lblDesc:UILabel!
    @IBOutlet weak var btnProfile:UIButton!
    @IBOutlet weak var imgStatus:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var obj : JobUserModel!{
        didSet{
            if let img = obj.user_image{
                self.imgImage.setImageWithActivity(img, UIActivityIndicatorView.Style.gray)
            }
            self.lblTitle.text = (obj.first_name ?? "") + " " +  (obj.last_name ?? "")
            var arrnm:[String] = []
            if let arr = obj.jobtitle{
            for i in arr{
                arrnm.append(i.job_title ?? "")
            }}
            self.lblDesc.text = arrnm.joined(separator: ",")
            self.lblRating.text = obj.job_review
        
        }
    }
    
}
