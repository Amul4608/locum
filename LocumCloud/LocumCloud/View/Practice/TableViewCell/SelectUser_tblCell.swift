//
//  SelectUser_tblCell.swift
//  AgencyUser
//
//  Created by Amul Patel on 30/11/20.
//  Copyright © 2020 Amul Patel. All rights reserved.
//

import UIKit

class SelectUser_tblCell: UITableViewCell {

    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var imgProfile:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
