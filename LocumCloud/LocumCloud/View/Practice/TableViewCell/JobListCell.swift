//
//  JobListCell.swift
//  LocumCloud
//
//  Created by AmulCGM on 15/08/21.
//

import UIKit

class JobListCell: UITableViewCell {

    @IBOutlet weak var vwContainer:UIView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lblMonth:UILabel!
    @IBOutlet weak var lblStartTime:UILabel!
    @IBOutlet weak var lblEndTime:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var lblApplyed:UILabel!
    @IBOutlet weak var lblShift:UILabel!
    var arrColor:[String] = ["#FFFFFF","#B8DD52","#52DDCD","#F0BFFF"]
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        vwContainer.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        vwContainer.backgroundColor = UIColor(hexString: arrColor.randomElement() ?? "#52DDCD")
        // Configure the view for the selected state
    }
    
    var obj:JobListDataModel!{
        didSet{
            lblTitle.text = (obj.headline ?? "") + " (\(obj.qualification ?? ""))"
            lblAddress.text = obj.location
            lblPrice.text = "£" + (obj.hourly_rate  ?? "")
            lblApplyed.text = "\(obj.total_applied_users ?? 0)"
            lblMonth.text = getDisplayDate(date: obj.date ?? "", dateFormat: "yyyy-MM-dd", displayFormat: "MMM")
            lblDate.text = getDisplayDate(date: obj.date ?? "", dateFormat: "yyyy-MM-dd", displayFormat: "dd")
            lblStartTime.text = getDisplayDate(date: obj.start_time ?? "", dateFormat: "HH:mm:ss", displayFormat: "hh:mm a")
            lblEndTime.text = getDisplayDate(date: obj.end_time ?? "", dateFormat: "HH:mm:ss", displayFormat: "hh:mm a")
            lblShift.text = obj.description
            
        }
    }
    
}
