//
//  ReviewCell.swift
//  LocumCloud
//
//  Created by AmulCGM on 15/08/21.
//

import UIKit

class ReviewCell: UITableViewCell {

    @IBOutlet weak var vwRate:UIView!
    @IBOutlet weak var vwContainer:UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        vwRate.addShadowLikeAndroidWithCorne(cornerRadius: 12.5)
        vwContainer.addShadowLikeAndroidWithCorne(cornerRadius: 12.5)
        // Configure the view for the selected state
    }
    
}
