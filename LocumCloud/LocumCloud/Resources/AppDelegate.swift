//
//  AppDelegate.swift
//  AmulPatel
//
//  Created by Amul Patel on 18/02/21.
//

import UIKit
import SVProgressHUD
import IQKeyboardManagerSwift
import MapKit
import MapKit
import GoogleMaps
import GooglePlaces
import UserNotifications
import UserNotificationsUI
import LocalAuthentication
import Toast_Swift
import Alamofire
var isLockValidate:Bool = false
var latitude_current:Double = 0.0
var longitude_current:Double = 0.0
var isLoginDataSaved:Bool = false
enum UIUserInterfaceIdiom : Int {
    case unspecified
    
    case phone // iPhone and iPod touch style UI
    case pad   // iPad style UI (also includes macOS Catalyst)
}


@main
class AppDelegate: UIResponder, UIApplicationDelegate {


    static let sharedAppDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var window: UIWindow?
    var dicLanguage = NSDictionary()
    var mainNavigationController  = UINavigationController()
    typealias locationBlock = (CLLocation?,NSError?) -> Void
    var locationManager: CLLocationManager = CLLocationManager()
    var currentLocation : CLLocation!
    var lBlock : locationBlock?
    var olderLocation : CLLocation!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Thread.sleep(forTimeInterval: 2.0)
        IQKeyboardManager.shared.enable = true
        cofigSvProgress()
        currntDevice()
        if let nav = self.window?.rootViewController?.navigationController {
            mainNavigationController = nav
        }
        if let user = AppData.shared.getUserInfo(){
            appUser = user
            
        }
        getCurrentLocation { (location, error) in
            //print(location)
            //print(error?.localizedDescription)
        }
        if let isLoginData = UserDefaults.standard.value(forKey: "isLoginDataSaved") as? Bool{
            isLoginDataSaved = isLoginData
        }
        return true
    }

    func cofigSvProgress()
    {
      
        SVProgressHUD.resetOffsetFromCenter()
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setMinimumSize(CGSize(width: 80, height: 80))
        SVProgressHUD.setForegroundColor(UIColor.white)
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setBackgroundColor(UIColor.clear)
        SVProgressHUD.setRingThickness(6)
        SVProgressHUD.setMaximumDismissTimeInterval(1.0)
        SVProgressHUD.setMinimumDismissTimeInterval(1.0)
    }
    
    func currntDevice() {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                IsIphoneX = false
            case 1334:
                print("iPhone 6/6S/7/8")
                IsIphoneX = false
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                IsIphoneX = false
            case 2436:
                print("iPhone X, Xs")
                IsIphoneX = true
            case 2688:
                print("iPhone Xs Max")
                IsIphoneX = true
            case 1792:
                print("iPhone Xr")
                IsIphoneX = true
            default:
                print("unknown")
                IsIphoneX = true
            }
        }
    }
    
   
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
    }
  

}



extension AppDelegate : CLLocationManagerDelegate{
    func getCurrentLocation(_ lBlock : @escaping locationBlock ){
        self.lBlock = lBlock;
        self.LocationUpdate();
    }
    private func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            locationManager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            locationManager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            locationManager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            break
        default:
            break
        }
    }

    
    //Loacation Methods
    func LocationUpdate()
    {
        locationManager.delegate = self
       
        locationManager.delegate = self

        locationManager.requestWhenInUseAuthorization()

        locationManager.desiredAccuracy = kCLLocationAccuracyBest

        locationManager.startUpdatingLocation()

        locationManager.startMonitoringSignificantLocationChanges()

        // Here you can check whether you have allowed the permission or not.

        if CLLocationManager.locationServicesEnabled()
            {
                switch(CLLocationManager.authorizationStatus())
                {

                case .authorizedAlways, .authorizedWhenInUse:
                    print("Authorize.")
                    break
                case .notDetermined:
                    print("Not determined.")
                    break
                case .restricted:
                    print("Restricted.")
                    break
                case .denied:
                    print("Denied.")
                @unknown default:
                    print("Error")
                }
            }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        //let locValue : CLLocationCoordinate2D = manager.location!.coordinate;
        currentLocation = manager.location
        manager.stopUpdatingLocation()
        latitude_current = manager.location?.coordinate.latitude ?? 0.0
        longitude_current  = manager.location?.coordinate.longitude ?? 0.0
        lBlock?(currentLocation,nil);
        lBlock = nil;
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        lBlock?(nil,error as NSError?);
    }
    
}
extension AppDelegate{
    //MARK: - App lock related stuff
    func biometricType() -> BiometricType {
        let authContext = LAContext()
        if #available(iOS 11, *) {
            let _ = authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
            switch(authContext.biometryType) {
            case .none:
                return .none
            case .touchID:
                return .touch
            case .faceID:
                return .face
            }
        } else {
            return authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) ? .touch : .none
        }
    }
    func validateAppLockController(contoller:UIViewController , isTrance:Bool){
        isCreate = false
        var options = ALOptions()
        if biometricType() == .face{
            options.image = #imageLiteral(resourceName: "faceID")
        }else{
            options.image = #imageLiteral(resourceName: "Thumb")
        }
        
        options.title = "Enter pin to unlock the app"
        options.subtitle = ""
        options.isSensorsEnabled = false
        
        options.onSuccessfulDismiss = { (mode: ALMode?) in
            if let mode = mode {
                print("Password \(String(describing: mode))d successfully")
                isLockValidate = true
                if isTrance == true{
                    if let user = AppData.shared.getUserInfo(){
                        appUser = user
                        self.redirectToHome(Vc: contoller)
                    }
                    
                }
                //                    self.redirectToHome()
            } else {
                print("User Cancelled")
            }
        }
        options.onFailedAttempt = { (mode: ALMode?) in
            print("Failed to \(String(describing: mode))")
        }
        AppLocker.present(with: .validate, and: options, over: contoller)
    }
    
    func redirectToHome(Vc:UIViewController){
        UserDefaults.standard.setValue(true, forKey: "isLoginDataSaved")
        if let user = AppData.shared.getUserInfo(){
            appUser = user
            if let id = appUser?.data?.role_id{
                if id == 2{
                    let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: PracticeDashboardVC.className) as! PracticeDashboardVC
                    _ = Vc.navigationController?.pushViewController(nextVc, animated: true)
                }else{
                    let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.ProfetionalDashboardStoryBord, identifire: ProfetionalDashboardVC.className) as! ProfetionalDashboardVC
                    _ = Vc.navigationController?.pushViewController(nextVc, animated: true)
                }
            }
        }
    }
    
    //MARK: - Change app lock
    func ChangeAppLockController(contoller:UIViewController ,res:[String:Any] ){
        isCreate = true
        var options = ALOptions()
        if biometricType() == .face{
            options.image = #imageLiteral(resourceName: "faceID")
        }else{
            options.image = #imageLiteral(resourceName: "fingure-print")
        }
            options.title = "Enter pin to unlock the app"
            options.subtitle = ""
            options.isSensorsEnabled = false
            options.onSuccessfulDismiss = { (mode: ALMode?) in
                if let mode = mode {
                    print("Password \(String(describing: mode))d successfully")
                    do {
                        let pin  = try AppLocker.valet.string(forKey: ALConstants.kPincode)
                        //self.userPinApi()
                        
                        print(pin)
                        isLockValidate = true
                        AppData.shared.saveUserInfo(res)
                        
                        if let user = AppData.shared.getUserInfo(){
                            appUser = user
                            if let email = appUser?.data?.email{
                                self.setuserPIN(pin: pin, email: email, VC: contoller )
                            }
                            if let id = appUser?.data?.role_id{
                                if id == 1{
                                    let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.ProfetionalDashboardStoryBord, identifire: ProfetionalDashboardVC.className) as! ProfetionalDashboardVC
                                    _ = contoller.navigationController?.pushViewController(nextVc, animated: true)
                                }else{
                                    let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: PracticeDashboardVC.className) as! PracticeDashboardVC
                                    _ = contoller.navigationController?.pushViewController(nextVc, animated: true)
                                }
                            }
                        }
                        AppData.hideProgress()
                    }
                    catch _ {
                       // self.userPin = ""
                    }
                } else {
                    print("User Cancelled")
                }
            }
            options.onFailedAttempt = { (mode: ALMode?) in
                print("Failed to \(String(describing: mode))")
            }

        AppLocker.present(with: .create, and: options, over: contoller)
    }
    
    func setuserPIN(pin:String , email:String , VC:UIViewController){
        let url = WebService.createURLForWebService(WebService.user_secret_code)
        var param:Parameters  = [:]
        param["email"] = email
        param["secret_code"] = pin
        
        NetworkManager.showNetworkLog()
         _ = NetworkManager.shared.requestWithMethodType(withObject: .post, url: url, parameters: param,isToken: false, successHandler: { (response) in
             DispatchQueue.main.async(execute: {
                 print("response \(response)")
                 
                 AppData.hideProgress()
             })
         }) { (errorString) in
            AppData.hideProgress()
            VC.view.makeToast(errorString)
         }
        
        
    }
    
    
}
