//
//  AppALConstants.swift
//  AppLocker
//
//  Created by Oleg Ryasnoy on 07.07.17.
//  Copyright © 2017 Oleg Ryasnoy. All rights reserved.
//
import UIKit
import AudioToolbox
import LocalAuthentication
import Valet
import Toast_Swift
var isCreate:Bool = false
enum BiometricType {
    case none
    case touch
    case face
}

public enum ALConstants {
    static let nibName = "AppLocker"
    static let kPincode = "pincode" // Key for saving pincode to keychain
    static let kLocalizedReason = "Unlock with sensor" // Your message when sensors must be shown
    static let duration = 0.3 // Duration of indicator filling
    static let maxPinLength = 4
    
    enum button: Int {
        case delete = 1000
        case cancel = 1001
    }
}

public typealias onSuccessfulDismissCallback = (_ mode: ALMode?) -> () // Cancel dismiss will send mode as nil
public typealias onFailedAttemptCallback = (_ mode: ALMode) -> ()
public struct ALOptions { // The structure used to display the controller
    public var title: String?
    public var subtitle: String?
    public var image: UIImage?
    public var color: UIColor?
    public var isSensorsEnabled: Bool?
    
    public var onSuccessfulDismiss: onSuccessfulDismissCallback?
    public var onFailedAttempt: onFailedAttemptCallback?
    public init() {}
}

public enum ALMode { // Modes for AppLocker
    case validate
    case change
    case deactive
    case create
}

public class AppLocker: UIViewController {
    
    
    
    // MARK: - Top view
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var submessageLabel: UILabel!
    @IBOutlet var pinIndicators: [Indicator]!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var HeightForForgotPin: NSLayoutConstraint!
    @IBOutlet weak var viewForgotPin: UIView!
    
    static let valet = Valet.valet(with: Identifier(nonEmpty: "Druidia")!, accessibility: .whenUnlockedThisDeviceOnly)
    // MARK: - Pincode
    private var onSuccessfulDismiss: onSuccessfulDismissCallback?
    private var onFailedAttempt: onFailedAttemptCallback?
    private let context = LAContext()
    private var pin = "" // Entered pincode
    private var reservedPin = "" // Reserve pincode for confirm
    private var isFirstCreationStep = true
    public var savedPin: String? {
        get {
            do {
                return try AppLocker.valet.string(forKey: ALConstants.kPincode)
            }
            catch _ {
                return ""
            }
        }
        set {
            guard let newValue = newValue else { return }
            do {
                try AppLocker.valet.setString(newValue, forKey: ALConstants.kPincode)
            }
            catch _ {
                
            }
        }
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        // https://stackoverflow.com/questions/56459329/disable-the-interactive-dismissal-of-presented-view-controller-in-ios-13
        modalPresentationStyle = .fullScreen
        if isCreate == false{
            matchLock()
        }
    }
    func matchLock(){
        let myContext = LAContext()
        let myLocalizedReasonString = "Biometric Authntication testing !! "
        
        var authError: NSError?
        if #available(iOS 8.0, macOS 10.12.1, *) {
            if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { success, evaluateError in
                    
                    DispatchQueue.main.async {
                        if success {
                            // User authenticated successfully, take appropriate action
                            //self.view.makeToast("Awesome!!... User authenticated successfully")
                            isLockValidate = true
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FirstLoginVC"), object: nil, userInfo:["Isavail":false])
                            self.dismiss(animated: true, completion: nil)
                            
                        } else {
                            // User did not authenticate successfully, look at error and take appropriate action
                           // self.view.makeToast("Sorry!!... User did not authenticate successfully")
                        }
                    }
                }
            } else {
                // Could not evaluate policy; look at authError and present an appropriate message to user
                //self.view.makeToast("Sorry!!.. Could not evaluate policy.")
            }
        } else {
            // Fallback on earlier versions
           // self.view.makeToast("Ooops!!.. This feature is not supported.")
        }
    }
    func biometricType() -> BiometricType {
        let authContext = LAContext()
        if #available(iOS 11, *) {
            let _ = authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
            switch(authContext.biometryType) {
            case .none:
                return .none
            case .touchID:
                return .touch
            case .faceID:
                return .face
            }
        } else {
            return authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) ? .touch : .none
        }
    }
    
    fileprivate var mode: ALMode = .validate {
        didSet {
            switch mode {
            case .create:
                submessageLabel.text = "Create your passcode" // Your submessage for create mode
                HeightForForgotPin.constant = 0.0
                viewForgotPin.isHidden = true
                
            case .change:
                submessageLabel.text = "Enter your passcode" // Your submessage for change mode
                HeightForForgotPin.constant = 0.0
                viewForgotPin.isHidden = true

            case .deactive:
                submessageLabel.text = "Enter your passcode" // Your submessage for deactive mode
                HeightForForgotPin.constant = 0.0
                viewForgotPin.isHidden = true

            case .validate:
                submessageLabel.text = "Enter your passcode" // Your submessage for validate mode
                cancelButton.isHidden = true
                isFirstCreationStep = false
//                HeightForForgotPin.constant = 0.0
//                viewForgotPin.isHidden = true
                HeightForForgotPin.constant = 40.0
                viewForgotPin.isHidden = false
            }
        }
    }
    
    private func precreateSettings () { // Precreate settings for change mode
        mode = .create
        clearView()
    }
    
    private func drawing(isNeedClear: Bool, tag: Int? = nil) { // Fill or cancel fill for indicators
        let results = pinIndicators.filter { $0.isNeedClear == isNeedClear }
        let pinView = isNeedClear ? results.last : results.first
        pinView?.isNeedClear = !isNeedClear
        
        UIView.animate(withDuration: ALConstants.duration, animations: {
            pinView?.backgroundColor = isNeedClear ? .clear : .white
        }) { _ in
            isNeedClear ? self.pin = String(self.pin.dropLast()) : self.pincodeChecker(tag ?? 0)
        }
    }
    
    private func pincodeChecker(_ pinNumber: Int) {
        if pin.count < ALConstants.maxPinLength {
            pin.append("\(pinNumber)")
            if pin.count == ALConstants.maxPinLength {
                switch mode {
                case .create:
                    createModeAction()
                case .change:
                    changeModeAction()
                case .deactive:
                    deactiveModeAction()
                case .validate:
                    validateModeAction()
                }
            }
        }
    }
    
    @IBAction func btnForgotPinClicked(_ sender: Any) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        //let controller  = storyboard.instantiateViewController(withIdentifier: "ForgotPinViewController") as! ForgotPinViewController
        //controller.modalPresentationStyle = .fullScreen
        //self.present(controller, animated: false, completion: nil)
//        self.navigationController?.pushViewController(controller!, animated: true)
    }
    
    
    // MARK: - Modes
    private func createModeAction() {
        if isFirstCreationStep {
            isFirstCreationStep = false
            reservedPin = pin
            clearView()
            submessageLabel.text = "Confirm your pincode"
        } else {
            confirmPin()
        }
    }
    
    private func changeModeAction() {
        if pin == savedPin {
            precreateSettings()
        } else {
            onFailedAttempt?(mode)
            incorrectPinAnimation()
        }
    }
    
    private func deactiveModeAction() {
        if pin == savedPin {
            removePin()
        } else {
            onFailedAttempt?(mode)
            incorrectPinAnimation()
        }
    }
    
    private func validateModeAction() {
        if pin == savedPin {
            isLockValidate = true
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FirstLoginVC"), object: nil, userInfo:["Isavail":false])
            self.dismiss(animated: true, completion: nil)
        } else {
            onFailedAttempt?(mode)
            incorrectPinAnimation()
        }
    }
    
    private func removePin() {
        do {
            try AppLocker.valet.removeObject(forKey: ALConstants.kPincode)
        }
        catch _ {
            
        }
        dismiss(animated: true) {
            self.onSuccessfulDismiss?(self.mode)
        }
    }
    
    private func confirmPin() {
        if pin == reservedPin {
            savedPin = pin
            dismiss(animated: true) {
                self.onSuccessfulDismiss?(self.mode)
            }
        } else {
            onFailedAttempt?(mode)
            incorrectPinAnimation()
        }
    }
    
    private func incorrectPinAnimation() {
        pinIndicators.forEach { view in
            view.shake(delegate: self)
            view.backgroundColor = .clear
        }
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    fileprivate func clearView() {
        pin = ""
        pinIndicators.forEach { view in
            view.isNeedClear = false
            UIView.animate(withDuration: ALConstants.duration, animations: {
                view.backgroundColor = .clear
            })
        }
    }
    
    // MARK: - Touch ID / Face ID
    fileprivate func checkSensors() {
        if case .validate = mode {} else { return }
        
        var policy: LAPolicy = .deviceOwnerAuthenticationWithBiometrics // iOS 8+ users with Biometric and Custom (Fallback button) verification
        
        // Depending the iOS version we'll need to choose the policy we are able to use
        if #available(iOS 9.0, *) {
            // iOS 9+ users with Biometric and Passcode verification
            policy = .deviceOwnerAuthentication
        }
        
        var err: NSError?
        // Check if the user is able to use the policy we've selected previously
        guard context.canEvaluatePolicy(policy, error: &err) else {return}
        
        // The user is able to use his/her Touch ID / Face ID 👍
        context.evaluatePolicy(policy, localizedReason: ALConstants.kLocalizedReason, reply: {  success, error in
            if success {
                DispatchQueue.main.async { [weak self] in
                    guard let `self` = self else { return }
                    self.dismiss(animated: false) {
                        self.onSuccessfulDismiss?(self.mode)
                    }
                }
            }
        })
    }
    
    // MARK: - Keyboard
    @IBAction func keyboardPressed(_ sender: UIButton) {
        switch sender.tag {
        case ALConstants.button.delete.rawValue:
            drawing(isNeedClear: true)
        case ALConstants.button.cancel.rawValue:
            clearView()
            dismiss(animated: true) {
                self.onSuccessfulDismiss?(nil)
            }
        default:
            drawing(isNeedClear: false, tag: sender.tag)
        }
    }
    
}

// MARK: - CAAnimationDelegate
extension AppLocker: CAAnimationDelegate {
    public func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        clearView()
    }
}

// MARK: - Present
public extension AppLocker {
    // Present AppLocker
    class func present(with mode: ALMode, and config: ALOptions? = nil, over viewController: UIViewController? = nil) {
        let vc = viewController ?? UIApplication.shared.keyWindow?.rootViewController
        guard let root = vc,
            
            let locker = Bundle(for: self.classForCoder()).loadNibNamed(ALConstants.nibName, owner: self, options: nil)?.first as? AppLocker else {
                return
        }
        locker.messageLabel.text = config?.title ?? ""
        locker.submessageLabel.text = config?.subtitle ?? ""
        locker.view.backgroundColor = config?.color ?? .black
        locker.mode = mode
        locker.onSuccessfulDismiss = config?.onSuccessfulDismiss
        locker.onFailedAttempt = config?.onFailedAttempt
        
        if config?.isSensorsEnabled ?? false {
            locker.checkSensors()
        }
        
        
        if let image = config?.image {
            locker.photoImageView.image = image
        } else {
            locker.photoImageView.isHidden = true
        }
        
        root.modalPresentationStyle = .fullScreen
        root.present(locker, animated: false, completion: nil)
    }
}
