//
//  BaseViewController.swift
//  masterapp
//
//  Created by Jaydeep on 28/09/19.
//  Copyright © 2019 swaminarayanbhagvanapps. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, TabDelegate {

    
    @IBOutlet weak var bottomofView: NSLayoutConstraint!
    
    @IBInspectable var showTabButton: Bool = true
    
    
    public var tab: TabbarView?

   
    var hight: CGFloat = 0.0

    var viewController: UIViewController?

    override func awakeFromNib() {
        super.awakeFromNib()
        

        if let tabBarView = TabbarView.CreateTabBarView() {
            tabBarView.delegate = self
            tab = tabBarView
        }

        AppDelegate.sharedAppDelegate.currntDevice()
    }

    override func viewDidLayoutSubviews() {
        tab?.frame = CGRect(x: 0, y: self.view.frame.height - hight, width: self.view.frame.width, height: hight)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        hight = IsIphoneX == true ? 82:63
        self.viewDidLayoutSubviews()
        self.navigationController?.isNavigationBarHidden = true
        isTabbarNotAvail(showTabButton)
    }

    func setUPTab() {
        if tab == nil {
            tab?.frame = CGRect(x: 0, y: self.view.frame.height - hight, width: self.view.frame.width, height: hight)
            tab?.shadow = true
            
            self.view.addSubview(tab!)
            if appUser?.data?.role_id == 1{
                tab?.btnInvoice.setImage(UIImage(named: "Icon_Tab_TimeSheet.png"), for: .normal)
                tab?.btnFavourite.setImage(UIImage(named: "Icon_Tab_Like.png"), for: .normal)
            }else{
                tab?.btnInvoice.setImage(UIImage(named: "invoices-icon.png"), for: .normal)
                tab?.btnFavourite.setImage(UIImage(named: "active-jobs.png"), for: .normal)
            }
        }
    }

    func isTabbarNotAvail( _ isHidden: Bool) {
        if tab != nil {
            if isHidden == true {
                bottomofView?.constant =  hight
                tab?.frame = CGRect(x: 0, y: self.view.frame.height - hight, width: self.view.frame.width, height: hight)
                self.view.addSubview(tab!)
            } else {
                bottomofView?.constant = 0
                self.tab?.removeFromSuperview()
            }
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

   
    func didCickLike() {
        
        if appUser?.data?.role_id == 1{
            _ = self.manageViewController(storyBoardName: StoryBoardName.ProfetionalDashboardStoryBord, viewControllerClass: MyLikeJobListVC.className)
        }else{
            _ = self.manageViewController(storyBoardName: StoryBoardName.PracticeDashboardStoryBord, viewControllerClass: ActiveJobVC.className)
        }
    }
    
    func didClickCalendar() {
        _ = self.manageViewController(storyBoardName: StoryBoardName.ProfetionalDashboardStoryBord, viewControllerClass: ProfetionalCalendarVC.className)
    }
    func didClickProfile() {
       
        if appUser?.data?.role_id == 1{
            _ = self.manageViewController(storyBoardName: StoryBoardName.ProfetionalDashboardStoryBord, viewControllerClass: ProfetionalProfileVC.className)
        }else{
            _ = self.manageViewController(storyBoardName: StoryBoardName.PracticeDashboardStoryBord, viewControllerClass: PrecticeMyProfileVC.className)
        }
    }
    func didClickMyTimeSheet() {
        
        
        if appUser?.data?.role_id == 1{
            _ = self.manageViewController(storyBoardName: StoryBoardName.ProfetionalDashboardStoryBord, viewControllerClass: ProfetionalInvoiceVC.className)
        }else{
            _ = self.manageViewController(storyBoardName: StoryBoardName.PracticeDashboardStoryBord, viewControllerClass: InvoiceVc.className)
        }
    }

    func didClickHome() {
        if appUser?.data?.role_id == 1{
        _ = self.manageViewController(storyBoardName: StoryBoardName.ProfetionalDashboardStoryBord, viewControllerClass: ProfetionalDashboardVC.className)
        }else{
            _ = self.manageViewController(storyBoardName: StoryBoardName.PracticeDashboardStoryBord, viewControllerClass: PracticeDashboardVC.className)
        }
    }

    
    // ==================================================================
    // MARK: - ManageViewController   Code Here
    // ==================================================================
    func manageViewController(storyBoardName: String, viewControllerClass: String) -> UIViewController? {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController.className == viewControllerClass {
                self.navigationController!.popToViewController(aViewController, animated: false)
                return aViewController
            }
        }
        let viewController = Utilities.getViewControllerFromStoryboard(storyBoardName, identifire: viewControllerClass)
        self.navigationController?.pushViewController(viewController, animated: false)
        return viewController
    }

    
}

