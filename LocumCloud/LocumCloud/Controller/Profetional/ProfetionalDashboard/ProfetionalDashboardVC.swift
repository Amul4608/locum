//
//  ProfetionalDashboardVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 15/08/21.
//

import UIKit

class ProfetionalDashboardVC: BaseViewController {

    @IBOutlet weak var clwBanner:UICollectionView!
    @IBOutlet weak var tblList:UITableView!
    @IBOutlet weak var heightOFTbl:NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        if isLockValidate == false{
        
            AppDelegate.sharedAppDelegate.validateAppLockController(contoller: self, isTrance: false)
        }
        setupUI()
    }
    
    func setupUI(){
        clwBanner.register(UINib(nibName: "BannerCell", bundle: nil), forCellWithReuseIdentifier: "BannerCell")
        tblList.register(UINib(nibName: ProfetionalJobListCell.className, bundle: nil), forCellReuseIdentifier: ProfetionalJobListCell.className)
        //Get device width
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = clwBanner.frame.width
        
        //set section inset as per your requirement.
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        //set cell item size here
        layout.itemSize = CGSize(width: Int(width) , height: 120)
        
        //set Minimum spacing between 2 items
        layout.minimumInteritemSpacing = 0
        
        //set minimum vertical line spacing here between two lines in collectionview
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        
        //apply defined layout to collectionview
        clwBanner!.collectionViewLayout = layout
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        heightOFTbl.constant = tblList.contentSize.height
    }
    
    
    

}

extension ProfetionalDashboardVC : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfetionalJobListCell.className) as! ProfetionalJobListCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 122
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.ProfetionalDashboardStoryBord, identifire: ProfetinalJobDetailVC.className) as! ProfetinalJobDetailVC
        _ = self.navigationController?.pushViewController(nextVc, animated: true)
    }
}

extension ProfetionalDashboardVC : UICollectionViewDataSource , UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
        
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BannerCell.className, for: indexPath) as! BannerCell
       
        return cell
       }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        let cellsize = CGSize(width: collectionView.frame.width , height:120)
        return cellsize
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }

}
