//
//  Profational_RegisterOneVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 16/08/21.
//

import UIKit
import Alamofire

class Profational_RegisterOneVC: UIViewController {

    @IBOutlet weak var vwFirstName:UIView!
    @IBOutlet weak var vwLastName:UIView!
    @IBOutlet weak var btnContinue:UIButton!
    @IBOutlet weak var txtFirstName:UITextField!
    @IBOutlet weak var txtLastName:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI(){
        vwFirstName.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        vwLastName.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        btnContinue.addShadowLikeAndroidWithCorne(cornerRadius: 10)
    }
    
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnContinueTapped(_ sender:UIButton){
        if validate(){
            var DicParam:Parameters = [:]
            DicParam["first_name"] = txtFirstName.text!
            DicParam["last_name"] = txtLastName.text!
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.ProfetionalRegisterStoryBord, identifire: Profational_RegisterTwoVC.className) as! Profational_RegisterTwoVC
            nextVc.DicParam = DicParam
        _ = self.navigationController?.pushViewController(nextVc, animated: true)
        }
    }
    
    
    func validate() -> Bool{
        if txtFirstName.text?.isEmpty == true{
            self.view.makeToast("Please enter first name")
            return false
        }else if txtLastName.text?.isEmpty == true{
            self.view.makeToast("Please enter last name")
            return false
        }
        return true
    }

}
extension Profational_RegisterOneVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtFirstName{
            txtLastName.becomeFirstResponder()
            txtFirstName.resignFirstResponder()
        }else if textField == txtLastName{
            txtLastName.resignFirstResponder()
            self.view.endEditing(true)
        }
        return true
    }
}
