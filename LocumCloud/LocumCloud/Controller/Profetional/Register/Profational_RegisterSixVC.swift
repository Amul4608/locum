//
//  Profational_RegisterSixVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 16/08/21.
//

import UIKit
import MobileCoreServices
import AVFoundation
import AssetsLibrary
import MediaPlayer
import AVKit
import Alamofire
import ObjectMapper

enum ProfetinalPickerType: String {
    case GCD          = "CGD"
    case MyCert       = "MyCert"
    case DBS          = "DBS"
    case PhotoID      = "PhotoID"
    case PhotoIDForm  = "PhotoIDForm"
    case BloodReport  = "BloodReport"
    case CV           = "CV"
}

class Profational_RegisterSixVC: UIViewController {
    
    @IBOutlet weak var vwGCD:UIView!
    @IBOutlet weak var vwCert:UIView!
    @IBOutlet weak var vwDBS:UIView!
    @IBOutlet weak var vwPhotoID:UIView!
    @IBOutlet weak var vwPhotoAddress:UIView!
    @IBOutlet weak var vwBloodReport:UIView!
    @IBOutlet weak var vwCV:UIView!
    @IBOutlet weak var vwRefrence:UIView!
    @IBOutlet weak var btnContinue:UIButton!
    
    @IBOutlet weak var txtName:UITextField!
    @IBOutlet weak var txtContact:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    
    @IBOutlet weak var txtName1:UITextField!
    @IBOutlet weak var txtContact1:UITextField!
    @IBOutlet weak var txtEmail1:UITextField!
    
    var DicParam:Parameters = [:]
    var GCDName:String = ""
    var MyCrtName:String = ""
    var DBSName:String = ""
    var PhototIDName:String = ""
    var PhotoIDFormName:String = ""
    var BloodReportName:String = ""
    var CVName:String = ""
    var arrAttechment:[attachmentModel] = []
    @IBOutlet weak var btnGCD:UIImageView!
    @IBOutlet weak var btnMyCrt:UIImageView!
    @IBOutlet weak var btnDBS:UIImageView!
    @IBOutlet weak var btnPhotoID:UIImageView!
    @IBOutlet weak var btnPhotoIDNAme:UIImageView!
    @IBOutlet weak var btnBloodReport:UIImageView!
    @IBOutlet weak var btnCVNAme:UIImageView!
    
    @IBOutlet weak var txtGCD:UITextField!
    @IBOutlet weak var txtMyCrt:UITextField!
    @IBOutlet weak var txtDBS:UITextField!
    @IBOutlet weak var txtPhotoID:UITextField!
    @IBOutlet weak var txtPhotoIDNAme:UITextField!
    @IBOutlet weak var txtBloodReport:UITextField!
    @IBOutlet weak var txtCVNAme:UITextField!
    
    //
    let imagepicker = UIImagePickerController()
    var currntPicker:ProfetinalPickerType = .GCD
    var isFromProfile:Bool  = false
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI(){
        vwGCD.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        vwCert.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        vwDBS.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        vwPhotoID.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        vwPhotoAddress.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        vwBloodReport.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        vwCV.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        vwRefrence.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        btnContinue.addShadowLikeAndroidWithCorne(cornerRadius: 10)
    }
    
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnContinueTapped(_ sender:UIButton){
        if isFromProfile == false{
            if isValidate() {
                DicParam["reference_name_1"] = txtName.text!
                DicParam["reference_email_1"] = txtEmail.text!
                DicParam["reference_contact_1"] = txtContact.text!
                DicParam["reference_name_2"] = txtName1.text!
                DicParam["reference_contact_2"] = txtContact1.text!
                DicParam["reference_email_2"] = txtEmail1.text!
                DicParam["role_id"] = "1"
                let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.MainStoryBord, identifire: RegisterFourVC.className) as! RegisterFourVC
                nextVc.DicParam = DicParam
                nextVc.arrAttechment = self.arrAttechment
                _ = self.navigationController?.pushViewController(nextVc, animated: true)
            }
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    
    @IBAction func btnAttchmentTapped(_ sender:UIButton){
        if sender.tag == 0{
            currntPicker = .GCD
        }else if sender.tag == 1{
            currntPicker = .MyCert
        }else if sender.tag == 2{
            currntPicker = .DBS
        }else if sender.tag == 3{
            currntPicker = .PhotoID
        }else if sender.tag == 4{
            currntPicker = .PhotoIDForm
        }else if sender.tag == 5{
            currntPicker = .BloodReport
        }else{
            currntPicker = .CV
        }
        let attchOPT:[String] = ["Camera" , "Photo Library" ]
        if currntPicker == .PhotoID{
            AlertView.showActionSheetWithCancelButton(true, title: "Choose Option", buttons: attchOPT, viewcontroller: self) { btn in
                if btn == 0{
                    self.showImage(type: "Camera", isProof: sender.tag == 0 ? false : true ,animation: true)
                }else if btn == 1{
                    self.showImage(type: "photoLibrary", isProof: sender.tag == 0 ? false : true, animation: true)
                }
            }
        }else{
            let importMenu = UIDocumentPickerViewController(documentTypes: ["com.microsoft.word.doc","org.openxmlformats.wordprocessingml.document", kUTTypePDF as String], in: UIDocumentPickerMode.import)
            importMenu.delegate = self
            //currntDoc = .QualiCerti
            self.present(importMenu, animated: true, completion: nil)
        }
        
    }
    
}


extension Profational_RegisterSixVC : UIDocumentPickerDelegate ,UINavigationControllerDelegate{
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
       
        if urls[0].lastPathComponent.components(separatedBy: ".").last?.lowercased() ?? "pdf" == "pdf"{
            let fileData = try! Data.init(contentsOf: urls[0])
            self.documentPickedSet(fileData, pathExt: urls[0].pathExtension, type: "pdf")
        }else if urls[0].lastPathComponent.components(separatedBy: ".").last?.lowercased() ?? "doc" == "doc"{
            let fileData = try! Data.init(contentsOf: urls[0])
            self.documentPickedSet(fileData, pathExt: urls[0].pathExtension, type: "doc")
            
        }
        
    }
    
    func documentPickedSet(_ urlData:Data , pathExt:String , type:String){
        var key:String = "gdc_reg_file"
        var name:String = "GCD_Report_2021.png"
        switch currntPicker{
        case .GCD:
            key = "gdc_reg_file"
            name = "GCD_Report_2021.\(pathExt)"
            txtGCD.text = "GCD_Report_2021.\(pathExt)"
            btnGCD.image = UIImage(named: "icon_RightGreen.png")
            break
        case .MyCert:
            key = "identity_certificate"
            name = "Identity_Certificate_2021.\(pathExt)"
            txtMyCrt.text = "Identity_Certificate_2021.\(pathExt)"
            btnMyCrt.image = UIImage(named: "icon_RightGreen.png")
            break
        case .DBS:
            key = "current_dbs"
            name = "Current_DBS_2021.\(pathExt)"
            txtDBS.text = "Current_DBS_2021.\(pathExt)"
            btnDBS.image = UIImage(named: "icon_RightGreen.png")
            break
        case .PhotoID:
            key = "photo_id"
            name = "Photo_Id_2021.\(pathExt)"
            txtPhotoID.text = "Photo_Id_2021.\(pathExt)"
            btnPhotoID.image = UIImage(named: "icon_RightGreen.png")
            break
        case .PhotoIDForm:
            key = "address_proof"
            name = "Address_Proof_2021.\(pathExt)"
            txtPhotoIDNAme.text = "Address_Proof_2021.\(pathExt)"
            btnPhotoIDNAme.image = UIImage(named: "icon_RightGreen.png")
            break
        case .BloodReport:
            key = "blood_report"
            name = "Blood_Report_2021.\(pathExt)"
            txtBloodReport.text = "Blood_Report_2021.\(pathExt)"
            btnBloodReport.image = UIImage(named: "icon_RightGreen.png")
            break
        case .CV:
            key = "cv_resume"
            name = "CV_Rseume_2021.\(pathExt)"
            txtCVNAme.text = "CV_Rseume_2021.\(pathExt)"
            btnCVNAme.image = UIImage(named: "icon_RightGreen.png")
            break
        }
        let ard = arrAttechment.filter({$0.key == key})
        let obj = attachmentModel(name: name, type: type, key: key, val: urlData)
        if ard.count > 0{
            if let fIndex = arrAttechment.firstIndex(where: {$0.key == key}){
                arrAttechment[fIndex] = obj
            }
        }else{
            
            arrAttechment.append(obj)
        }
        
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
    }
    
}
//MARK:- Add Attechment
extension  Profational_RegisterSixVC :   UIImagePickerControllerDelegate{
    
    func showImage(type:String , isProof:Bool , animation:Bool){
        
        DispatchQueue.main.async
        {
            self.imagepicker.delegate = self
            self.imagepicker.mediaTypes = ["public.image"]
            self.imagepicker.allowsEditing = false
            if type != "photoLibrary"{
                self.imagepicker.sourceType = .camera
                self.imagepicker.cameraCaptureMode = .photo
            }else{
                self.imagepicker.sourceType = .photoLibrary
            }
            self.present(self.imagepicker, animated: animation, completion: nil)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Code here
        if info[.mediaType] as? String == "public.image"{
            if let selectedImage = info[.originalImage] as? UIImage{
                let data = selectedImage.jpegData(compressionQuality: 0.5)! as Data
                self.documentPickedSet(data, pathExt: "png", type: "image")
                dismiss(animated: true, completion: nil)
            }
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func isValidate() -> Bool{
        if txtGCD.text?.isEmpty == true{
            self.view.makeToast("Please add your  GCD Registration Document or Photo.")
            return false
        }else  if txtMyCrt.text?.isEmpty == true{
            self.view.makeToast("Please provide My Indemnity Certificate Document or Photo..")
            return false
        }else  if txtDBS.text?.isEmpty == true{
            self.view.makeToast("Please select Current DBS  Document or Photo.")
            return false
        }else  if txtPhotoID.text?.isEmpty == true{
            self.view.makeToast("A Form of Photo with Name and Address Document or Photo.")
            return false
        }else  if txtPhotoIDNAme.text?.isEmpty == true{
            self.view.makeToast("Please provide A Form of Photo ID Document or Photo.")
            return false
        }else  if txtCVNAme.text?.isEmpty == true{
            self.view.makeToast("Please Upload  CV  Document or Photo.")
            return false
        }else  if txtName.text?.isEmpty == true{
            self.view.makeToast("Please enter refrance 1 name.")
            return false
        }else  if txtEmail.text?.isEmpty == true{
            self.view.makeToast("Please enter refrance 1 email address.")
            return false
        }else  if txtEmail.text?.isValidEmailAddress() == false{
            self.view.makeToast("Please enter refrance 1 valid email address.")
            return false
        }else  if txtContact.text?.isEmpty == true{
            self.view.makeToast("Please enter refrance 1 contact number.")
            return false
        }else  if txtName1.text?.isEmpty == true{
            self.view.makeToast("Please enter refrance 2 name.")
            return false
        }else  if txtEmail1.text?.isEmpty == true{
            self.view.makeToast("Please enter refrance 2 email address.")
            return false
        }else  if txtEmail1.text?.isValidEmailAddress() == false{
            self.view.makeToast("Please enter refrance 2 valid email address.")
            return false
        }else  if txtContact1.text?.isEmpty == true{
            self.view.makeToast("Please enter refrance 2 contact number.")
            return false
        }
        return true
    }
    
    
    
}

