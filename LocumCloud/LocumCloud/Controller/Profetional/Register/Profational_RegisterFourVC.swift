//
//  Profational_RegisterFourVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 16/08/21.
//

import UIKit
import Alamofire
import ObjectMapper

class Profational_RegisterFourVC: UIViewController {
    @IBOutlet weak var vwJobTitle:UIView!
    @IBOutlet weak var vwJobType:UIView!
    @IBOutlet weak var vwQualification:UIView!
    @IBOutlet weak var vwExperience:UIView!
    var DicParam:Parameters = [:]
    var arrJobTitle:[JobTitleDataModel] = []
    var arrJobType:[JobTypeDataModel] = []
    var selectedJobTitle:JobTitleDataModel?
    var selectedJobType:JobTypeDataModel?
    var currntJobPickerType:JobPickerType = .JobTitle
    @IBOutlet weak var txtJobTitle:UITextField!
    @IBOutlet weak var txtJobType:UITextField!
    @IBOutlet weak var txtQualification:UITextField!
    @IBOutlet weak var txtExperience:UITextField!
    var picker: UIPickerView!
    @IBOutlet weak var btnContinue:UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        api_JobTitle()
        api_JobType()
        // Do any additional setup after loading the view.
    }
    
    func setupUI(){
        vwJobTitle.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        vwJobType.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        vwQualification.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        vwExperience.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        btnContinue.addShadowLikeAndroidWithCorne(cornerRadius: 10)
    }
    
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnContinueTapped(_ sender:UIButton){
        if validate(){
            DicParam["job_title"] = "\(selectedJobTitle?.id ?? 0)"
            DicParam["job_type"] = "\(selectedJobType?.id ?? 0)"
            DicParam["qualification"] = txtQualification.text!
            DicParam["experience"] = txtExperience.text!
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.ProfetionalRegisterStoryBord, identifire: Profational_RegisterFiveVC.className) as! Profational_RegisterFiveVC
            nextVc.DicParam = self.DicParam
        _ = self.navigationController?.pushViewController(nextVc, animated: true)
        }
    }

    func validate() -> Bool{
       if txtJobTitle.text?.isEmpty == true{
            self.view.makeToast("Please select job title")
            return false
        }else if txtJobType.text?.isEmpty == true{
            self.view.makeToast("Please select job type")
            return false
        }else if txtQualification.text?.isEmpty == true{
            self.view.makeToast("Please enter qualification")
            return false
        }else if txtExperience.text?.isEmpty == true{
            self.view.makeToast("Please enter experience")
            return false
        }
        return true
    }

}
extension Profational_RegisterFourVC : UITextFieldDelegate{
    
  
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
         if textField == txtJobType{
            currntJobPickerType = .JobType
            showLPicker(label_title: "Select Job Type", textfiled: txtJobType)
        }else if textField == txtJobTitle{
            currntJobPickerType = .JobTitle
            showLPicker(label_title: "Select Job Title", textfiled: txtJobTitle)
        }
    }
}


extension Profational_RegisterFourVC : UIPickerViewDelegate  , UIPickerViewDataSource {
    func showLPicker(label_title: String ,textfiled: UITextField)
    {
        
        // let label_title = "Amul"
        picker = UIPickerView(frame: CGRect(x: 0, y: 200, width: self.view.bounds.width, height: 216))
        picker.backgroundColor = .white
        picker.delegate = self
        picker.dataSource = self
        
        var toolbar = UIToolbar()
        let rect = CGRect(origin: CGPoint(x: 0,y :self.view.frame.height/6), size: CGSize(width: self.view.frame.size.width, height: 40))
        toolbar = UIToolbar(frame: rect)
        toolbar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
        toolbar.isTranslucent = false
        toolbar.barStyle = UIBarStyle.default
        toolbar.tintColor = UIColor.white
        toolbar.barTintColor = UIColor(red: 105 / 255.0, green: 105 / 255.0, blue: 105 / 255.0, alpha: 1.0)
        
        let returnbutton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.cancel, target: self, action: #selector(self.cancelPicker))
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(self.donePicker))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil)
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width / 3, height: self.view.frame.size.height))
        label.font = UIFont(name: "Gotham", size: 11)
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.white
        label.text = label_title
        label.textAlignment = NSTextAlignment.center
        
        let textBtn = UIBarButtonItem(customView: label)
        toolbar.setItems([returnbutton,flexSpace,textBtn,flexSpace,doneButton], animated: false)
        //textfiled.inputAccessoryView = toolbar/
        txtJobType.inputAccessoryView = toolbar
        txtJobType.inputView = picker
        txtJobTitle.inputAccessoryView = toolbar
        txtJobTitle.inputView = picker
       
        
        switch currntJobPickerType {
        case .JobType:
            if let index = arrJobType.firstIndex(where: {$0.job_type == txtJobType.text}){
                picker.selectRow(index, inComponent: 0, animated: false)
            }
        case .JobTitle:
            if let index = arrJobTitle.firstIndex(where: {$0.job_title == txtJobTitle.text}){
                picker.selectRow(index, inComponent: 0, animated: false)
            }
        }
        
    }
    
    
    @objc func donePicker(){
        print("done")
        let selectedRow = picker.selectedRow(inComponent: 0)
        switch currntJobPickerType {
        case .JobType:
            txtJobType.text = arrJobType[selectedRow].job_type
            selectedJobType = arrJobType[selectedRow]
        case .JobTitle:
            txtJobTitle.text =  (arrJobTitle[selectedRow].job_title)
            selectedJobTitle = arrJobTitle[selectedRow]
       
        }
        
        self.view.endEditing(true)
    }
    
    @objc func cancelPicker(){
        
        self.view.endEditing(true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch currntJobPickerType {
        case .JobType:
            return arrJobType.count
        case .JobTitle:
            return  (arrJobTitle.count)
        
           
        }
       
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch currntJobPickerType {
        case .JobType:
            return arrJobType[row].job_type
            
        case .JobTitle:
            return (arrJobTitle[row].job_title)
        
        }
        
    }
    
    func api_JobTitle(){
        let url = WebService.createURLForWebService(WebService.JobTitles)
        AppData.showProgress()
        NetworkManager.hideNetworkLog()
        _ = NetworkManager.shared.requestWithMethodType(withObject: .get, url: url, parameters: nil,isToken: false, successHandler: { (response) in
            DispatchQueue.main.async(execute: {
                print("response \(response)")
                if let userInfo = response as? [String : Any] {
                    if let ard = Mapper<JobTitleDataModel>().mapArray(JSONObject: userInfo["data"]){
                        self.arrJobTitle = ard
                        AppData.hideProgress()
                        
                    }else{
                        AppData.hideProgress()
                    }
                }else{
                    AppData.hideProgress()
                }
                
                
            })
        }) { (errorString) in
            AppData.hideProgress()
            //SVProgressHUD.dismiss()
            //self.view.makeToast(errorString)
        }
    }
    
    func api_JobType(){
        let url = WebService.createURLForWebService(WebService.JobType)
        AppData.showProgress()
        NetworkManager.hideNetworkLog()
        _ = NetworkManager.shared.requestWithMethodType(withObject: .get, url: url, parameters: nil,isToken: false, successHandler: { (response) in
            DispatchQueue.main.async(execute: {
                print("response \(response)")
                if let userInfo = response as? [String : Any] {
                    if let ard = Mapper<JobTypeDataModel>().mapArray(JSONObject: userInfo["data"]){
                        self.arrJobType = ard
                        AppData.hideProgress()
                        
                    }else{
                        AppData.hideProgress()
                    }
                }else{
                    AppData.hideProgress()
                }
                
                
            })
        }) { (errorString) in
            AppData.hideProgress()
            //SVProgressHUD.dismiss()
            //self.view.makeToast(errorString)
        }
    }
    
   
}
