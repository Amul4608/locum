//
//  Profational_RegisterFiveVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 16/08/21.
//

import UIKit
import Alamofire

class Profational_RegisterFiveVC: UIViewController {

    @IBOutlet weak var vwHourlyPrice:UIView!
    @IBOutlet weak var txtHourlyPrice:UITextField!
    @IBOutlet weak var btnContinue:UIButton!
    var DicParam:Parameters = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI(){
        vwHourlyPrice.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        btnContinue.addShadowLikeAndroidWithCorne(cornerRadius: 10)
    }
    
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnContinueTapped(_ sender:UIButton){
        if txtHourlyPrice.text?.isEmpty == true{
            self.view.makeToast("Please enter hourly price.")
        }else{
            DicParam["rate"] = txtHourlyPrice.text!
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.ProfetionalRegisterStoryBord, identifire: Profational_RegisterSixVC.className) as! Profational_RegisterSixVC
            nextVc.DicParam = DicParam
        _ = self.navigationController?.pushViewController(nextVc, animated: true)
        }
       
    }


}
