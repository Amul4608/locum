//
//  Profational_RegisterTwoVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 16/08/21.
//

import UIKit
import Alamofire

class Profational_RegisterTwoVC: UIViewController {

    @IBOutlet weak var vwAddress:UIView!
    @IBOutlet weak var vwAddress2:UIView!
    @IBOutlet weak var vwCity:UIView!
    @IBOutlet weak var vwPostCode:UIView!
    @IBOutlet weak var vwCountry:UIView!
    @IBOutlet weak var btnContinue:UIButton!
    
   
    @IBOutlet weak var txtAddress:UITextField!
    @IBOutlet weak var txtpostCode:UITextField!
    @IBOutlet weak var txtCity:UITextField!
    @IBOutlet weak var txtCountry:UITextField!
    @IBOutlet weak var txtAddress2:UITextField!
    
    
    var DicParam:Parameters = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI(){
        vwAddress.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        vwAddress2.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        vwCity.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        vwPostCode.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        vwCountry.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        btnContinue.addShadowLikeAndroidWithCorne(cornerRadius: 10)
    }
    
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnContinueTapped(_ sender:UIButton){
        if validate(){
            
            DicParam["address"] = txtAddress.text!
            DicParam["address2"] = txtAddress2.text ?? ""
            DicParam["postcode"] = txtpostCode.text!
            DicParam["city"] = txtCity.text!
            DicParam["country"] = txtCountry.text!
            DicParam["role_id"] = "1"
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.ProfetionalRegisterStoryBord, identifire: Profational_RegisterThreeVC.className) as! Profational_RegisterThreeVC
            nextVc.DicParam = DicParam
        _ = self.navigationController?.pushViewController(nextVc, animated: true)
        }
    }
    
    func validate() -> Bool{
       if txtAddress.text?.isEmpty == true{
            self.view.makeToast("Please enter address")
            return false
        }else if txtCity.text?.isEmpty == true{
            self.view.makeToast("Please enter city name")
            return false
        }else if txtpostCode.text?.isEmpty == true{
            self.view.makeToast("Please enter postcode")
            return false
        }else if txtCountry.text?.isEmpty == true{
            self.view.makeToast("Please enter country name")
            return false
        }
        return true
    }

}
extension Profational_RegisterTwoVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtAddress{
            txtAddress2.becomeFirstResponder()
            txtAddress.resignFirstResponder()
        }else if textField == txtAddress2{
            txtCity.becomeFirstResponder()
            txtAddress2.resignFirstResponder()
        }else if textField == txtCity{
            txtpostCode.becomeFirstResponder()
            txtCity.resignFirstResponder()
        }else if textField == txtpostCode{
            txtCountry.becomeFirstResponder()
            txtpostCode.resignFirstResponder()
        }else if textField == txtCountry{
            txtCountry.resignFirstResponder()
            self.view.endEditing(true)
        }
        return true
    }
}
