//
//  ProfetinalJobDetailVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 16/08/21.
//

import UIKit

class ProfetinalJobDetailVC: BaseViewController {

    @IBOutlet weak var vwMain:UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    func setupUI(){
       
        vwMain.addShadowLikeAndroidWithCorne(cornerRadius: 10)
    }
    
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
   

}

