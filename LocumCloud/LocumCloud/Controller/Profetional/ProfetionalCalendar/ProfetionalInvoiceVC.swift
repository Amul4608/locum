//
//  ProfetionalInvoiceVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 16/08/21.
//

import UIKit

class ProfetionalInvoiceVC: BaseViewController {

    @IBOutlet weak var tblList:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    func setupUI(){
        tblList.register(UINib(nibName: ProfetionalsInvoiceCell.className, bundle: nil), forCellReuseIdentifier: ProfetionalsInvoiceCell.className)
    }
    
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    


}

extension ProfetionalInvoiceVC : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfetionalsInvoiceCell.className) as! ProfetionalsInvoiceCell
        cell.vwContainer.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

