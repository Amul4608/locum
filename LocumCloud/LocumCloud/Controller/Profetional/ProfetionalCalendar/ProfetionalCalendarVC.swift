//
//  ProfetionalCalendarVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 16/08/21.
//

import UIKit

class ProfetionalCalendarVC: BaseViewController {

    @IBOutlet weak var tblList:UITableView!
    @IBOutlet weak var heightOFTbl:NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    func setupUI(){
        tblList.register(UINib(nibName: ProfetionalJobListCell.className, bundle: nil), forCellReuseIdentifier: ProfetionalJobListCell.className)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        heightOFTbl.constant = CGFloat(9 * 122)
    }
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
}


extension ProfetionalCalendarVC : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfetionalJobListCell.className) as! ProfetionalJobListCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 122
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.ProfetionalDashboardStoryBord, identifire: ProfetinalJobDetailVC.className) as! ProfetinalJobDetailVC
        _ = self.navigationController?.pushViewController(nextVc, animated: true)
    }
}
