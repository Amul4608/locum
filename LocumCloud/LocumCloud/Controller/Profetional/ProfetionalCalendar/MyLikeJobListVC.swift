//
//  MyLikeJobListVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 16/08/21.
//

import UIKit

class MyLikeJobListVC: BaseViewController {

    @IBOutlet weak var tblList:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tblList.register(UINib(nibName: ProfetionalJobListCell.className, bundle: nil), forCellReuseIdentifier: ProfetionalJobListCell.className)
    }
    
    
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }

   
}


extension MyLikeJobListVC : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfetionalJobListCell.className) as! ProfetionalJobListCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 122
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
