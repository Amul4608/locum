//
//  ForgotPassVC.swift
//  AnyCleanApp
//
//  Created by AmulCGM on 04/07/21.
//

import UIKit
import BottomPopup
import Toast_Swift

class ForgotPassVC: BottomPopupViewController {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblNote:UILabel!
    @IBOutlet weak var lblTextTitle:UILabel!
    @IBOutlet weak var txtValue:UITextField!
    @IBOutlet weak var vwContainer:UIView!
    @IBOutlet weak var btnSend:UIButton!
    
    
    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    func setupUI(){
        vwContainer.addShadowLikeAndroidView()
        btnSend.addShadowLikeAndroidView()
        lblTitle.text = "Forgot Password?"
        lblNote.text = "Enter your email below to receive your password reset instructions."
        
        
    }
    
    // Bottom popup attribute variables
    // You can override the desired variable to change appearance
    
    override var popupHeight: CGFloat { return height ?? CGFloat(300) }
    
    override var popupTopCornerRadius: CGFloat { return topCornerRadius ?? CGFloat(10) }
    
    override var popupPresentDuration: Double { return presentDuration ?? 1.0 }
    
    override var popupDismissDuration: Double { return dismissDuration ?? 1.0 }
    
    override var popupShouldDismissInteractivelty: Bool { return shouldDismissInteractivelty ?? true }
    
    
    @IBAction func btnEmailSend(_ sender:UIButton){
        if txtValue.text?.isEmpty == true{
            self.view.makeToast("Please enter email address.")
        }else if txtValue.text?.isValidEmailAddress() == false{
            self.view.makeToast("Please enter valid email address")
        }else{
            AppData.showProgress()
        let url = WebService.createURLForWebService(WebService.forgotpassword)
        let param = ["email":txtValue.text! ]
       _ = NetworkManager.sharedManager().requestWithMethodTypeRegister(withObject: .post, url: url, parameters: param, isToken: false) { responce in
            if let res = responce as? [String:Any]{
                print(res)
                if let message = res["message"] as? String{
                    AlertView.showAlert("", strMessage: message, button: ["ok"], viewcontroller: self) { btn in
                        self.dismiss(animated: true, completion: nil)
                    }
                }else{
                    self.dismiss(animated: true, completion: nil)
                }
                AppData.hideProgress()
                
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        } failureHandler: { error in
            AppData.hideProgress()
            self.view.makeToast(error)
        }
        }
    }

}
