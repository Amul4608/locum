//
//  CookiesVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 14/08/21.
//

import UIKit
import BottomPopup

class CookiesVC: BottomPopupViewController {

    var height: CGFloat?
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // Bottom popup attribute variables
    // You can override the desired variable to change appearance
    
    override var popupHeight: CGFloat { return height ?? CGFloat(300) }
    
    override var popupTopCornerRadius: CGFloat { return topCornerRadius ?? CGFloat(10) }
    
    override var popupPresentDuration: Double { return presentDuration ?? 1.0 }
    
    override var popupDismissDuration: Double { return dismissDuration ?? 1.0 }
    
    override var popupShouldDismissInteractivelty: Bool { return shouldDismissInteractivelty ?? true }
    
    @IBAction func btnAcceptTapped(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnPrefTapped(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }


}
