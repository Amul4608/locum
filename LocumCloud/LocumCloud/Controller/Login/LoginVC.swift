//
//  LoginVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 14/08/21.
//

import UIKit
import Toast_Swift
import BottomPopup
import ObjectMapper

class LoginVC: UIViewController {

    @IBOutlet weak var btnSend:UIButton!
    @IBOutlet weak var txtUserName:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var vwUserName:UIView!
    @IBOutlet weak var vwPassword:UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        txtUserName.text = ""//"bhavesh.text@mail.com"
        txtPassword.text = ""//"bhavesh1233"
        // Do any additional setup after loading the view.
    }
    func setupUI(){
        btnSend.addShadowLikeAndroidView()
        vwUserName.addShadowLikeAndroidView()
        vwPassword.addShadowLikeAndroidView()
        
    }
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLoginTapped(_ sender:UIButton){
       
        if validate(){
            AppData.showProgress()
        let url = WebService.createURLForWebService(WebService.login)
        let param = ["email":txtUserName.text! , "password":txtPassword.text!]
       _ = NetworkManager.sharedManager().requestWithMethodTypeRegister(withObject: .post, url: url, parameters: param, isToken: false) { responce in
           if let res = responce as? [String:Any]{
               print(res)
               AppData.hideProgress()
               if let mod = Mapper<UserModel>().map(JSON: res){
                if mod.status_code == 500{
                    self.view.makeToast(mod.message ?? "")
                }else{
                   if let secret_code = mod.data?.secret_code{
                      
                           do {
                               try AppLocker.valet.setString("\(secret_code)", forKey: ALConstants.kPincode)
                               AppData.shared.saveUserInfo(res)
                               UserDefaults.standard.setValue(true, forKey: "isLoginDataSaved")
                               if let user = AppData.shared.getUserInfo(){
                                   appUser = user
                                   if let id = appUser?.data?.role_id{
                                       if id == 2{
                                           let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: PracticeDashboardVC.className) as! PracticeDashboardVC
                                           _ = self.navigationController?.pushViewController(nextVc, animated: true)
                                       }else{
                                           let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.ProfetionalDashboardStoryBord, identifire: ProfetionalDashboardVC.className) as! ProfetionalDashboardVC
                                           _ = self.navigationController?.pushViewController(nextVc, animated: true)
                                       }
                                   }
                               }
                           }catch _ {}
                       } else{
                           do {
                               try AppLocker.valet.removeObject(forKey: ALConstants.kPincode)
                           }catch _ {}
                       AppDelegate.sharedAppDelegate.ChangeAppLockController(contoller: self, res: res)
                           
                   }
                }
               }
               
           }
        } failureHandler: { error in
            AppData.hideProgress()
            self.view.makeToast(error)
        }
        }
       
    }
    

    @IBAction func btnForgotTapped(_ sender:UIButton){
        let popupVC = Utilities.getViewControllerFromStoryboard(StoryBoardName.MainStoryBord, identifire: ForgotPassVC.className) as! ForgotPassVC
        popupVC.height = self.view.frame.height - 90
        popupVC.topCornerRadius = 15
        popupVC.presentDuration = 0.5
        popupVC.dismissDuration = 0.5
        popupVC.shouldDismissInteractivelty = true
        popupVC.popupDelegate = self
        self.present(popupVC, animated: true, completion: nil)
    }
    
    func validate() -> Bool{
        if txtUserName.text?.isEmpty == true{
            self.view.makeToast("Please enter email address.")
            return false
        }else if txtUserName.text?.isValidEmailAddress() == false{
            self.view.makeToast("Please enter valid email address.")
            return false
        }else if txtPassword.text?.isEmpty == true{
            self.view.makeToast("Please enter password.")
            return false
        }
        return true
    }

}
extension LoginVC: BottomPopupDelegate {
    func bottomPopupViewLoaded() {
        print("bottomPopupViewLoaded")
    }
    
    func bottomPopupWillAppear() {
        print("bottomPopupWillAppear")
    }
    
    func bottomPopupDidAppear() {
        print("bottomPopupDidAppear")
    }
    
    func bottomPopupWillDismiss() {
        print("bottomPopupWillDismiss")
    }
    
    func bottomPopupDidDismiss() {
        print("bottomPopupDidDismiss")
    }
    
    func bottomPopupDismissInteractionPercentChanged(from oldValue: CGFloat, to newValue: CGFloat) {
        print("bottomPopupDismissInteractionPercentChanged fromValue: \(oldValue) to: \(newValue)")
    }
}
