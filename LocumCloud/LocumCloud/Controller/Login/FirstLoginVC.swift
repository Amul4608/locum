//
//  FirstLoginVC.swift
//  AnyCleanApp
//
//  Created by AmulCGM on 23/09/21.
//

import UIKit
import LocalAuthentication

class FirstLoginVC: UIViewController {

   
    @IBOutlet weak var imgProfile:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var btnId:UIButton!
    @IBOutlet weak var vwView:UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        vwView.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        if biometricType() == .face{
            btnId.setTitle("Login with Face ID", for: .normal)
        }else{
            btnId.setTitle("Login with Touch ID", for: .normal)
        }
        if let user = AppData.shared.getUserInfo(){
            appUser = user
            
            if let image = appUser?.data?.user_image , image != ""{
            self.imgProfile.setImageWithActivity(image, UIActivityIndicatorView.Style.gray)
        }
        if let name  = appUser?.data?.fullNAme() {
            self.lblTitle.text = name
        }
        }
        // Do any additional setup after loading the view.
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "FirstLoginVC"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FirstLoginVC(_:)), name: NSNotification.Name(rawValue: "FirstLoginVC"), object: nil)
    }
    
    @objc func FirstLoginVC(_ notification:NSNotification)
    {
        UserDefaults.standard.setValue(true, forKey: "isLoginDataSaved")
        if let user = AppData.shared.getUserInfo(){
            appUser = user
            if let id = appUser?.data?.role_id{
                if id == 2{
                    let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: PracticeDashboardVC.className) as! PracticeDashboardVC
                    _ = self.navigationController?.pushViewController(nextVc, animated: true)
                }else{
                    let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.ProfetionalDashboardStoryBord, identifire: ProfetionalDashboardVC.className) as! ProfetionalDashboardVC
                    _ = self.navigationController?.pushViewController(nextVc, animated: true)
                }
            }
        }
    }
    
    @IBAction func btnLoginTapped(_ sender:UIButton){
        appUser = nil
        if let viewControllers = self.navigationController?.viewControllers {
               for vc in viewControllers {
                    if vc.isKind(of: LoginVC.classForCoder()) {
                         print("It is in stack")
                         //Your Process
                        self.navigationController?.popToViewController(vc, animated: true)
                        return
                    }
               }
            let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.MainStoryBord, identifire: LoginVC.className) as! LoginVC
            _ = self.navigationController?.pushViewController(nextVc, animated: false)
         }
    }
    @IBAction func btnFaceLoackTapped(_ sender:UIButton){
        AppDelegate.sharedAppDelegate.validateAppLockController(contoller: self, isTrance: true)
    }
    func matchLock(){
        let myContext = LAContext()
        let myLocalizedReasonString = "Biometric Authntication testing !! "
        
        var authError: NSError?
        if #available(iOS 8.0, macOS 10.12.1, *) {
            if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { success, evaluateError in
                    
                    DispatchQueue.main.async {
                        if success {
                            UserDefaults.standard.setValue(true, forKey: "isLoginDataSaved")
                            if let user = AppData.shared.getUserInfo(){
                                appUser = user
                                if let id = appUser?.data?.role_id{
                                    if id == 2{
                                        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: PracticeDashboardVC.className) as! PracticeDashboardVC
                                        _ = self.navigationController?.pushViewController(nextVc, animated: true)
                                    }else{
                                        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.ProfetionalDashboardStoryBord, identifire: ProfetionalDashboardVC.className) as! ProfetionalDashboardVC
                                        _ = self.navigationController?.pushViewController(nextVc, animated: true)
                                    }
                                }
                            }
                        } else {
                            // User did not authenticate successfully, look at error and take appropriate action
                            self.view.makeToast("Sorry!!... User did not authenticate successfully")
                        }
                    }
                }
            } else {
                // Could not evaluate policy; look at authError and present an appropriate message to user
                self.view.makeToast("Sorry!!.. Could not evaluate policy.")
            }
        } else {
            // Fallback on earlier versions
            self.view.makeToast("Ooops!!.. This feature is not supported.")
        }
    }
    func biometricType() -> BiometricType {
        let authContext = LAContext()
        if #available(iOS 11, *) {
            let _ = authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
            switch(authContext.biometryType) {
            case .none:
                return .none
            case .touchID:
                return .touch
            case .faceID:
                return .face
            }
        } else {
            return authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) ? .touch : .none
        }
    }
    
    @IBAction func btnRegisterTapped(_ sender:UIButton){
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.MainStoryBord, identifire: RegisterOneVC.className) as! RegisterOneVC
        _ = self.navigationController?.pushViewController(nextVc, animated: true)
    }

    

}
