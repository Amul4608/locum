//
//  LaunchVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 14/08/21.
//

import UIKit
import BottomPopup


class LaunchVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if appUser != nil{
        if isLoginDataSaved == true{
            if let id = appUser?.data?.role_id{
                if id == 2{
                    let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: PracticeDashboardVC.className) as! PracticeDashboardVC
                    _ = self.navigationController?.pushViewController(nextVc, animated: true)
                }else{
                    let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.ProfetionalDashboardStoryBord, identifire: ProfetionalDashboardVC.className) as! ProfetionalDashboardVC
                    _ = self.navigationController?.pushViewController(nextVc, animated: true)
                }
            }else{
              //  vwSplash.isHidden = true
                
            }
        }else{
           // vwSplash.isHidden = true
            if appUser != nil{
           
            let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.MainStoryBord, identifire: FirstLoginVC.className) as! FirstLoginVC
            _ = self.navigationController?.pushViewController(nextVc, animated: true)
            }
            
        }
        }else{
        goCookies()
        }
        
    }
    
    @IBAction func btnLoginTapped(_ sender:UIButton){
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.MainStoryBord, identifire: LoginVC.className) as! LoginVC
        _ = self.navigationController?.pushViewController(nextVc, animated: true)
    }
    
    @IBAction func btnRegisterTapped(_ sender:UIButton){
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.MainStoryBord, identifire: RegisterOneVC.className) as! RegisterOneVC
        _ = self.navigationController?.pushViewController(nextVc, animated: true)
    }
    
    
    func goCookies(){
        let popupVC = Utilities.getViewControllerFromStoryboard(StoryBoardName.MainStoryBord, identifire: CookiesVC.className) as! CookiesVC
        popupVC.height = 390
        popupVC.topCornerRadius = 15
        popupVC.presentDuration = 0.5
        popupVC.dismissDuration = 0.5
        popupVC.shouldDismissInteractivelty = true
        popupVC.popupDelegate = self
        self.present(popupVC, animated: true, completion: nil)
    }
    


}
extension LaunchVC: BottomPopupDelegate {
    func bottomPopupViewLoaded() {
        print("bottomPopupViewLoaded")
    }
    
    func bottomPopupWillAppear() {
        print("bottomPopupWillAppear")
    }
    
    func bottomPopupDidAppear() {
        print("bottomPopupDidAppear")
    }
    
    func bottomPopupWillDismiss() {
        print("bottomPopupWillDismiss")
    }
    
    func bottomPopupDidDismiss() {
        print("bottomPopupDidDismiss")
    }
    
    func bottomPopupDismissInteractionPercentChanged(from oldValue: CGFloat, to newValue: CGFloat) {
        print("bottomPopupDismissInteractionPercentChanged fromValue: \(oldValue) to: \(newValue)")
    }
}


