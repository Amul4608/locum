//
//  FeedBackVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 15/08/21.
//

import UIKit

class FeedBackVC: BaseViewController {

    @IBOutlet weak var tblList:UITableView!
    @IBOutlet weak var heightOFTbl:NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    func setupUI(){
        tblList.register(UINib(nibName: ReviewCell.className, bundle: nil), forCellReuseIdentifier: ReviewCell.className)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        heightOFTbl.constant = CGFloat(10*248)
    }
    
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    


}

extension FeedBackVC : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReviewCell.className) as! ReviewCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 248
    }
}
