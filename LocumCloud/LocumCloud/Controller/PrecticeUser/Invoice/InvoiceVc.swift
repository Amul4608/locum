//
//  InvoiceVc.swift
//  LocumCloud
//
//  Created by AmulCGM on 15/08/21.
//

import UIKit

class InvoiceVc: BaseViewController {

    @IBOutlet weak var tblList:UITableView!
    @IBOutlet weak var heightOFTbl:NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    func setupUI(){
        self.tab?.resetAll()
        self.tab?.btnInvoice.setImage(UIImage(named: "invoices-icon-active.png"), for: .normal)
        tblList.register(UINib(nibName: InvoiceCell.className, bundle: nil), forCellReuseIdentifier: InvoiceCell.className)
    }
    
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        heightOFTbl.constant = CGFloat(10*267)
    }


}

extension InvoiceVc : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: InvoiceCell.className) as! InvoiceCell
        cell.vwContainer.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 267
    }
}

