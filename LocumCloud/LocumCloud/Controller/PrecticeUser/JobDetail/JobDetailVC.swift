//
//  JobDetailVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 15/08/21.
//

import UIKit
import ObjectMapper
import Alamofire

class JobDetailVC: BaseViewController {

    @IBOutlet weak var lblJobTitle:UILabel!
    @IBOutlet weak var lblJobDate:UILabel!
    @IBOutlet weak var lblJobMOnth:UILabel!
    @IBOutlet weak var lblJobNameTitle:UILabel!
    
    @IBOutlet weak var lblJObAddress:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lblTime:UILabel!
    
    @IBOutlet weak var lblSpTitle:UILabel!
    @IBOutlet weak var lblpriceData:UILabel!
    
    @IBOutlet weak var lblActiveInActive:UILabel!
    @IBOutlet weak var lblTotalPrice:UILabel!
    @IBOutlet weak var lblJobDesc:UILabel!
    
   
    @IBOutlet weak var vwActive:UIView!
    
    var delegate:JobUpdateDelegate?
    
    @IBOutlet weak var tblList:UITableView!
    @IBOutlet weak var heightOfTbl:NSLayoutConstraint!
    @IBOutlet weak var vwMain:UIView!
    
    var jobDataModel:JobListDataModel?
    var jobUSer:[JobApply_UserData] = []
    var jobID:Int = 0
    var objJobData:JobDetailModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    func setupUI(){
        tblList.register(UINib(nibName: "UserInfoCell", bundle: nil), forCellReuseIdentifier: "UserInfoCell")
        tblList.tableFooterView = UIView()
        vwMain.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        apicall_JobDetails(id: jobID)
    }
    
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        heightOfTbl.constant = CGFloat(jobUSer.count * 175)
    }

}

extension JobDetailVC : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jobUSer.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserInfoCell") as! UserInfoCell
        cell.obj = jobUSer[indexPath.row].user
        cell.btnProfile.tag = indexPath.row
        cell.btnProfile.addTarget(self, action: #selector(self.btnProfileTapped(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 175
    }
    
    
    
    
    func apicall_JobDetails(id:Int){
        
        let url = WebService.createURLForWebService(WebService.job_details)
        let param = ["job_id":id]
        AppData.showProgress()
         NetworkManager.hideNetworkLog()
         _ = NetworkManager.shared.requestWithMethodType(withObject: .post, url: url, parameters: param,isToken: true, successHandler: { (response) in
             DispatchQueue.main.async(execute: {
                 print("response \(response)")
                 if let userInfo = response as? [String : Any] {
                     if let ard = Mapper<JobDetailModel>().map(JSONObject: userInfo["data"]){
                         self.objJobData = ard
                         self.setupDate(obj: ard)
                     }
                 }
                 
                 AppData.hideProgress()
             })
         }) { (errorString) in
            AppData.hideProgress()
             self.view.makeToast(errorString)
         }
    }
    
    func setupDate(obj:JobDetailModel){
        lblJobTitle.text = "Job \(obj.id ?? 0)"
        lblJobDate.text = getDisplayDate(date: obj.date ?? "", dateFormat: "yyyy-MM-dd", displayFormat: "dd")
        lblJobMOnth.text = "\(getDisplayDate(date: obj.date ?? "", dateFormat: "yyyy-MM-dd", displayFormat: "MMM"))"
        lblTime.text = " \(getDisplayDate(date: obj.start_time ?? "", dateFormat: "HH:mm:ss", displayFormat: "hh:mm a")) -  \(getDisplayDate(date: obj.end_time ?? "", dateFormat: "HH:mm:ss", displayFormat: "hh:mm a"))"
        lblDate.text = getDisplayDate(date: obj.date ?? "", dateFormat: "yyyy-MM-dd", displayFormat: "EEE dd/MM/yyyy")
        lblJObAddress.text = obj.location
        lblJobNameTitle.text = (obj.headline ?? "") + " (\(obj.job_title?.job_title ?? "")) \(obj.job_type?.job_type ?? "")"
        lblSpTitle.text = (obj.headline ?? "") + " (\(obj.job_title?.job_title ?? ""))"
        lblpriceData.text = "£\(obj.hourly_rate ?? "0") per hour from \(lblTime.text ?? "")"
        lblTotalPrice.text = "£450"
        lblJobDesc.text = obj.description
        jobUSer = obj.job_apply ?? []
        self.viewDidLayoutSubviews()
        self.tblList.reloadData()
        if obj.status == 0{
            lblActiveInActive.text = "Inactive"
            vwActive.backgroundColor = UIColor(hexString: "#DC5746")
        }else if obj.status == 1{
            
                lblActiveInActive.text = "Active"
                vwActive.backgroundColor = UIColor(hexString: "#9AD34D")
            
        }else{
            lblActiveInActive.text = "In Progress"
            vwActive.backgroundColor = UIColor(hexString: "#9AD34D")
        }
        
    }
    
    @IBAction func btnProfileTapped(_ sender:UIButton){
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: PracticeJobUserVC.className) as! PracticeJobUserVC
       // nextVc.delegate = self
        nextVc.jobDataModel = self.jobDataModel
        nextVc.jobUSer = jobUSer[sender.tag]
        //nextVc.isFromEdit = true
        self.navigationController?.pushViewController(nextVc, animated: true)
    }
    
    @IBAction func BtnActiveINActiveTapped(_ sender:UIButton){
        if objJobData?.status == 0{
            apicallStatusUpdateJob(id: self.jobID, status: 1)
        }else if objJobData?.status == 1{
            apicallStatusUpdateJob(id: self.jobID, status: 0)
        }
    }
    
    @IBAction func editTapped(_ sender:UIButton){
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: CreateJobVC.className) as! CreateJobVC
        nextVc.delegate = self
        nextVc.jobDataModel = self.jobDataModel
        nextVc.isFromEdit = true
        self.present(nextVc, animated: true, completion: nil)
    }
    @IBAction func DeleteTapped(_ sender:UIButton){
        AlertView.showAlert("", strMessage: "Are you sure want to remove this job?", button: ["Yes, Remove" , "No, Go Back"], viewcontroller: self) { btn in
            if btn == 0{
               
                self.apicallDeleteJob(id: self.jobID)
            }
        }
    }
    @IBAction func DuplicateTapped(_ sender:UIButton){
        AlertView.showAlert("", strMessage: "Are you sure want to copy of this job.", button: ["Copy" , "No"], viewcontroller: self) { btn in
            if btn == 0{
               
                self.apicallDuplicateJob(id: self.jobID)
            }
        }
    }
    
    func apicallStatusUpdateJob(id:Int , status:Int){
        let url = WebService.createURLForWebService(WebService.jobStatus)
        AppData.showProgress()
        let param = ["job_id":id , "status":status]
         NetworkManager.showNetworkLog()
         _ = NetworkManager.shared.requestWithMethodType(withObject: .post, url: url, parameters: param,isToken: true, successHandler: { (response) in
             self.view.makeToast("Job status updated sucessfully.")
             self.delegate?.JobUpdate()
             self.objJobData?.status = status
             if status == 0{
                 self.lblActiveInActive.text = "Inactive"
                 self.vwActive.backgroundColor = UIColor(hexString: "#DC5746")
             }else if status == 1{
                 
                 self.lblActiveInActive.text = "Active"
                 self.vwActive.backgroundColor = UIColor(hexString: "#9AD34D")
                 
             }
         }) { (errorString) in
            AppData.hideProgress()
             self.view.makeToast(errorString)
         }
    }
    
    func apicallDuplicateJob(id:Int){
        let url = WebService.createURLForWebService(WebService.job_Duplicate) + "\(id)"
        AppData.showProgress()
        
         NetworkManager.showNetworkLog()
         _ = NetworkManager.shared.requestWithMethodType(withObject: .post, url: url, parameters: nil,isToken: true, successHandler: { (response) in
             self.view.makeToast("Job copied sucessfully.")
             self.delegate?.JobUpdate()
             self.navigationController?.popViewController(animated: true)
         }) { (errorString) in
            AppData.hideProgress()
             self.view.makeToast(errorString)
         }
    }
    
    func apicallDeleteJob(id:Int){
        let url = WebService.createURLForWebService(WebService.jobs_delete) + "\(id)"
        AppData.showProgress()
        
         NetworkManager.showNetworkLog()
         _ = NetworkManager.shared.requestWithMethodType(withObject: .post, url: url, parameters: nil,isToken: true, successHandler: { (response) in
             DispatchQueue.main.async(execute: {
                 print("response \(response)")
                 self.view.makeToast("Job deleted sucessfully.")
                 self.delegate?.JobUpdate()
                 self.navigationController?.popViewController(animated: true)
                 AppData.hideProgress()
             })
         }) { (errorString) in
            AppData.hideProgress()
             self.view.makeToast(errorString)
         }
    }
    
    
    
}
extension JobDetailVC : JobUpdateDelegate{
    func JobUpdate() {
        self.apicall_JobDetails(id: self.jobID)
        self.delegate?.JobUpdate()
    }
    
    
}
