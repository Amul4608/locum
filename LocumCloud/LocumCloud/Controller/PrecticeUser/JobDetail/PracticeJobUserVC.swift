//
//  PracticeJobUserVC.swift
//  LocumCloud
//
//  Created by Amul Patel on 09/10/21.
//

import UIKit
import Alamofire


class PracticeJobUserVC: UIViewController {

    @IBOutlet weak var lblNavTitle:UILabel!
    @IBOutlet weak var vwContainer:UIView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lblMonth:UILabel!
    @IBOutlet weak var lblStartTime:UILabel!
    @IBOutlet weak var lblEndTime:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    
    @IBOutlet weak var lblShift:UILabel!
    
    @IBOutlet weak var imgImage:UIImageView!
    @IBOutlet weak var lblJOBUSERTitle:UILabel!
    @IBOutlet weak var lblRating:UILabel!
    @IBOutlet weak var lblDesc:UILabel!
    @IBOutlet weak var btnAccept:UIButton!
    @IBOutlet weak var btnRemove:UIButton!
    
    
    @IBOutlet weak var lblDocumetntitle:UILabel!
    @IBOutlet weak var lblFeedBackTitle:UILabel!
    
    var delegate:JobUpdateDelegate?
    var jobDataModel:JobListDataModel!
    var jobUSer:JobApply_UserData!
    var jobID:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI(){
        vwContainer.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        setData()
    }
    func setData(){
        lblTitle.text = (jobDataModel.headline ?? "") + " (\(jobDataModel.qualification ?? ""))"
        lblAddress.text = jobDataModel.location
        lblPrice.text = "£" + (jobDataModel.hourly_rate  ?? "")
        self.lblDesc.text = jobDataModel.description
        lblMonth.text = getDisplayDate(date: jobDataModel.date ?? "", dateFormat: "yyyy-MM-dd", displayFormat: "MMM")
        lblDate.text = getDisplayDate(date: jobDataModel.date ?? "", dateFormat: "yyyy-MM-dd", displayFormat: "dd")
        lblStartTime.text = getDisplayDate(date: jobDataModel.start_time ?? "", dateFormat: "HH:mm:ss", displayFormat: "hh:mm a")
        lblEndTime.text = getDisplayDate(date: jobDataModel.end_time ?? "", dateFormat: "HH:mm:ss", displayFormat: "hh:mm a")
        
        if let obj = jobUSer.user{
        if let img = obj.user_image{
            self.imgImage.setImageWithActivity(img, UIActivityIndicatorView.Style.gray)
        }
        self.lblJOBUSERTitle.text = (obj.first_name ?? "") + " " +  (obj.last_name ?? "")
        btnAccept.setTitle("Accept \(obj.first_name ?? "") for this Job", for: .normal)
        btnRemove.setTitle("Reject \(obj.first_name ?? "") for this Job", for: .normal)
        self.lblRating.text = obj.job_review
            var arrnm:[String] = []
            if let arr = obj.jobtitle{
            for i in arr{
                arrnm.append(i.job_title ?? "")
            }}
            self.lblShift.text = arrnm.joined(separator: ",")
            lblDocumetntitle.text = "\(obj.first_name ?? "") Documents"
            lblFeedBackTitle.text = "\(obj.first_name ?? "") Feedback"
            lblNavTitle.text = "\(obj.first_name ?? "") Profile"
    }
    }
    
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnApplyapped(_ sender:UIButton){
        var param:Parameters = [:]
        param["job_id"] = self.jobID
        param["user_id"] = self.jobUSer.id ?? 0
        let url =  WebService.createURLForWebService(WebService.job_accept)
        AppData.showProgress()
        NetworkManager.showNetworkLog()
        _ = NetworkManager.shared.requestWithMethodType(withObject: .post, url: url, parameters: param,isToken: true, successHandler: { (response) in
             DispatchQueue.main.async(execute: {
                 print("response \(response)")
                 
                     self.view.makeToast("Job user selected sucessfully.")
                     self.delegate?.JobUpdate()
                     self.navigationController?.popViewController(animated: true)
                 
                 AppData.hideProgress()
             })
         }) { (errorString) in
            AppData.hideProgress()
             self.view.makeToast(errorString)
         }
    }
    @IBAction func btnRequestTapped(_ sender:UIButton){
        var param:Parameters = [:]
        param["job_id"] = self.jobID
        param["user_id"] = self.jobUSer.id ?? 0
        let url =  WebService.createURLForWebService(WebService.job_reject)
        AppData.showProgress()
        NetworkManager.showNetworkLog()
        _ = NetworkManager.shared.requestWithMethodType(withObject: .post, url: url, parameters: param,isToken: true, successHandler: { (response) in
             DispatchQueue.main.async(execute: {
                 print("response \(response)")
                 
                     self.view.makeToast("Job user selected sucessfully.")
                     self.delegate?.JobUpdate()
                     self.navigationController?.popViewController(animated: true)
                 
                 AppData.hideProgress()
             })
         }) { (errorString) in
            AppData.hideProgress()
             self.view.makeToast(errorString)
         }
    }
    
    @IBAction func btnaFeedbackTapped(_ sender:UIButton){
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: FeedBackVC.className) as! FeedBackVC
        _ = self.navigationController?.pushViewController(nextVc, animated: true)
    }
    

    
}
