//
//  PracticeDashboardVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 15/08/21.
//

import UIKit
import ObjectMapper

class PracticeDashboardVC: UIViewController {

    @IBOutlet weak var clwBanner:UICollectionView!
    var arrList:[JobListDataModel] = []
    @IBOutlet weak var lblUserNAme:UILabel!
    @IBOutlet weak var lblJobCount:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isLockValidate == false{
        
            AppDelegate.sharedAppDelegate.validateAppLockController(contoller: self, isTrance: false)
        }
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI(){
        lblUserNAme.text = " Hello , \(appUser?.data?.fullNAme() ?? "")"
        lblJobCount.text = "\(arrList.count)"
        clwBanner.register(UINib(nibName: "BannerCell", bundle: nil), forCellWithReuseIdentifier: "BannerCell")
        //Get device width
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = clwBanner.frame.width
        
        //set section inset as per your requirement.
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        //set cell item size here
        layout.itemSize = CGSize(width: Int(width) , height: 120)
        
        //set Minimum spacing between 2 items
        layout.minimumInteritemSpacing = 0
        
        //set minimum vertical line spacing here between two lines in collectionview
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        
        //apply defined layout to collectionview
        clwBanner!.collectionViewLayout = layout
        apicall(id: "\(appUser?.data?.id ?? 0)")
    }
    
    @IBAction func btnCardTapped(_ sender:UIButton){
        if sender.tag == 0{
            let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: ActiveJobVC.className) as! ActiveJobVC
            _ = self.navigationController?.pushViewController(nextVc, animated: true)
        }else if sender.tag == 1{
            let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: CalendarVC.className) as! CalendarVC
            _ = self.navigationController?.pushViewController(nextVc, animated: true)
        }else if sender.tag == 2{
            let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: InvoiceVc.className) as! InvoiceVc
            _ = self.navigationController?.pushViewController(nextVc, animated: true)
        }else if sender.tag == 3{
            let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: PrecticeMyProfileVC.className) as! PrecticeMyProfileVC
            _ = self.navigationController?.pushViewController(nextVc, animated: true)
        }else if sender.tag == 4{
            let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: FeedBackVC.className) as! FeedBackVC
            _ = self.navigationController?.pushViewController(nextVc, animated: true)
        }
    }
    

}

extension PracticeDashboardVC : UICollectionViewDataSource , UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
        
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BannerCell.className, for: indexPath) as! BannerCell
       
        return cell
       }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        let cellsize = CGSize(width: collectionView.frame.width , height:120)
        return cellsize
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
    
    func apicall(id:String){
            let url = WebService.createURLForWebService(WebService.practice_my_job)
            AppData.showProgress()
            NetworkManager.showNetworkLog()
            _ = NetworkManager.shared.requestWithMethodType(withObject: .post, url: url, parameters: ["user_id":id],isToken: true, successHandler: { (response) in
                 DispatchQueue.main.async(execute: {
                     print("response \(response)")
                     if let userInfo = response as? [String : Any] {
                         if let ard = Mapper<JobListDataModel>().mapArray(JSONObject: userInfo["data"]){
                             self.arrList = ard
                             self.lblJobCount.text = "\(self.arrList.count)"
                         }
                     }
                     
                     AppData.hideProgress()
                 })
             }) { (errorString) in
                AppData.hideProgress()
                 self.view.makeToast(errorString)
             }
        }
   


}
