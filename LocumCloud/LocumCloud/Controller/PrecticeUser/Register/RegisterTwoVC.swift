//
//  RegisterTwoVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 14/08/21.
//

import UIKit
import Alamofire

class RegisterTwoVC: UIViewController {

    @IBOutlet weak var vwFirstName:UIView!
    @IBOutlet weak var vwLastName:UIView!
    @IBOutlet weak var vwJobName:UIView!
    @IBOutlet weak var btnConTinue:UIButton!
    @IBOutlet weak var txtFirstName:UITextField!
    @IBOutlet weak var txtLastName:UITextField!
    @IBOutlet weak var txtJobName:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vwFirstName.addShadowLikeAndroidView()
        vwJobName.addShadowLikeAndroidView()
        vwLastName.addShadowLikeAndroidView()
        btnConTinue.addShadowLikeAndroidView()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnContinueTapped(_ sender:UIButton){
        if validate(){
            var DicParam:Parameters = [:]
            DicParam["first_name"] = txtFirstName.text!
            DicParam["last_name"] = txtLastName.text!
            DicParam["job_title"] = txtJobName.text!
            let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.MainStoryBord, identifire: RegisterThreeVC.className) as! RegisterThreeVC
            nextVc.DicParam = DicParam
            _ = self.navigationController?.pushViewController(nextVc, animated: true)
        }
       
    }
  
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func validate() -> Bool{
        if txtFirstName.text?.isEmpty == true{
            self.view.makeToast("Please enter first name")
            return false
        }else if txtLastName.text?.isEmpty == true{
            self.view.makeToast("Please enter last name")
            return false
        }else if txtJobName.text?.isEmpty == true{
            self.view.makeToast("Please enter job name")
            return false
        }
        return true
    }


}
extension RegisterTwoVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtFirstName{
            txtLastName.becomeFirstResponder()
            txtFirstName.resignFirstResponder()
        }else if textField == txtLastName{
            txtJobName.becomeFirstResponder()
            txtLastName.resignFirstResponder()
        }else if textField == txtJobName{
            txtJobName.resignFirstResponder()
            self.view.endEditing(true)
        }
        return true
    }
}
