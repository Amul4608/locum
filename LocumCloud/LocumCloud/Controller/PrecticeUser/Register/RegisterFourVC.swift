//
//  RegisterFourVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 15/08/21.
//

import UIKit
import Alamofire

class RegisterFourVC: UIViewController {

    var DicParam:Parameters = [:]
    @IBOutlet weak var vwEmailName:UIView!
    @IBOutlet weak var vwPasswordName:UIView!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    var arrAttechment:[attachmentModel] = []
    @IBOutlet weak var btnConTinue:UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        vwEmailName.addShadowLikeAndroidView()
        vwPasswordName.addShadowLikeAndroidView()
        btnConTinue.addShadowLikeAndroidView()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnContinueTapped(_ sender:UIButton){
        if validate(){
            DicParam["email"] = txtEmail.text!
            DicParam["password"] = txtPassword.text!
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.MainStoryBord, identifire: RegisterTermsConditionVC.className) as! RegisterTermsConditionVC
            nextVc.DicParam = self.DicParam
            nextVc.email = self.txtEmail.text!
            nextVc.password = self.txtPassword.text!
            nextVc.arrAttechment = self.arrAttechment
        _ = self.navigationController?.pushViewController(nextVc, animated: true)
        }
    }
  
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    func validate() -> Bool{
        if txtEmail.text?.isEmpty == true{
            self.view.makeToast("Please enter email address")
            return false
        }else if txtEmail.text?.isValidEmailAddress() == false{
            self.view.makeToast("Please enter valid email address")
            return false
        }else if txtPassword.text?.isEmpty == true{
            self.view.makeToast("Please enter password")
            return false
        }else if txtPassword.text?.count ?? 0 < 6{
            self.view.makeToast("Password required atleast 6 character")
        }
        return true
    }



}
