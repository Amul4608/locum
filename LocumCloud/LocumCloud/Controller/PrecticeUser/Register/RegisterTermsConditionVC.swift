//
//  RegisterTermsConditionVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 15/08/21.
//

import UIKit
import Alamofire

class RegisterTermsConditionVC: UIViewController {
    var arrAttechment:[attachmentModel] = []
    @IBOutlet weak var tblList:UITableView!
    @IBOutlet weak var heightOfTbl:NSLayoutConstraint!
    @IBOutlet weak var btnContinue:UIButton!
    var DicParam:Parameters = [:]
    var email:String = ""
    var password:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI(){
        btnContinue.addShadowLikeAndroidView()
        tblList.register(UINib(nibName: "RegisterCell", bundle: nil), forCellReuseIdentifier: "RegisterCell")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        heightOfTbl.constant = tblList.contentSize.height
    }
    
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnContinueTapped(_ sender:UIButton){
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.MainStoryBord, identifire: RegisterImageVC.className) as! RegisterImageVC
        nextVc.DicParam = self.DicParam
        nextVc.email = self.email
        nextVc.password = self.password
        nextVc.arrAttechment = self.arrAttechment
        _ = self.navigationController?.pushViewController(nextVc, animated: true)
    }

}

extension RegisterTermsConditionVC : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RegisterCell.className) as! RegisterCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
