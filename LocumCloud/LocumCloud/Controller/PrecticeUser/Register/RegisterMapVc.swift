//
//  RegisterMapVc.swift
//  LocumCloud
//
//  Created by AmulCGM on 14/08/21.
//

import UIKit
import MapKit
import Alamofire

class RegisterMapVc: UIViewController , MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var btnContinue:UIButton!
    @IBOutlet weak var vwMap:UIView!
    var locations:[Location] = []
    var DicParam:Parameters = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        btnContinue.addShadowLikeAndroidView()
        vwMap.addShadowLikeAndroidView()
        AppDelegate.sharedAppDelegate.getCurrentLocation { loca, error in
            let obj = Location(title: "", latitude: (loca?.coordinate.latitude)!, longitude: (loca?.coordinate.longitude)!)
            self.DicParam["latitude"] = "\((loca?.coordinate.latitude)!)"
            self.DicParam["longitude"] = "\((loca?.coordinate.longitude)!)"
            self.locations.append(obj)
            self.annotationsOnMap()
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func annotationsOnMap() {
        for location in locations {
            let annotations = MKPointAnnotation()
            annotations.title = location.title
            annotations.coordinate = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
            mapView.addAnnotation(annotations)
            let locationCoordinate2d = annotations.coordinate
            let span = MKCoordinateSpan(latitudeDelta: 0, longitudeDelta: 360 / pow(2, 16) * Double(self.mapView.frame.size.width) / 256)
            let region = MKCoordinateRegion(center: locationCoordinate2d, span: span)
            mapView.setRegion(region, animated: true)
        }
    }
    
    @IBAction func btnContinueTapped(_ sender:UIButton){
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.MainStoryBord, identifire: RegisterFourVC.className) as! RegisterFourVC
        nextVc.DicParam = self.DicParam
        _ = self.navigationController?.pushViewController(nextVc, animated: true)
    }

}



struct Location {
    let title: String
    let latitude: Double
    let longitude: Double
}

extension MKMapView {
    var zoomLevel: Double {
        get {
            return log2(360 * (Double(self.frame.size.width / 256) / self.region.span.longitudeDelta)) + 1
        }
        set (newZoomLevel){
            setCenterCoordinate(coordinate:self.centerCoordinate, zoomLevel: newZoomLevel, animated: false)
        }
    }
    private func setCenterCoordinate(coordinate: CLLocationCoordinate2D, zoomLevel: Double, animated: Bool) {
        let span = MKCoordinateSpan(latitudeDelta: 0, longitudeDelta: 360 / pow(2, zoomLevel) * Double(self.frame.size.width) / 256)
        setRegion(MKCoordinateRegion(center: coordinate, span: span), animated: animated)
    }
}
