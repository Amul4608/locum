//
//  RegisterImageVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 15/08/21.
//

import UIKit
import Alamofire
import ObjectMapper

var isPrecticeUser:Bool = false
class RegisterImageVC: UIViewController {

    @IBOutlet weak var vwCamera:UIView!
    @IBOutlet weak var vwGallery:UIView!
    @IBOutlet weak var btnContinue:UIButton!
    @IBOutlet weak var imgUser:UIImageView!
    let imagepicker = UIImagePickerController()
    var selectedImageData:Data? = nil
    var DicParam:Parameters = [:]
    var email:String = ""
    var password:String = ""
    var arrAttechment:[attachmentModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI(){
        vwCamera.addShadowLikeAndroidView()
        vwGallery.addShadowLikeAndroidView()
        btnContinue.addShadowLikeAndroidView()
    }
    
    @IBAction func btnContinueTapped(_ sender:UIButton){
        /*if isPrecticeUser == true{
            let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: PracticeDashboardVC.className) as! PracticeDashboardVC
            _ = self.navigationController?.pushViewController(nextVc, animated: true)
        }else{
            let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.ProfetionalDashboardStoryBord, identifire: ProfetionalDashboardVC.className) as! ProfetionalDashboardVC
            _ = self.navigationController?.pushViewController(nextVc, animated: true)
        }*/
        self.ApiCallRegister()
        
    }
  
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    

}

//MARK:- Add Attechment
extension  RegisterImageVC :   UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    
    @IBAction func btnGalleryTapped(_ sender:UIButton){
        self.showImage(type: "photoLibrary", isProof: sender.tag == 0 ? false : true, animation: true)
    }
    @IBAction func btnCameraTapped(_ sender:UIButton){
        self.showImage(type: "Camera", isProof: sender.tag == 0 ? false : true ,animation: true)
    }
    
    
    func showImage(type:String , isProof:Bool , animation:Bool){
        
       // self.captureType = type
        DispatchQueue.main.async
            {
                self.imagepicker.delegate = self
                self.imagepicker.mediaTypes = ["public.image"]
                self.imagepicker.allowsEditing = false
                if type != "photoLibrary"{
                    self.imagepicker.sourceType = .camera
                    self.imagepicker.cameraCaptureMode = .photo
                }else{
                    self.imagepicker.sourceType = .photoLibrary
                }
                self.present(self.imagepicker, animated: animation, completion: nil)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Code here
        if info[.mediaType] as? String == "public.image"{
            
            if let selectedImage = info[.originalImage] as? UIImage{
                let data = selectedImage.jpegData(compressionQuality: 0.5)! as Data
                
                selectedImageData = data
                self.imgUser.image = selectedImage
               
                dismiss(animated: true, completion: nil)
                
            }
            
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    
    func ApiCallRegister(){
     
       let url = WebService.createURLForWebService(WebService.practice_register)
    
       AppData.showProgress()
        let data = imgUser.image?.jpegData(compressionQuality: 0.5)
        selectedImageData = data
        let obj = attachmentModel(name: "user_image", type: "image", key: "user_image", val: self.selectedImageData!)
        arrAttechment.append(obj)
      NetworkManager.showNetworkLog()
       
       AppData.showProgress()
        
      NetworkManager.showNetworkLog()
        _ = NetworkManager.shared.multipartRequestWithAttachmentMethodType(.post, attechment: arrAttechment, url: url, parameters: self.DicParam, isToken: false, successHandler: { (response) in
          
          DispatchQueue.main.async(execute: {
              
            print("response \(response)")
            self.apicallLogin()
                       
          })
      }, progressHandler: { (progress) in
          print("progress\(progress)")
      }, failureHandler: { (errorString) in
          AppData.hideProgress()
          AlertView.showOKTitleAlert(errorString, viewcontroller: self)
      })
        
    }
    
    
    func apicallLogin(){
        AppData.showProgress()
    let url = WebService.createURLForWebService(WebService.login)
        let param = ["email":self.email , "password":self.password]
   _ = NetworkManager.sharedManager().requestWithMethodTypeRegister(withObject: .post, url: url, parameters: param, isToken: false) { responce in
       if let res = responce as? [String:Any]{
           print(res)
           AppData.hideProgress()
           if let mod = Mapper<UserModel>().map(JSON: res){
               if let secret_code = mod.data?.secret_code{
                  
                       do {
                           try AppLocker.valet.setString("\(secret_code)", forKey: ALConstants.kPincode)
                           AppData.shared.saveUserInfo(res)
                           UserDefaults.standard.setValue(true, forKey: "isLoginDataSaved")
                           if let user = AppData.shared.getUserInfo(){
                               appUser = user
                               if let id = appUser?.data?.role_id{
                                   if id == 2{
                                       let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: PracticeDashboardVC.className) as! PracticeDashboardVC
                                       _ = self.navigationController?.pushViewController(nextVc, animated: true)
                                   }else{
                                       let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.ProfetionalDashboardStoryBord, identifire: ProfetionalDashboardVC.className) as! ProfetionalDashboardVC
                                       _ = self.navigationController?.pushViewController(nextVc, animated: true)
                                   }
                               }
                           }
                       }catch _ {}
                   } else{
                       do {
                           try AppLocker.valet.removeObject(forKey: ALConstants.kPincode)
                       }catch _ {}
                   AppDelegate.sharedAppDelegate.ChangeAppLockController(contoller: self, res: res)
                       
               }
           }
           
       }
    } failureHandler: { error in
        AppData.hideProgress()
        self.view.makeToast(error)
    }
        
    }
   
    
}

