//
//  RegisterOneVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 14/08/21.
//

import UIKit

class RegisterOneVC: UIViewController {

    @IBOutlet weak var btnProfetional:UIButton!
    @IBOutlet weak var btnPractice:UIButton!
    @IBOutlet weak var btnConTinue:UIButton!
    
    var isPractice:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        btnConTinue.addShadowLikeAndroidView()
        btnProfetional.addShadowLikeAndroidView()
        btnPractice.addShadowLikeAndroidView()
    }
    
    @IBAction func btnTypeTapped(_ sender:UIButton){
        isPractice = sender.tag == 1 ? "Practice" : "Profetional"
        if isPractice == "Practice"{
            btnPractice.backgroundColor = ConstColor.appLiteSky
            btnProfetional.backgroundColor = UIColor.white
            btnPractice.setTitleColor(UIColor.white, for: .normal)
            btnProfetional.setTitleColor(ConstColor.appFontRegular, for: .normal)
        }else{
            btnProfetional.backgroundColor = ConstColor.appLiteSky
            btnPractice.backgroundColor = UIColor.white
            btnProfetional.setTitleColor(UIColor.white, for: .normal)
            btnPractice.setTitleColor(ConstColor.appFontRegular, for: .normal)
        }
    }
    @IBAction func btnContinueTapped(_ sender:UIButton){
        if isPractice == ""{
            self.view.makeToast("Please select atleast one user type.")
        }else{
            if isPractice == "Practice"{
                isPrecticeUser = true
            let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.MainStoryBord, identifire: RegisterTwoVC.className) as! RegisterTwoVC
            _ = self.navigationController?.pushViewController(nextVc, animated: true)
            }else{
                isPrecticeUser = false
                let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.ProfetionalRegisterStoryBord, identifire: Profational_RegisterOneVC.className) as! Profational_RegisterOneVC
                _ = self.navigationController?.pushViewController(nextVc, animated: true)
            }
        }
    }
  
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }

}
