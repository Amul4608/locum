//
//  RegisterThreeVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 14/08/21.
//

import UIKit
import Alamofire

class RegisterThreeVC: UIViewController {

    @IBOutlet weak var vwPrectice:UIView!
    @IBOutlet weak var vwCQC:UIView!
    @IBOutlet weak var vwAddress:UIView!
    @IBOutlet weak var vwPostCode:UIView!
    @IBOutlet weak var vwCity:UIView!
    @IBOutlet weak var vwAddress2:UIView!
    @IBOutlet weak var vwCountry:UIView!
    @IBOutlet weak var btnContinue:UIButton!
    
    @IBOutlet weak var txtPrectice:UITextField!
    @IBOutlet weak var txtCQC:UITextField!
    @IBOutlet weak var txtAddress:UITextField!
    @IBOutlet weak var txtpostCode:UITextField!
    @IBOutlet weak var txtCity:UITextField!
    @IBOutlet weak var txtCountry:UITextField!
    @IBOutlet weak var txtAddress2:UITextField!
    
    
    var DicParam:Parameters = [:]
    var arrViews:[UIView] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        arrViews = [vwCQC , vwPrectice , vwPostCode , vwCity , vwAddress , vwCountry , vwAddress2]
        for i in arrViews{
            i.addShadowLikeAndroidView()
        }
        btnContinue.addShadowLikeAndroidView()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnContinueTapped(_ sender:UIButton){
        if validate(){
            DicParam["practice_name"] = txtPrectice.text!
            DicParam["cqc_number"] = txtCQC.text!
            DicParam["address"] = txtAddress.text!
            DicParam["address2"] = txtAddress2.text ?? ""
            DicParam["postcode"] = txtpostCode.text!
            DicParam["city"] = txtCity.text!
            DicParam["country"] = txtCountry.text!
            DicParam["role_id"] = "2"
            let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.MainStoryBord, identifire: RegisterMapVc.className) as! RegisterMapVc
            nextVc.DicParam = DicParam
            _ = self.navigationController?.pushViewController(nextVc, animated: true)
        }
        
    }
  
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func validate() -> Bool{
        if txtPrectice.text?.isEmpty == true{
            self.view.makeToast("Please enter prectice name")
            return false
        }else if txtCQC.text?.isEmpty == true{
            self.view.makeToast("Please enter CQC number")
            return false
        }else if txtAddress.text?.isEmpty == true{
            self.view.makeToast("Please enter address")
            return false
        }else if txtCity.text?.isEmpty == true{
            self.view.makeToast("Please enter city name")
            return false
        }else if txtpostCode.text?.isEmpty == true{
            self.view.makeToast("Please enter postcode")
            return false
        }else if txtCountry.text?.isEmpty == true{
            self.view.makeToast("Please enter country name")
            return false
        }
        return true
    }
    
    


}
extension RegisterThreeVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtPrectice{
            txtCQC.becomeFirstResponder()
            txtPrectice.resignFirstResponder()
        }else if textField == txtCQC{
            txtAddress2.becomeFirstResponder()
            txtCQC.resignFirstResponder()
        }else if textField == txtAddress{
            txtAddress2.becomeFirstResponder()
            txtAddress.resignFirstResponder()
        }else if textField == txtAddress2{
            txtCity.becomeFirstResponder()
            txtAddress2.resignFirstResponder()
        }else if textField == txtCity{
            txtpostCode.becomeFirstResponder()
            txtCity.resignFirstResponder()
        }else if textField == txtpostCode{
            txtCountry.becomeFirstResponder()
            txtpostCode.resignFirstResponder()
        }else if textField == txtCountry{
            txtCountry.resignFirstResponder()
            self.view.endEditing(true)
        }
        return true
    }
}
