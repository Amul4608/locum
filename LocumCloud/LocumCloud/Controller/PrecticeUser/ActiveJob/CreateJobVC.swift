//
//  CreateJobVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 15/08/21.
//

import UIKit
import ObjectMapper
import Alamofire

enum JobPickerType: String {
    case JobTitle     = "JobTitle"
    case JobType      = "JobType"
}
enum JobTimePickerType: String {
    case StartTime     = "StartTime"
    case EndTime       = "EndTime"
    case Date          = "Date"
}

//MARK: - Offset Process Delegate
protocol JobUpdateDelegate {
    func JobUpdate()
}

class CreateJobVC: UIViewController {

    var delegate:JobUpdateDelegate?
    @IBOutlet weak var vwHeadline:UIView!
    @IBOutlet weak var vwJobTitle:UIView!
    @IBOutlet weak var vwJobType:UIView!
    @IBOutlet weak var vwQualification:UIView!
    @IBOutlet weak var vwExperince:UIView!
    @IBOutlet weak var vwDate:UIView!
    @IBOutlet weak var vwStartTime:UIView!
    @IBOutlet weak var vwEndTime:UIView!
    @IBOutlet weak var vwDesc:UIView!
    @IBOutlet weak var vwJobLocation:UIView!
    @IBOutlet weak var vwJobHourly:UIView!
    
    
    @IBOutlet weak var txtHeadlineL:UITextField!
    @IBOutlet weak var txtjobTitle:UITextField!
    @IBOutlet weak var txtjobtype:UITextField!
    @IBOutlet weak var txtqualification:UITextField!
    @IBOutlet weak var txtexperience:UITextField!
    @IBOutlet weak var txtDate:UITextField!
    @IBOutlet weak var txtstartTime:UITextField!
    @IBOutlet weak var txtEndTime:UITextField!
    @IBOutlet weak var txtJobHourly:UITextField!
    @IBOutlet weak var txtJobLocation:UITextField!
    
    @IBOutlet weak var txtDesc:UITextView!
    @IBOutlet weak var btnSubmit:UIButton!
    
    
    var isFromEdit:Bool = false
    var jobDataModel:JobListDataModel?
    
    var picker: UIPickerView!
    var arrViews:[UIView] = []
    var arrJobTitle:[JobTitleDataModel] = []
    var arrJobType:[JobTypeDataModel] = []
    var selectedJobTitle:JobTitleDataModel?
    var selectedJobType:JobTypeDataModel?
    var edDate:String = ""
    var startTime:String = ""
    var endTime:String = ""
    var startTimeInDate:Date?
    var endTimeInDate:Date?
    var datePicker = UIDatePicker()
    var currntDatePicker:JobTimePickerType = .StartTime
    var currntJobPickerType:JobPickerType = .JobTitle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrViews = [vwDate , vwDesc , vwHeadline , vwExperince , vwQualification , vwJobType , vwEndTime , vwJobTitle , vwStartTime,vwJobLocation,vwJobHourly]
        for i in arrViews{
            i.addShadowLikeAndroidWithCorne(cornerRadius: 10)
        }
        let status = isFromEdit == true ? "Submit Job" : "Update Job"
        btnSubmit.setTitle(status, for: .normal)
        if isFromEdit == true{
            setData()
        }
        api_JobTitle()
        api_JobType()
    }
    
    @IBAction func btnBackTapped(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func setData(){
        if let obj = self.jobDataModel{
            txtHeadlineL.text = obj.headline
            txtexperience.text = obj.experience
            txtqualification.text = obj.qualification
            txtDesc.text = obj.description
            txtDate.text = getDisplayDate(date: obj.date ?? "", dateFormat: "yyyy-MM-dd", displayFormat: "yyyy-MM-dd")
            txtstartTime.text = getDisplayDate(date: obj.start_time ?? "", dateFormat: "HH:mm:ss", displayFormat: "hh:mm a")
            txtEndTime.text = getDisplayDate(date: obj.end_time ?? "", dateFormat: "HH:mm:ss", displayFormat: "hh:mm a")
            
            edDate = obj.date ?? ""
            startTime = obj.start_time ?? ""
            endTime =  obj.end_time ?? ""
            if let jbType = obj.job_type{
            txtjobtype.text = jbType.job_type
            selectedJobType = jbType
            }
            if let jbTitle = obj.job_title{
            txtjobTitle.text = jbTitle.job_title
            selectedJobTitle = jbTitle
            }
            txtJobHourly.text = obj.hourly_rate
            txtJobLocation.text = obj.location
            
        }
    }
    

}
extension CreateJobVC : UITextFieldDelegate{
    
  
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtEndTime{
            currntDatePicker = .EndTime
            showDatePicker(txtEndTime)
        }else if textField == txtstartTime{
            currntDatePicker = .StartTime
            showDatePicker(txtstartTime)
        }else if textField == txtDate{
            currntDatePicker = .Date
            showDatePicker(txtDate)
        }else if textField == txtjobtype{
            currntJobPickerType = .JobType
            showLPicker(label_title: "Select Job Type", textfiled: txtjobtype)
        }else if textField == txtjobTitle{
            currntJobPickerType = .JobTitle
            showLPicker(label_title: "Select Job Title", textfiled: txtjobTitle)
        }
    }
}
extension CreateJobVC{
    
        
    func showDatePicker(_ textField:UITextField){
        if currntDatePicker == .Date{
            datePicker.minimumDate = Date()
            datePicker.datePickerMode = .date
        }else{
            datePicker.minimumDate = Date()
            datePicker.datePickerMode = .time
        }
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
            //datePicker.maximumDate = futureDate
            //ToolBar
            var toolbar = UIToolbar()
            let rect = CGRect(origin: CGPoint(x: 0,y :self.view.frame.height/6), size: CGSize(width: self.view.frame.size.width, height: 40))
            toolbar = UIToolbar(frame: rect)
            toolbar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
            toolbar.isTranslucent = false
            toolbar.barStyle = UIBarStyle.default
            toolbar.tintColor = UIColor.white
            toolbar.barTintColor = UIColor(red: 105 / 255.0, green: 105 / 255.0, blue: 105 / 255.0, alpha: 1.0)
            
            let returnbutton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.cancel, target: self, action: #selector(self.cancelDatePicker))
            let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(self.donedatePicker))
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil)
            
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width / 3, height: self.view.frame.size.height))
            label.font = UIFont(name: "SF Pro Display Semibold", size: 14)
            label.backgroundColor = UIColor.clear
            label.textColor = UIColor.white
            label.text = textField.placeholder
            label.textAlignment = NSTextAlignment.center
            
            let textBtn = UIBarButtonItem(customView: label)
            toolbar.setItems([returnbutton,flexSpace,textBtn,flexSpace,doneButton], animated: false)
            //textfiled.inputAccessoryView = toolbar/
        if currntDatePicker == .StartTime{
            if let stdt = self.startTimeInDate{
                datePicker.date = stdt
            }
            if let dat = self.endTimeInDate{
                datePicker.maximumDate = dat
            }
        }else if currntDatePicker == .EndTime{
            if let stdt = self.endTimeInDate{
                datePicker.date = stdt
            }
            if let dat = self.startTimeInDate{
                datePicker.minimumDate = dat
                datePicker.maximumDate = dat.addDays(3333)
            }
        }
            txtstartTime.inputAccessoryView = toolbar
            txtstartTime.inputView = datePicker
            txtEndTime.inputAccessoryView = toolbar
            txtEndTime.inputView = datePicker
            txtDate.inputAccessoryView = toolbar
            txtDate.inputView = datePicker
        
        
        }
        
        @objc func donedatePicker(){
            let dateFormatter = DateFormatter()
            switch currntDatePicker {
            case .StartTime:
                dateFormatter.dateFormat = "hh:mm a"
                startTimeInDate = datePicker.date
                txtstartTime.text = dateFormatter.string(from: datePicker.date)
                dateFormatter.dateFormat = "HH:mm:ss"
                startTime = dateFormatter.string(from: datePicker.date)
            case .EndTime:
            dateFormatter.dateFormat = "hh:mm a"
                endTimeInDate = datePicker.date
            txtEndTime.text = dateFormatter.string(from: datePicker.date)
            dateFormatter.dateFormat = "HH:mm:ss"
            endTime = dateFormatter.string(from: datePicker.date)
            case .Date:
                dateFormatter.dateFormat = "yyyy-MM-dd"
                txtDate.text = dateFormatter.string(from: datePicker.date)
                dateFormatter.dateFormat = "yyyy-MM-dd"
                edDate = dateFormatter.string(from: datePicker.date)
            }
            
            
            
            self.view.endEditing(true)
            
        }
        
        @objc func cancelDatePicker(){
            self.view.endEditing(true)
        }
}

extension CreateJobVC : UIPickerViewDelegate  , UIPickerViewDataSource {
    func showLPicker(label_title: String ,textfiled: UITextField)
    {
        
        // let label_title = "Amul"
        picker = UIPickerView(frame: CGRect(x: 0, y: 200, width: self.view.bounds.width, height: 216))
        picker.backgroundColor = .white
        picker.delegate = self
        picker.dataSource = self
        
        var toolbar = UIToolbar()
        let rect = CGRect(origin: CGPoint(x: 0,y :self.view.frame.height/6), size: CGSize(width: self.view.frame.size.width, height: 40))
        toolbar = UIToolbar(frame: rect)
        toolbar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
        toolbar.isTranslucent = false
        toolbar.barStyle = UIBarStyle.default
        toolbar.tintColor = UIColor.white
        toolbar.barTintColor = UIColor(red: 105 / 255.0, green: 105 / 255.0, blue: 105 / 255.0, alpha: 1.0)
        
        let returnbutton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.cancel, target: self, action: #selector(self.cancelPicker))
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(self.donePicker))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil)
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width / 3, height: self.view.frame.size.height))
        label.font = UIFont(name: "Gotham", size: 11)
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.white
        label.text = label_title
        label.textAlignment = NSTextAlignment.center
        
        let textBtn = UIBarButtonItem(customView: label)
        toolbar.setItems([returnbutton,flexSpace,textBtn,flexSpace,doneButton], animated: false)
        //textfiled.inputAccessoryView = toolbar/
        txtjobtype.inputAccessoryView = toolbar
        txtjobtype.inputView = picker
        txtjobTitle.inputAccessoryView = toolbar
        txtjobTitle.inputView = picker
       
        
        switch currntJobPickerType {
        case .JobType:
            if let index = arrJobType.firstIndex(where: {$0.job_type == txtjobtype.text}){
                picker.selectRow(index, inComponent: 0, animated: false)
            }
        case .JobTitle:
            if let index = arrJobTitle.firstIndex(where: {$0.job_title == txtjobTitle.text}){
                picker.selectRow(index, inComponent: 0, animated: false)
            }
        }
        
    }
    
    
    @objc func donePicker(){
        print("done")
        let selectedRow = picker.selectedRow(inComponent: 0)
        switch currntJobPickerType {
        case .JobType:
            txtjobtype.text = arrJobType[selectedRow].job_type
            selectedJobType = arrJobType[selectedRow]
        case .JobTitle:
            txtjobTitle.text =  (arrJobTitle[selectedRow].job_title)
            selectedJobTitle = arrJobTitle[selectedRow]
       
        }
        
        self.view.endEditing(true)
    }
    
    @objc func cancelPicker(){
        
        self.view.endEditing(true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch currntJobPickerType {
        case .JobType:
            return arrJobType.count
        case .JobTitle:
            return  (arrJobTitle.count)
        
           
        }
       
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch currntJobPickerType {
        case .JobType:
            return arrJobType[row].job_type
            
        case .JobTitle:
            return (arrJobTitle[row].job_title)
        
        }
        
    }
    
    func api_JobTitle(){
        let url = WebService.createURLForWebService(WebService.JobTitles)
        AppData.showProgress()
        NetworkManager.hideNetworkLog()
        _ = NetworkManager.shared.requestWithMethodType(withObject: .get, url: url, parameters: nil,isToken: false, successHandler: { (response) in
            DispatchQueue.main.async(execute: {
                print("response \(response)")
                if let userInfo = response as? [String : Any] {
                    if let ard = Mapper<JobTitleDataModel>().mapArray(JSONObject: userInfo["data"]){
                        self.arrJobTitle = ard
                        AppData.hideProgress()
                        
                    }else{
                        AppData.hideProgress()
                    }
                }else{
                    AppData.hideProgress()
                }
                
                
            })
        }) { (errorString) in
            AppData.hideProgress()
            //SVProgressHUD.dismiss()
            //self.view.makeToast(errorString)
        }
    }
    
    func api_JobType(){
        let url = WebService.createURLForWebService(WebService.JobType)
        AppData.showProgress()
        NetworkManager.hideNetworkLog()
        _ = NetworkManager.shared.requestWithMethodType(withObject: .get, url: url, parameters: nil,isToken: false, successHandler: { (response) in
            DispatchQueue.main.async(execute: {
                print("response \(response)")
                if let userInfo = response as? [String : Any] {
                    if let ard = Mapper<JobTypeDataModel>().mapArray(JSONObject: userInfo["data"]){
                        self.arrJobType = ard
                        AppData.hideProgress()
                        
                    }else{
                        AppData.hideProgress()
                    }
                }else{
                    AppData.hideProgress()
                }
                
                
            })
        }) { (errorString) in
            AppData.hideProgress()
            //SVProgressHUD.dismiss()
            //self.view.makeToast(errorString)
        }
    }
    
    
    func isValidate() -> Bool {
        if txtHeadlineL.text?.isEmpty == true{
            self.view.makeToast("Please enter job headline")
            return false
        }else if txtjobTitle.text?.isEmpty == true{
            self.view.makeToast("Please select job title")
            return false
        }else if txtjobtype.text?.isEmpty == true{
            self.view.makeToast("Please select job type")
            return false
        }else if txtqualification.text?.isEmpty == true{
            self.view.makeToast("Please enter job qualification")
            return false
        }else if txtexperience.text?.isEmpty == true{
            self.view.makeToast("Please enter job experience")
            return false
        }else if txtJobHourly.text?.isEmpty == true{
            self.view.makeToast("Please enter job hourly price")
            return false
        }else if txtJobLocation.text?.isEmpty == true{
            self.view.makeToast("Please enter job location")
            return false
        }else if txtDate.text?.isEmpty == true{
            self.view.makeToast("Please select job date")
            return false
        }else if txtstartTime.text?.isEmpty == true{
            self.view.makeToast("Please select job starttime")
            return false
        }else if txtEndTime.text?.isEmpty == true{
            self.view.makeToast("Please select job endtime")
            return false
        }else if txtDesc.text?.isEmpty == true || txtDesc.text == "Write description here."{
            self.view.makeToast("Please enter job description")
            return false
        }
        return true
    }
    
    @IBAction func btnSubmitTapped(_ sender:UIButton){
        if isValidate(){
            var url = WebService.createURLForWebService(WebService.JobCreate)
            if self.isFromEdit == true{
                url = WebService.createURLForWebService(WebService.jobEdit) + "\(self.jobDataModel?.id ?? 0)"
            }
            var param:Parameters = [:]
            param["job_title_id"] = selectedJobTitle?.id ?? 0
            param["user_id"] = appUser?.data?.id ?? 0
            param["headline"] = txtHeadlineL.text!
            param["description"] = txtDesc.text!
            param["location"] = txtJobLocation.text!
            param["latitude"] = 0.0
            param["longitude"] = 0.0
            param["job_type_id"] = selectedJobType?.id ?? 0
            param["qualification"] = txtqualification.text!
            param["experience"] = txtexperience.text!
        
            param["date"] = txtDate.text!
            let stDate = getDisplayDate(date: txtstartTime.text!, dateFormat: "hh:mm a", displayFormat: "HH:mm:ss")
            param["start_time"] = stDate
            let edDate = getDisplayDate(date: txtEndTime.text!, dateFormat: "hh:mm a", displayFormat: "HH:mm:ss")
            param["end_time"] = edDate
            param["hourly_rate"] = txtJobHourly.text!
            AppDelegate.sharedAppDelegate.getCurrentLocation { location, Error in
                if location != nil{
                    param["latitude"] = "\(location?.coordinate.latitude ?? 0.0)"
                    param["longitude"] = "\(location?.coordinate.longitude ?? 0.0)"
                }
            }
            print("\(url) :- \(param)")
            AppData.showProgress()
            NetworkManager.showNetworkLog()
            
            _ = NetworkManager.shared.requestWithMethodType(withObject: .post, url: url, parameters: param,isToken: true, successHandler: { (response) in
                DispatchQueue.main.async(execute: {
                    
                    print("response \(response)")
                    
                    AppData.hideProgress()
                    if let userInfo = response as? [String : Any] {
                        if let message = userInfo["message"] as? String{
                            AppData.hideProgress()
                            AlertView.showAlert("", strMessage: message, button: ["Ok"], viewcontroller: self, blockButtonClicked: { (count) in
                                if count == 0
                                {
                                    AppData.hideProgress()
                                    self.delegate?.JobUpdate()
                                    self.dismiss(animated: true, completion: nil)
                                }
                                
                            }
                                
                            )}
                        
                    }
                    
                })
            }) { (errorString) in
                AppData.hideProgress()
                self.view.makeToast(errorString)
                //AlertView.showOKTitleAlert(errorString, viewcontroller: self)
            }
        }
    }
    
    
}
extension CreateJobVC :UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtDesc.text == "Write description here."{
            txtDesc.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtDesc.text == ""{
            txtDesc.text = "Write description here."
        }
    }
}
