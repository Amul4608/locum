//
//  Job_LocationVC.swift
//  AgencyUser
//
//  Created by Amul Patel on 30/11/20.
//  Copyright © 2020 Amul Patel. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps
import CoreLocation
import GooglePlaces
import Alamofire
import ObjectMapper

//MARK: - App Delegate
let APP_DELEGATE                =   UIApplication.shared.delegate as! AppDelegate

class Job_LocationVC: UIViewController {

    @IBOutlet weak var vwHeadline:UIView!
    @IBOutlet weak var tblLocation:UITableView!
    @IBOutlet weak var HeightOFTblAddressView:NSLayoutConstraint!
   
   
    
    var locationManager:CLLocationManager!
    @IBOutlet weak var txtSearchED:UITextField!
    var fullAddress:String = ""
    var lat: Double = 0.0
    var lon: Double = 0.0
    
    //Double
    var currentLatitude:Double = 0.0
    var currentLongitude:Double = 0.0
    var destLatitude:Double = 0.0
    var destLongitude:Double = 0.0
    var isPreviousSelected:Bool = false
    var fromLatitude:Double = 0.0
    var fromLongitude:Double = 0.0
    var originLatitude:Double = 0.0
    var originLongitude:Double = 0.0
    var pickupCity = ""
    var pickupCountry = ""
    //var delegate:SelectLocation?
    var arrFirstLocation:[NSDictionary] = []
    var arrPrevLocation:[String:Any] = [:]
    var isSearch = false
    let geoCoder = CLGeocoder()
    //GMSMapView
    @IBOutlet var MapView: GMSMapView!
    //@IBOutlet weak var vwCurrentLocation:UIView!
    //GMSMarker
    var currentLocationMarker = GMSMarker() //GMSMarker?
    var destinationMarker = GMSMarker()
 
    var currentLocation = CLLocation()
    var nearbylocation:[NSDictionary] = []
    var dic:[String:Any] = [:]
    
    
    //UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI(){
        tblLocation.register(UINib(nibName: SelectUser_tblCell.className, bundle: nil), forCellReuseIdentifier: SelectUser_tblCell.className)
       
        MapView.delegate = self
        initLocationManager()
        APP_DELEGATE.getCurrentLocation { (locationmMnager, Error) in
            if locationmMnager != nil
            {
                self.lat = (locationmMnager?.coordinate.latitude)!
                self.lon = (locationmMnager?.coordinate.longitude)!
                self.getlaqtLongToAddressApi(lat: "\(self.lat)", long: "\(self.lon)", isCurrntBtn: false)
            }
            
        }
    }
    func Calculate_SerchViewHeight(count:Int)
    {
        HeightOFTblAddressView.constant = CGFloat(count * 35 )
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    

}
// MARK:- Location manager methods
extension Job_LocationVC: CLLocationManagerDelegate , GMSMapViewDelegate
{
    func initLocationManager()
    {
        self.locationManager = CLLocationManager()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.distanceFilter = 10
        self.locationManager.startUpdatingLocation()
        self.locationManager.delegate = self
        
        MapView.delegate = self
        MapView.clear()
        MapView.isMyLocationEnabled = true
        MapView.settings.compassButton = false
        MapView.settings.myLocationButton = false
        
            MapView.settings.setAllGesturesEnabled(true)
       
        if(self.locationManager.responds(to: #selector(CLLocationManager.requestWhenInUseAuthorization))){
            let status = CLLocationManager.authorizationStatus()
            if(status == CLAuthorizationStatus.notDetermined) {
                self.locationManager.requestWhenInUseAuthorization();
            }
        }
        //self.setMapstyle()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        if CLLocationManager.locationServicesEnabled() {
            startMonitoringLocation()
            if isPreviousSelected == false{
            addCurrentLocationMarker()
                
            }else{
                var lati:String = ""
                var long:String = ""
                if let latitude = arrPrevLocation["lat"] as? String{
                   lati = latitude
                }
                if let longitude = arrPrevLocation["lon"] as? String{
                   long = longitude
                }
                
                addSelectedLocation(lati: lati, long: long)
            }
        }
    }
    
    func startMonitoringLocation() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.activityType = CLActivityType.automotiveNavigation
            locationManager.distanceFilter = 1
            locationManager.headingFilter = 1
            locationManager.requestWhenInUseAuthorization()
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
        }
    }
    
    func stopMonitoringLocation() {
        locationManager.stopMonitoringSignificantLocationChanges()
        locationManager.stopUpdatingLocation()
    }
    func addSelectedLocation(lati:String , long:String){
        
        if  lati != ""
        {
            if  long != ""
            {
                //let marker:GMSMarker = GMSMarker()
                currentLocationMarker.position.latitude = Double(lati)!
                currentLocationMarker.position.longitude = Double(long)!
                currentLocationMarker.icon = UIImage(named: "Map_icon_Blue")
                currentLocationMarker.map = self.MapView
                self.getlaqtLongToAddressApi(lat: lati, long: long, isCurrntBtn: false)
                let camera = GMSCameraPosition.camera(withLatitude: Double(lati)!, longitude: Double(long)!, zoom: 14)
                MapView?.camera = camera
                MapView?.animate(to: camera)
                //marker.snippet = "My Location"
                //MapView.selectedMarker=marker
                currentLocationMarker.isDraggable = true
                //MapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: self.vwLocation.frame.height + 15, right: 0)
                
            }
        }
        
       
       
    }
    
    func addCurrentLocationMarker() {
        currentLocationMarker.map = nil
        //currentLocationMarker = nil
        if let location = locationManager.location {
            currentLocationMarker = GMSMarker(position: location.coordinate)
            
            currentLocationMarker.icon = UIImage(named: "Map_icon_Blue")
            currentLocationMarker.map = MapView
           // currentLocationMarker.rotation
            currentLocationMarker.isDraggable = true
            //currentLocationMarker.rotation = locationManager.location?.course ?? 0
            if !isSearch {
                self.updateAddress()
            }
        }
    }
    
    func zoomToCoordinates(_ coordinates: CLLocationCoordinate2D) {
        let camera = GMSCameraPosition.camera(withLatitude: coordinates.latitude, longitude: coordinates.longitude, zoom: 14)
        MapView.camera = camera
    }
    
    //MARK:- Location Manager Delegate
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location manager erroe -> \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            break
        case .restricted:
            break
        case .denied:
            stopMonitoringLocation()
            break
        default:
            if isPreviousSelected == false{
                addCurrentLocationMarker()
                
            }
            startMonitoringLocation()
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let lastLocation = locations.last {
            currentLocationMarker.position = lastLocation.coordinate
            currentLocationMarker.rotation = lastLocation.course
            if isPreviousSelected == false{
            self.zoomToCoordinates(lastLocation.coordinate)
            }else{
                var lati:String = ""
                var long:String = ""
                if let latitude = arrPrevLocation["lat"] as? String{
                   lati = latitude
                }
                if let longitude = arrPrevLocation["lon"] as? String{
                   long = longitude
                }
                
                addSelectedLocation(lati: lati, long: long)
            }
            locationManager.stopUpdatingLocation()
            self.stopMonitoringLocation()
            
        }
    }
    
    
    
    
    func updateAddress(){
        //if currentLocationMarker != nil {
        
            let coordinate = CLLocationCoordinate2DMake(Double((currentLocationMarker.position.latitude)),Double((currentLocationMarker.position.longitude)))
            
            self.currentLatitude = coordinate.latitude
            self.currentLongitude = coordinate.longitude
            self.fromLongitude = self.currentLongitude
            self.fromLatitude = self.currentLatitude
            
       // }
    }
    
    func updateAddressforDest(){
        //if currentLocationMarker != nil {
            let geocoder = GMSGeocoder()
            
            let coordinate = CLLocationCoordinate2DMake(Double((currentLocationMarker.position.latitude)),Double((currentLocationMarker.position.longitude)))
            var currentAddress = String()
            
            geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
                if let address = response?.firstResult() {
                    let lines = address.lines! as [String]
                    
                    currentAddress = lines.joined(separator: "\n")
                    print(currentAddress)
                    self.destLatitude = coordinate.latitude
                    self.destLongitude = coordinate.longitude
                    
                    print("Current Add \(currentAddress)")
                    
                    
                    
                }
            //}
        }
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
        self.updateAddress()
        
    }
    
   
    func setMapstyle(){
        //Mapstyle
        do {
            // Set the map style by passing the URL of the local file. Make sure style.json is present in your project
            if let styleURL = Bundle.main.url(forResource: "Mapstyle", withExtension: "json") {
                self.MapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                print("Unable to find style.json")
            }
        } catch {
            print("The style definition could not be loaded: \(error)")
        }
    }
    
    
    
}
extension Job_LocationVC : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            print(tableView)
            
                return arrFirstLocation.count
            
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            print(tableView)
           
                let cell = tblLocation.dequeueReusableCell(withIdentifier: "SelectUser_tblCell") as! SelectUser_tblCell
                let obj = arrFirstLocation[indexPath.row]
                
                if txtSearchED.text?.isEmpty == false
                {
                    if let terms = obj.value(forKey: "structured_formatting") as? NSDictionary {
                        var textLabel = ""
                        if let text = terms.value(forKey: "main_text") as? String
                        {
                            textLabel = text
                        }
                        
                        if let text2 = terms.value(forKey: "secondary_text") as? String
                        {
                            textLabel = textLabel + " " + text2
                        }
                        cell.lblName.text = textLabel
                    }
                    
                    
                }
                else
                {
                    if let location = obj["formatted_address"] as? String
                    {
                        if let objGeo = obj["address_components"] as? [NSDictionary]
                        {
                            for data in objGeo
                            {
    
                                    let locality = data.value(forKey: "long_name") as? String
                                    let FinalText = location.components(separatedBy: locality!)
                                    cell.lblName.text = FinalText.first
                                   // cell.lblSecondAddress.text = location.components(separatedBy: FinalText.first!).last
                               // }
                            }
                            
                        }
                        
                    }
                }
                
                return cell
           
        }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 35
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(arrFirstLocation[indexPath.row])
        let obj = arrFirstLocation[indexPath.row]
        
        if txtSearchED.text?.isEmpty == false
        {
           
           if let terms = obj.value(forKey: "structured_formatting") as? NSDictionary {
              
               if let text = terms.value(forKey: "main_text") as? String
               {
                self.dic["location"] = text
                   
               }
           }
            self.apiForLAtLONG(obj.value(forKey: "reference") as! String, address: self.dic)
        }else{
                 if let location = obj["formatted_address"] as? String
                {
                    
                    self.dic["location"] = location
                    self.dic["latitude"] = "\(self.lat)"
                    self.dic["longitude"] = "\(self.lon)"
                    if let geometry = obj["geometry"] as? [String:Any]
                    {
                        if let geometry_location = geometry["location"] as? [String:Any]
                        {
                            if let latitude = geometry_location["lat"] as? Double{
                                self.dic["latitude"] = "\(latitude)"
                            }
                            if let lng = geometry_location["lng"] as? Double{
                                self.dic["longitude"] = "\(lng)"
                            }
                        }
                    }
                    
                    print(dic)
                    self.gonext()
                }
           
        }
    }
}

extension Job_LocationVC
{
    func getlaqtLongToAddressApi(lat: String, long:String , isCurrntBtn:Bool) {
        
        let queue = DispatchQueue.global(qos: .default)
        queue.async(execute: {() -> Void in
            
            var request = URLRequest(url: URL(string:"https://maps.googleapis.com/maps/api/geocode/json?latlng=\(lat),\(long)&key=AIzaSyBi7kbgkFiUUkVnmwb7aMnbi5ua2Cpc6-M&sensor=true")!)
            
            
            request.httpMethod = "GET"
            
            //SET REQUEST PARAMETER HERE
            let postString = ""
            
            request.httpBody = postString.data(using: .utf8)
            
            let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
                if error != nil {
                    return
                }
                DispatchQueue.main.sync(execute: {() -> Void in
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                        print(json)
                        if json.count>0{
                            var isFristTime:Bool = false
                            if self.arrFirstLocation.count <= 0{
                                isFristTime = true
                                self.nearbylocation = []
                            }
                            self.arrFirstLocation.removeAll()
                            let result = json.value(forKey: "results") as! NSArray
                            if result.count>0{
                                
                               for data in result
                               {
                                   let obj = data as! NSDictionary
                                   if self.arrFirstLocation.count >= 0 && self.arrFirstLocation.count < 3
                                   {
                                    if isFristTime == true{
                                        self.nearbylocation.append(obj)
                                    }
                                       self.arrFirstLocation.append(obj)
                                    
                                      
                                   }
                               }
                            self.Calculate_SerchViewHeight(count: self.arrFirstLocation.count)
                               self.tblLocation.reloadData()
                               self.tblLocation.layoutIfNeeded()
                               self.view.layoutIfNeeded()
                               self.viewDidLayoutSubviews()
                                
                            }else{
                                self.Calculate_SerchViewHeight(count: self.arrFirstLocation.count)
                                self.tblLocation.reloadData()
                                self.tblLocation.layoutIfNeeded()
                                self.view.layoutIfNeeded()
                                self.viewDidLayoutSubviews()
                            }
                        }
                    }
                    catch let error as NSError {
                        
                        print(error)
                    }
                })
            }
            task.resume()
        })
    }
    
    func apiCallForAddress(str:String)
    {
         var gurl = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(str)&sensor=true&key=AIzaSyBi7kbgkFiUUkVnmwb7aMnbi5ua2Cpc6-M"
        
        gurl = (gurl as NSString).addingPercentEscapes(using: String.Encoding.utf8.rawValue) ?? ""
        
        Alamofire.request(gurl, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result
            {
            case .success :
                  print(response.result.value)
                if let res =  response.result.value as? [String:Any]
                {
                    if let data = res["predictions"] as? [NSDictionary]
                    {
                        self.arrFirstLocation.removeAll()
                        for val in data{
                        if  self.arrFirstLocation.count < 3{
                            self.arrFirstLocation.append(val)
                        }
                        }
                        //self.arrFirstLocation = data
                        self.Calculate_SerchViewHeight(count: self.arrFirstLocation.count)
                        self.tblLocation.reloadData()
                        self.tblLocation.layoutIfNeeded()
                        self.view.layoutIfNeeded()
                        self.viewDidLayoutSubviews()
                    }
                }
            case .failure(let error):
                print(error)
                self.tblLocation.reloadData()
            }
        }
        
    }
    func apiForLAtLONG(_ esc_addr:String , address:[String:Any])
    {
        let req = "https://maps.googleapis.com/maps/api/place/details/json?sensor=false&reference=\(esc_addr)&key=AIzaSyBi7kbgkFiUUkVnmwb7aMnbi5ua2Cpc6-M"
        Alamofire.request(req, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result
            {
            case .success :
                if let res =  response.result.value as? [String:Any]
                {
                    if let data = res["result"] as? [String:Any]
                    {
                        if let geometry = data["geometry"] as? [String:Any]
                        {
                            if let location = geometry["location"] as? [String:Any]
                            {
                                if let lati = location["lat"] as? Double
                                {
                                    self.lat = lati
                                }
                                if let longi = location["lng"] as? Double
                                {
                                    self.lon = longi
                                }
                                self.dic["latitude"] = "\(self.lat)"
                                self.dic["longitude"] = "\(self.lon)"
                                print(self.dic)
                                self.gonext()
                            }
                        }
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        let getLat: CLLocationDegrees = marker.position.latitude
        let getLon: CLLocationDegrees = marker.position.longitude
        let markerLocation: CLLocation =  CLLocation(latitude: getLat, longitude: getLon)
        currentLocationMarker.position.latitude = getLat
        currentLocationMarker.position.longitude = getLon
        let geocoder = CLGeocoder();
        
        geocoder.reverseGeocodeLocation(markerLocation, completionHandler: { (placemarks, error) -> Void in
            
            if error != nil
            {
                print(error?.localizedDescription)
            }
            else
            {
                if placemarks != nil
                {
                    let placeMark:CLPlacemark = placemarks![0];
                    self.getlaqtLongToAddressApi(lat: "\(placeMark.location?.coordinate.latitude ?? 0.0)", long: "\(placeMark.location?.coordinate.longitude ?? 0.0)", isCurrntBtn: false)
                }
            }
        })
    }
    
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        let getLat: CLLocationDegrees = marker.position.latitude
        let getLon: CLLocationDegrees = marker.position.longitude
        let markerLocation: CLLocation =  CLLocation(latitude: getLat, longitude: getLon)
        currentLocationMarker.position.latitude = getLat
        currentLocationMarker.position.longitude = getLon
        
        let geocoder = CLGeocoder();
        geocoder.reverseGeocodeLocation(markerLocation, completionHandler: { (placemarks, error) -> Void in
            if error != nil
            {
                print(error?.localizedDescription)
            }
            else
            {
                if placemarks != nil
                {
                    let placeMark:CLPlacemark = placemarks![0];
                    self.getlaqtLongToAddressApi(lat: "\(placeMark.location?.coordinate.latitude ?? 0.0)", long: "\(placeMark.location?.coordinate.longitude ?? 0.0)", isCurrntBtn: false)
                }
            }
        })
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D)
    {
        MapView.clear()
        currentLocationMarker.map = nil
        currentLocationMarker = GMSMarker(position: coordinate)
        currentLocationMarker.position.latitude = coordinate.latitude
        currentLocationMarker.position.longitude = coordinate.longitude
        let ULlocation = currentLocationMarker.position.latitude
        let ULlgocation = currentLocationMarker.position.longitude
        print(ULlocation)
        print(ULlgocation)
        currentLocationMarker.icon = #imageLiteral(resourceName: "mapPin")
        currentLocationMarker.map = self.MapView
        currentLocationMarker.isDraggable = true
        self.getlaqtLongToAddressApi(lat: "\(ULlocation)", long: "\(ULlgocation)", isCurrntBtn: false)
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        
        
        
        let getLat: CLLocationDegrees = marker.position.latitude
        let getLon: CLLocationDegrees = marker.position.longitude
        
        let markerLocation: CLLocation =  CLLocation(latitude: getLat, longitude: getLon)
        currentLocationMarker.position.latitude = getLat
        currentLocationMarker.position.longitude = getLon
        
        
        let geocoder = CLGeocoder();
        
        geocoder.reverseGeocodeLocation(markerLocation, completionHandler: { (placemarks, error) -> Void in
            
            if error != nil
            {
                print(error?.localizedDescription)
            }
            else
            {
                if placemarks != nil
                {
                    let placeMark:CLPlacemark = placemarks![0];
                    self.getlaqtLongToAddressApi(lat: "\(placeMark.location?.coordinate.latitude ?? 0.0)", long: "\(placeMark.location?.coordinate.longitude ?? 0.0)", isCurrntBtn: false)
                }
            }
        })
    }
    
}

extension Job_LocationVC : UITextFieldDelegate , UITextViewDelegate
{
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            if txtSearchED.text?.trimSpace.count != 0
            {
                self.apiCallForAddress(str: textField.text!)
            }else{
                self.arrFirstLocation = self.nearbylocation
                self.tblLocation.reloadData()
        }
        
        return true
    }
    @IBAction func editing_Change(_ sender: UITextField) {
           if txtSearchED.text?.trimSpace.count != 0
           {
            self.apiCallForAddress(str: txtSearchED.text!.trimSpace)
           }else{
            self.arrFirstLocation = self.nearbylocation
            self.Calculate_SerchViewHeight(count: self.arrFirstLocation.count)
            self.tblLocation.reloadData()
            self.tblLocation.layoutIfNeeded()
            self.view.layoutIfNeeded()
            self.viewDidLayoutSubviews()
        }
       }
}

//MARK:- IBAction

extension Job_LocationVC{
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnNextTapped(_ sender:UIButton){
       gonext()
    }
    func gonext(){
        if let add = dic["location"] as? String , add != ""{
        //let nextVC = Utilities.getViewControllerFromStoryboard(StoryBoardName.PostJobStoryBord, identifire: Job_DescriptionVC.className) as! Job_DescriptionVC
          //  nextVC.dic = self.dic
        //_ = self.navigationController?.pushViewController(nextVC, animated: true)
        }else{
            self.view.makeToast("Please select location")
        }
    }
}



extension UITextView {

    func addDoneButton(title: String, target: Any, selector: Selector) {

        let toolBar = UIToolbar(frame: CGRect(x: 0.0,
                                              y: 0.0,
                                              width: UIScreen.main.bounds.size.width,
                                              height: 44.0))//1
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)//2
        let barButton = UIBarButtonItem(title: title, style: .plain, target: target, action: selector)//3
        toolBar.setItems([flexible, barButton], animated: false)//4
        self.inputAccessoryView = toolBar//5
    }
}
