//
//  ActiveJobVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 15/08/21.
//

import UIKit
import ObjectMapper
import Alamofire

class ActiveJobVC: BaseViewController {

    @IBOutlet weak var tblList:UITableView!
    
    @IBOutlet weak var heightOFTbl:NSLayoutConstraint!
    var arrList:[JobListDataModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    func setupUI(){
        tab?.resetAll()
        tab?.btnFavourite.setImage(UIImage(named: "active-jobs-active.png"), for: .normal)
        tblList.register(UINib(nibName: JobListCell.className, bundle: nil), forCellReuseIdentifier: JobListCell.className)
        apicall(id: "\(appUser?.data?.id ?? 0)")
       
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        heightOFTbl.constant = CGFloat(arrList.count * 127)
    }
    
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnCreateJobTapped(_ sender:UIButton){
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: CreateJobVC.className) as! CreateJobVC
        nextVc.delegate = self
        self.present(nextVc, animated: true, completion: nil)
       // _ = self.navigationController?.pushViewController(nextVc, animated: true)
    }


}

extension ActiveJobVC : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: JobListCell.className) as! JobListCell
        cell.obj = arrList[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 127
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: JobDetailVC.className) as! JobDetailVC
        nextVc.jobID = arrList[indexPath.row].id ?? 0
        nextVc.delegate = self
        nextVc.jobDataModel = arrList[indexPath.row]
        _ = self.navigationController?.pushViewController(nextVc, animated: true)
    }
}


extension ActiveJobVC{
    
    func apicall(id:String){
            let url = WebService.createURLForWebService(WebService.practice_my_job)
            AppData.showProgress()
            NetworkManager.showNetworkLog()
            _ = NetworkManager.shared.requestWithMethodType(withObject: .post, url: url, parameters: ["user_id":id],isToken: true, successHandler: { (response) in
                 DispatchQueue.main.async(execute: {
                     print("response \(response)")
                     if let userInfo = response as? [String : Any] {
                         
                         if let ard = Mapper<JobListDataModel>().mapArray(JSONObject: userInfo["data"]){
                             self.arrList = ard
                            self.tblList.reloadData()
                            checkRecordAvailable(for: self.tblList, count: self.arrList.count, ctrlRefresh: nil, targetController: self, displayMessage:"No Record Found!")
                             self.heightOFTbl.constant = CGFloat(self.arrList.count * 127)
                         }
                     }
                     
                     AppData.hideProgress()
                 })
             }) { (errorString) in
                AppData.hideProgress()
                 self.view.makeToast(errorString)
             }
        }
   

}


extension ActiveJobVC : JobUpdateDelegate{
    func JobUpdate() {
        apicall(id: "\(appUser?.data?.id ?? 0)")
    }
    
    
}
