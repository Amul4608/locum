//
//  CalendarVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 15/08/21.
//

import UIKit

class CalendarVC: BaseViewController {

    @IBOutlet weak var tblList:UITableView!
    @IBOutlet weak var heightOFTbl:NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    func setupUI(){
        self.tab?.resetAll()
        self.tab?.btnCalender.setImage(UIImage(named: "create-job-active.png"), for: .normal)
        tblList.register(UINib(nibName: JobListCell.className, bundle: nil), forCellReuseIdentifier: JobListCell.className)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        heightOFTbl.constant = tblList.contentSize.height
    }
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCreateJobTapped(_ sender:UIButton){
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: CreateJobVC.className) as! CreateJobVC
        self.present(nextVc, animated: true, completion: nil)
        //_ = self.navigationController?.pushViewController(nextVc, animated: true)
    }
    
}


extension CalendarVC : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: JobListCell.className) as! JobListCell
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 122
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: JobDetailVC.className) as! JobDetailVC
        _ = self.navigationController?.pushViewController(nextVc, animated: true)
    }
}
