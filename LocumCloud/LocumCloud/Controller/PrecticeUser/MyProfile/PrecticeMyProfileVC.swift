//
//  PrecticeMyProfileVC.swift
//  LocumCloud
//
//  Created by AmulCGM on 15/08/21.
//

import UIKit

class PrecticeMyProfileVC: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tab?.resetAll()
        self.tab?.btnProfile.setImage(UIImage(named: "Group 4621.png"), for: .normal)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackTapped(_ sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCardTapped(_ sender:UIButton){
        if sender.tag == 0{
            let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: InvoiceVc.className) as! InvoiceVc
            _ = self.navigationController?.pushViewController(nextVc, animated: true)
        }else if sender.tag == 1{
            let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.PracticeDashboardStoryBord, identifire: FeedBackVC.className) as! FeedBackVC
            _ = self.navigationController?.pushViewController(nextVc, animated: true)
        }else if sender.tag == 2{
            
        }else if sender.tag == 3{
           
        }else if sender.tag == 4{
            AlertView.showAlert("", strMessage: "Are you sure want to logout?", button: ["Logout" , "Cancel"], viewcontroller:self) { btn in
                if btn == 0{
                    appUser = nil
                    UserDefaults.standard.setValue(false, forKey: "isLoginDataSaved")
                    if let viewControllers = self.navigationController?.viewControllers {
                           for vc in viewControllers {
                                if vc.isKind(of: FirstLoginVC.classForCoder()) {
                                     print("It is in stack")
                                     //Your Process
                                    self.navigationController?.popToViewController(vc, animated: true)
                                    return
                                }
                           }
                        let nextVc = Utilities.getViewControllerFromStoryboard(StoryBoardName.MainStoryBord, identifire: FirstLoginVC.className) as! FirstLoginVC
                        _ = self.navigationController?.pushViewController(nextVc, animated: false)
                     }
                }
            }
        }
    }
    


}
