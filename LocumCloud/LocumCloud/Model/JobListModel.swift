//
//  JobListModel.swift
//  LocumCloud
//
//  Created by Amul Patel on 08/10/21.
//

import Foundation
import ObjectMapper

class JobListModel: Mappable {
    var message : String?
    var data : [JobListDataModel]?
    var status_code : Int?

    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        message <- map["message"]
        data <- map["data"]
        status_code <- map["status_code"]
    }
}

class JobListDataModel: Mappable {
    var id : Int?
    var headline : String?
    var job_title_id : Int?
    var job_type_id : Int?
    var qualification : String?
    var experience : String?
    var date : String?
    var start_time : String?
    var end_time : String?
    var hourly_rate : String?
    var location : String?
    var user_id : Int?
    var description : String?
    var latitude : Int?
    var longitude : Int?
    var job_rating : String?
    var job_review : String?
    var status : Int?
    var created_at : String?
    var updated_at : String?
    var total_applied_users : Int?
    var job_type : JobTypeDataModel?
    var job_title : JobTitleDataModel?
    var job_apply :[JobApply_UserData]?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        job_apply <- map["job_apply"]
        job_type <- map["job_type"]
        job_title <- map["job_title"]
        id <- map["id"]
        headline <- map["headline"]
        job_title_id <- map["job_title_id"]
        job_type_id <- map["job_type_id"]
        qualification <- map["qualification"]
        experience <- map["experience"]
        date <- map["date"]
        start_time <- map["start_time"]
        end_time <- map["end_time"]
        hourly_rate <- map["hourly_rate"]
        location <- map["location"]
        user_id <- map["user_id"]
        description <- map["description"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        job_rating <- map["job_rating"]
        job_review <- map["job_review"]
        status <- map["status"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        total_applied_users <- map["total_applied_users"]
    }
}



class JobTitleDataModel : Mappable{
    var id : Int?
    var job_title : String?
    required init?(map: Map)
    {
    }
    func mapping(map: Map)
    {
        id <- map["id"]
        job_title <- map["job_title"]
    }
}


class JobTypeDataModel : Mappable{
    var id : Int?
    var job_type : String?
    required init?(map: Map)
    {
    }
    func mapping(map: Map)
    {
        id <- map["id"]
        job_type <- map["job_type"]
    }
}
