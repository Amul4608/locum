//
//  LoginModel.swift
//  247 Zay
//
//  Created by AmulCGM on 04/05/21.
//

import Foundation
import ObjectMapper


class UserModel:  NSObject, Mappable, NSCoding {
    
    var data : UserDataModel?
    var token : String?
    var message : String?
    var status_code : Int?
    
    
    
    
    //==========================================
    //MARK: - Initializer Method
    //==========================================
    required init?(map: Map)
    {
        
    }
    //==========================================
    //MARK: - NSCoder Initializer Methods
    //==========================================
    required init?(coder aDecoder: NSCoder)
    {
        self.data                   = aDecoder.decodeObject(forKey: "data") as? UserDataModel
        self.token                  = aDecoder.decodeObject(forKey: "token") as? String
        self.message                = aDecoder.decodeObject(forKey: "message") as? String
        self.status_code            = aDecoder.decodeObject(forKey: "status_code") as? Int
       
        
       
    }
    func encode(with aCoder: NSCoder)
    {
        aCoder.encode(data, forKey: "data")
        aCoder.encode(token, forKey: "token")
        aCoder.encode(message, forKey: "message")
        aCoder.encode(status_code, forKey: "status_code")
        
        
        
    }
    //==========================================
    //MARK: - Mappable Protocol Methods
    //==========================================
    func mapping(map: Map)
    {
        data <- map["data"]
        token <- map["token"]
        message <- map["message"]
        status_code <- map["status_code"]
       
    }
    
}


class UserDataModel:  NSObject, Mappable, NSCoding {
    
    var id : Int?
    var first_name : String?
    var last_name : String?
    var email : String?
    var username : String?
    var address : String?
    var address2:String?
    var postal_code : String?
    var city : String?
    var role_id : Int?
    var country : String?
    var latitude : Double?
    var longitude : Double?
    var cqc_number : String?
    var user_image : String?
    var practice : String?
    var email_verified_at : String?
    var created_at : String?
    var updated_at : String?
    var status : Int?
    var created_by:Int?
    var is_manager:Bool?
    var public_image:String?
    var secret_code:Int?
    
    
    
    
    //==========================================
    //MARK: - Initializer Method
    //==========================================
    required init?(map: Map)
    {
        
    }
    //==========================================
    //MARK: - NSCoder Initializer Methods
    //==========================================
    required init?(coder aDecoder: NSCoder)
    {
        self.id                   = aDecoder.decodeObject(forKey: "id") as? Int
        self.first_name                   = aDecoder.decodeObject(forKey: "first_name") as? String
        self.email                   = aDecoder.decodeObject(forKey: "email") as? String
        self.last_name                   = aDecoder.decodeObject(forKey: "last_name") as? String
        self.address2               = aDecoder.decodeObject(forKey: "address2") as? String
        self.username                   = aDecoder.decodeObject(forKey: "username") as? String
        self.address                   = aDecoder.decodeObject(forKey: "address") as? String
        self.postal_code                   = aDecoder.decodeObject(forKey: "postal_code") as? String
        self.city                   = aDecoder.decodeObject(forKey: "city") as? String
        self.role_id                   = aDecoder.decodeObject(forKey: "role_id") as? Int
        self.country                   = aDecoder.decodeObject(forKey: "country") as? String
        self.latitude                   = aDecoder.decodeObject(forKey: "latitude") as? Double
        self.longitude                     = aDecoder.decodeObject(forKey: "longitude") as? Double
        self.cqc_number                     = aDecoder.decodeObject(forKey: "cqc_number") as? String
        self.user_image                     = aDecoder.decodeObject(forKey: "user_image") as? String
        self.practice                     = aDecoder.decodeObject(forKey: "practice") as? String
        self.email_verified_at                     = aDecoder.decodeObject(forKey: "email_verified_at") as? String
        self.created_at                     = aDecoder.decodeObject(forKey: "created_at") as? String
        self.updated_at                     = aDecoder.decodeObject(forKey: "updated_at") as? String
        self.status                     = aDecoder.decodeObject(forKey: "status") as? Int
        self.created_by                     = aDecoder.decodeObject(forKey: "created_by") as? Int
        self.is_manager                     = aDecoder.decodeObject(forKey: "is_manager") as? Bool
        self.public_image                   = aDecoder.decodeObject(forKey: "public_image") as? String
        self.secret_code                    = aDecoder.decodeObject(forKey: "secret_code") as? Int
    }
    func encode(with aCoder: NSCoder)
    {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(first_name, forKey: "first_name")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(address2, forKey: "address2")
        aCoder.encode(last_name, forKey: "last_name")
        aCoder.encode(username, forKey: "username")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(postal_code, forKey: "postal_code")
        aCoder.encode(city, forKey: "city")
        aCoder.encode(role_id, forKey: "role_id")
        aCoder.encode(country, forKey: "country")
        aCoder.encode(latitude, forKey: "latitude")
        aCoder.encode(longitude, forKey: "longitude")
        aCoder.encode(cqc_number, forKey: "cqc_number")
        aCoder.encode(user_image, forKey: "user_image")
        aCoder.encode(practice, forKey: "practice")
        aCoder.encode(email_verified_at, forKey: "email_verified_at")
        aCoder.encode(created_at, forKey: "created_at")
        aCoder.encode(updated_at, forKey: "updated_at")
        aCoder.encode(status, forKey: "status")
        aCoder.encode(created_by, forKey: "created_by")
        aCoder.encode(is_manager, forKey: "is_manager")
        aCoder.encode(public_image, forKey: "public_image")
        aCoder.encode(secret_code, forKey: "secret_code")
        
        
    }
    //==========================================
    //MARK: - Mappable Protocol Methods
    //==========================================
    func mapping(map: Map)
    {
        id <- map["id"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        email <- map["email"]
        username <- map["username"]
        address <- map["address"]
        postal_code <- map["postal_code"]
        city <- map["city"]
        role_id <- map["role_id"]
        country <- map["country"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        cqc_number <- map["cqc_number"]
        user_image <- map["user_image"]
        practice <- map["practice"]
        email_verified_at <- map["email_verified_at"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        status <- map["status"]
        created_by <- map["created_by"]
        is_manager <- map["is_manager"]
        public_image <- map["public_image"]
        secret_code <- map["secret_code"]
       
    }
    
    func fullNAme() -> String{
        var name = self.first_name ?? ""
        if let lsname = self.last_name{
            name = name + " " + lsname
        }
        
        return name
    }
    
}


