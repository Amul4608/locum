//
//  JobDetailModel.swift
//  LocumCloud
//
//  Created by Amul Patel on 09/10/21.
//

import Foundation
import ObjectMapper

class JobDetailModel: Mappable {
    var id : Int?
    var headline : String?
    var job_title_id : Int?
    var job_type_id : Int?
    var qualification : String?
    var experience : String?
    var date : String?
    var start_time : String?
    var end_time : String?
    var hourly_rate : String?
    var location : String?
    var user_id : Int?
    var description : String?
    var latitude : Int?
    var longitude : Int?
    var job_rating : String?
    var job_review : String?
    var status : Int?
    var created_at : String?
    var created_by : String?
    var updated_at : String?
    var updated_by : String?
    var deleted_at : String?
    var deleted_by : String?
    var job_title : JobTitleDataModel?
    var job_type : JobTypeDataModel?
    var job_apply : [JobApply_UserData]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        headline <- map["headline"]
        job_title_id <- map["job_title_id"]
        job_type_id <- map["job_type_id"]
        qualification <- map["qualification"]
        experience <- map["experience"]
        date <- map["date"]
        start_time <- map["start_time"]
        end_time <- map["end_time"]
        hourly_rate <- map["hourly_rate"]
        location <- map["location"]
        user_id <- map["user_id"]
        description <- map["description"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        job_rating <- map["job_rating"]
        job_review <- map["job_review"]
        status <- map["status"]
        created_at <- map["created_at"]
        created_by <- map["created_by"]
        updated_at <- map["updated_at"]
        updated_by <- map["updated_by"]
        deleted_at <- map["deleted_at"]
        deleted_by <- map["deleted_by"]
        job_title <- map["job_title"]
        job_type <- map["job_type"]
        job_apply <- map["job_apply"]
    }
}



class JobApply_UserData:Mappable{
    var id : Int?
    var user_id : Int?
    var job_id : Int?
    var status : Int?
    var created_at : String?
    var updated_at : String?
    var user :JobUserModel?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        user_id <- map["user_id"]
        job_id <- map["job_id"]
        status <- map["status"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        user <- map["user"]
    }
}

class JobUserModel: Mappable {
    var id : Int?
    var first_name : String?
    var last_name : String?
    var email : String?
    var role_id : Int?
    var address : String?
    var address2 : String?
    var city : String?
    var postcode : String?
    var country : String?
    var latitude : Double?
    var longitude : Double?
    var cqc_number : String?
    var practice : String?
    var subscription_id : String?
    var location_radious : String?
    var user_image : String?
    var agency_name : String?
    var email_verified_at : String?
    var created_at : String?
    var updated_at : String?
    var deleted_at : String?
    var job_review : String?
    var jobtitle:[JobTitleDataModel]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        email <- map["email"]
        role_id <- map["role_id"]
        address <- map["address"]
        address2 <- map["address2"]
        city <- map["city"]
        postcode <- map["postcode"]
        country <- map["country"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        cqc_number <- map["cqc_number"]
        practice <- map["practice"]
        subscription_id <- map["subscription_id"]
        location_radious <- map["location_radious"]
        user_image <- map["user_image"]
        agency_name <- map["agency_name"]
        email_verified_at <- map["email_verified_at"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        deleted_at <- map["deleted_at"]
        job_review <- map["job_review"]
        jobtitle <- map["jobtitle"]
    }
}
