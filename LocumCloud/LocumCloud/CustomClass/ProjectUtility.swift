//
//  ProjectUtility.swift
//  
//

import Foundation
import CoreData
import AVFoundation
import Photos
import UIKit


//MARK: Add See More.... in String
func addReadMore(str: String, maxLength: Int) -> NSAttributedString {
    var attributedString = NSAttributedString()
    let index: String.Index = str.index(str.startIndex, offsetBy: maxLength)
    let editedText = String(str.prefix(upTo: index)) + "... Read More"
    attributedString = NSAttributedString(string: editedText)
    return attributedString
}

//MARK:- Extension UILabel
//MARK: Get Numbers of Line in Label
extension UILabel {
    var numberOfVisibleLines: Int {
        let textSize = CGSize(width: CGFloat(self.frame.size.width), height: CGFloat(MAXFLOAT))
        let rHeight: Int = lroundf(Float(self.sizeThatFits(textSize).height))
        let charSize: Int = lroundf(Float(self.font.pointSize))
        return rHeight / charSize
    }
}


//MARK:- Extension String
//MARK: Highlight Words In String
extension String {
    func highlightWordsIn(highlightedWords: String, attributes: [[NSAttributedString.Key: Any]]) -> NSMutableAttributedString {
        let range = (self as NSString).range(of: highlightedWords)
        let result = NSMutableAttributedString(string: self)
        
        for attribute in attributes {
            result.addAttributes(attribute, range: range)
        }
        return result
    }
}

extension CLLocation {
    func getDistance(anotherLocation : CLLocation)  -> Double{
        return self.distance(from:anotherLocation)
    }
}



//MARK:- Convert from NSManagedObject To JSON
func convertToJSONArray(myArray: [NSManagedObject]) -> Any {
    var jsonArray: [[String: Any]] = []
    for item in myArray {
        var dict: [String: Any] = [:]
        for attribute in item.entity.attributesByName {
            
            if let value = item.value(forKey: attribute.key) {
                dict[attribute.key] = value
            }
        }
        jsonArray.append(dict)
    }
    return jsonArray
}

//Remove Duplicate Value in Array
func removeDuplicates(array: [String]) -> [String] {
    var encountered = Set<String>()
    var result: [String] = []
    for value in array {
        if encountered.contains(value) {
            // Do not add a duplicate element.
        }
        else {
            // Add value to the set.
            encountered.insert(value)
            // ... Append the value.
            result.append(value)
        }
    }
    return result
}


