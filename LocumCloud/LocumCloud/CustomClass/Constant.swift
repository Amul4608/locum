//
//  Constant.swift
//


import UIKit

class Constant: NSObject {

}
var currentVc = UIViewController()

var IsIphoneX:Bool = false
var IsIphone5:Bool = false
var AppName:String = "247 Zay"
var currentTab = 0
var isFromSucessForgotPass = false
var SucessMSG = ""
var googleClientId = "539350061377-ulsup62k9m85iud3rga0rdviaqc4pebl.apps.googleusercontent.com"

class StoryBoardName:NSObject
{
    static let MainStoryBord:String = "Main"
    static let PracticeDashboardStoryBord:String = "PracticeDashboard"
    static let ProfetionalDashboardStoryBord:String = "ProfetionalDashboard"
    static let ProfetionalRegisterStoryBord:String = "ProfetionalRegister"
}

class ColorCode{
    
    static let CommanColor:UIColor = UIColor.black
    static let CommanColorGreay:UIColor = UIColor(hexString: "#707070")
    static let CommanMenuColor:UIColor = UIColor(red: 229.0/255.0, green: 74.0/255.0, blue: 58.0/255.0, alpha: 0.07)
}




extension UIColor {
    convenience init(hexStringall: String, alpha: CGFloat = 1.0) {
        var hexInt: UInt32 = 0
        let scanner = Scanner(string: hexStringall)
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        scanner.scanHexInt32(&hexInt)

        let red = CGFloat((hexInt & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexInt & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexInt & 0xff) >> 0) / 255.0
        let alpha = alpha

        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}



extension UIApplication {
    static var release: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String? ?? "x.x"
    }
    static var build: String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String? ?? "x"
    }
    static var version: String {
        return "\(release).\(build)"
    }
}

struct HelpSupport {
    static let callUS = "CALL US"
    static let sms = "SMS US"
    static let viber = "VIBER"
    static let whatsApp = "WhatsApp"
    static let email = "EMAIL US"
}
