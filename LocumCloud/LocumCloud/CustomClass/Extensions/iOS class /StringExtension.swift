//
//  StringExtension.swift
//

import Foundation
import MobileCoreServices


extension String
{
    //==========================================
    //MARK: - To get Length of String
    //==========================================
  

    var length: Int {
        return self.count
    }
    
    func nsstring () -> NSString {
        return (self as NSString)
    }
    
   func validateEmailString(_ emailString:String)->Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: emailString)
    }
    
     func validateSpecialCharacter(_ input:String)->Bool
    {
        let regEx = "[A-Za-z0-9]*"
        let testResult = NSPredicate(format:"SELF MATCHES %@", regEx)
        return testResult.evaluate(with: input)
    }
    func isValidEmailAddress () -> Bool
    {
        
        var returnValue = true
        
        let emailRegEx = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" +
            "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
            "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" +
            "z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" +
            "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
            "9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
        "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"//"[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"*/
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            
            let results = regex.matches(in: self.trimmingCharacters(in: .whitespacesAndNewlines), range: NSRange(location: 0, length: self.count))
            if results.count == 0
            {
                returnValue = false
            }
        }
        catch let error as NSError
        {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
  
    func isPasswordValid() -> Bool{
       // let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
       // return passwordTest.evaluate(with: self)
        let passwordRegex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{8,}$"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
    }
    
    //==========================================
    //MARK: - To get Float value from String
    //==========================================
    
    var floatValue: Float {
        let balance = self.replacingOccurrences(of: ",", with: "")
        return Float(balance)!
    }
    

    
    //==========================================
    //MARK: - To get Double value from String
    //==========================================
    
    func toDouble() -> Double? {
        
        // return NSNumberFormatter().numberFromString(self)?.doubleValue
        let balance = self.replacingOccurrences(of: ",", with: "")
        return Double(balance)
    }
    
    
    //==========================================
    //MARK: - To get Int value from String
    //==========================================
    
    func toInt() -> Int? {
        // return NSNumberFormatter().numberFromString(self)?.doubleValue
        let balance = self.replacingOccurrences(of: ",", with: "")
        return Int(balance)
    }
    
    
    //==========================================
    //MARK: - To get Bool value from String
    //==========================================
    
    var boolValue: Bool {
        return NSString(string: self).boolValue
    }
    
    
    //==========================================
    //MARK: - To get Number value from String
    //==========================================
    
    var numberValue:NSNumber? {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        return formatter.number(from: self)
    }
    
    //==========================================
    //MARK: - To get Localized String from String
    //==========================================
    
    func localizedString()->String{
        
        return  NSLocalizedString(self, tableName: "Messages", bundle: Bundle.main, value: "", comment: "") as String;
    }
    
    
    //==================================================
    //MARK: - To get Replaced String with another String
    //==================================================
    
    func replacementOfString(_ stringToReplace:String,stringByReplace:String) -> String{
        let newString = self.replacingOccurrences(of: stringToReplace, with: stringByReplace, options: NSString.CompareOptions.literal, range: nil)
        return newString
    }
    
    //==========================================
    //MARK: - To get UInt value from String
    //==========================================
    
    func toUInt() -> UInt? {
        if self.contains("-") {
            return nil
        }
        return self.withCString { cptr -> UInt? in
            var endPtr : UnsafeMutablePointer<Int8>? = nil
            errno = 0
            let result = strtoul(cptr, &endPtr, 10)
            if errno != 0 || endPtr?.pointee != 0 {
                return nil
            } else {
                return result
            }
        }
    }
    
    //==========================================
    //MARK: - To Get NSString from String
    //==========================================
  
    var ns: NSString {
        return self as NSString
    }
    
    
    //==========================================
    //MARK: - To Get Path Extension of String
    //==========================================
    
    var pathExtension: String? {
        return ns.pathExtension
    }
    
    //==========================================
    //MARK: - To Get mime type of file or filePath
    //==========================================
    
    var mimeTypeForPath : String? {

        let pathExtension = self.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return nil
    }
    
    //==============================================
    //MARK: - To Get last Path component from String
    //==============================================
    
    var lastPathComponent: String? {
        return ns.lastPathComponent
    }
    
    //==========================================
    //MARK: - To Get trim string
    //==========================================
    
    var trimSpace: String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    
    //==============================================
    //MARK: - To Get last 2 Digit from Number
    //==============================================
    
    func last2Digit()->String{
        let trimmedString: String = (self as NSString).substring(from: max(self.length-2,0))
        return trimmedString
    }
    
    
    //==============================================
    //MARK: - Subscript to Get Character at Index
    //==============================================
    
    subscript(integerIndex: Int) -> Character {
        let index = self.index(startIndex, offsetBy: integerIndex)
       // let index = characters.index(startIndex, offsetBy: integerIndex)
        return self[index]
    }
    
    
    //==========================================================
    //MARK: - Subscript to Get Range of String From given String
    //==========================================================
    
    subscript(integerRange: Range<Int>) -> String {
        let start = self.index(startIndex, offsetBy: integerRange.lowerBound)//characters.index(startIndex, offsetBy: integerRange.lowerBound)
        let end = self.index(startIndex, offsetBy: integerRange.upperBound)//characters.index(startIndex, offsetBy: integerRange.upperBound)
        let range = start..<end
        return String(self[range])
    }

    
    func isStringOnlyAlphabet() -> Bool{
        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z ].*", options: NSRegularExpression.Options())
        if regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(), range:NSMakeRange(0, self.count)) != nil {
            return false
        }
        return true
    }
    
    
    func isStringOnlyAlphabetAndComma() -> Bool{
        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z ,].*", options: NSRegularExpression.Options())
        if regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(), range:NSMakeRange(0, self.count)) != nil {
            return false
        }
        return true
    }
    
    func substring(from: Int?, to: Int?) -> String {
        if let start = from {
            guard start < self.count else {
                return ""
            }
        }
        
        if let end = to {
            guard end > 0 else {
                return ""
            }
        }
        
        if let start = from, let end = to {
            guard end - start > 0 else {
                return ""
            }
        }
        
        let startIndex: String.Index
        if let start = from, start >= 0 {
            startIndex = self.index(self.startIndex, offsetBy: start)
        } else {
            startIndex = self.startIndex
        }
        
        let endIndex: String.Index
        if let end = to, end >= 0, end < self.count {
            endIndex = self.index(self.startIndex, offsetBy: end + 1)
        } else {
            endIndex = self.endIndex
        }
        
        return String(self[startIndex ..< endIndex])
    }
    
    func substring(from: Int) -> String {
        return self.substring(from: from, to: nil)
    }
    
    func substring(to: Int) -> String {
        return self.substring(from: nil, to: to)
    }
    
    func substring(from: Int?, length: Int) -> String {
        guard length > 0 else {
            return ""
        }
        
        let end: Int
        if let start = from, start > 0 {
            end = start + length - 1
        } else {
            end = length
        }
        
        return self.substring(from: from, to: end)
    }
    
    func substring(length: Int, to: Int?) -> String {
        guard let end = to, end > 0, length > 0 else {
            return ""
        }
        
        let start: Int
        if let end = to, end - length > 0 {
            start = end - length + 1
        } else {
            start = 0
        }
        
        return self.substring(from: start, to: to)
    }
    
    ////==========================================
    //MARK: - Encoding and Decoding
    //============================================
    
    func decodeString(str : String) -> String{
        let wI = NSMutableString( string: str )
        CFStringTransform( wI, nil, "Any-Hex/Java" as NSString, true )
        return wI as String
        
    }
    
    func encodeString(str :  String) -> String{
        let eS = NSMutableString( string: str )
        CFStringTransform( eS, nil, "Any-Hex/Java" as NSString, false)
        return eS as String
    }
 
        var htmlAttributedString: NSAttributedString? {
            return try? NSAttributedString(data: Data(utf8), options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }
    
    func HtmlStringLoad() -> NSAttributedString
    {
        let data = self.data(using: String.Encoding.unicode)! // mind "!"
        let attrStr = try? NSAttributedString( // do catch
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil)
        return attrStr!
    }
    func slice(from: String, to: String) -> String? {
        
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                substring(with: substringFrom..<substringTo)
            }
        }
    }
    func HTMLImageCorrector() -> String {
        var HTMLToBeReturned = self
        while HTMLToBeReturned.range(of: "(?<=width=\")[^\" height]+", options: .regularExpression) != nil{
            if let match = HTMLToBeReturned.range(of: "(?<=width=\")[^\" height]+", options: .regularExpression) {
                HTMLToBeReturned.removeSubrange(match)
                if let match2 = HTMLToBeReturned.range(of: "(?<=height=\")[^\"]+", options: .regularExpression) {
                    HTMLToBeReturned.removeSubrange(match2)
                    let string2del = "width=\"\" height=\"\""
                    HTMLToBeReturned = HTMLToBeReturned.replacingOccurrences(of: string2del, with: "style=\"width: 100%\"")
                }
            }
            
        }
        
        return HTMLToBeReturned
    }
    
}



var AttributedFontColor:UIColor = .black
extension NSMutableAttributedString {
    var fontSize:CGFloat { return 14 }
    var fontColor:UIColor { return AttributedFontColor }
    var boldFont:UIFont { return UIFont(name: "AvenirNext-Bold", size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize) }
    var normalFont:UIFont { return UIFont(name: "AvenirNext-Regular", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)}
    var smallFont:UIFont { return UIFont(name: "AvenirNext-Regular", size: 9) ?? UIFont.systemFont(ofSize: 9)}
    
    
    func bold(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : boldFont
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func normal(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : fontColor,
            .backgroundColor : UIColor.clear
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    func Small(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  smallFont,
            .foregroundColor : fontColor,
            .backgroundColor : UIColor.clear
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    /* Other styling methods */
    func orangeHighlight(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.orange
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func blackHighlight(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.black
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func underlined(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .underlineStyle : NSUnderlineStyle.single.rawValue
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
}
extension String {

    public func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect,
                                        options: .usesLineFragmentOrigin,
                                        attributes: [.font: font], context: nil)

        return ceil(boundingBox.width)
    }
}

