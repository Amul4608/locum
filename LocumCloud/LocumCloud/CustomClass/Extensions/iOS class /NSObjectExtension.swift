//
//  NSObjectExtension.swift
//

import Foundation
import UIKit

extension NSObject {
    var className : String{
        return String(describing: type(of:self))
    }
    
    
    public class var className: String {
        return NSStringFromClass(self).components(separatedBy: ".").last ?? ""
    }
}

