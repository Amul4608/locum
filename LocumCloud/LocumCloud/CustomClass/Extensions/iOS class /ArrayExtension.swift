//
//  ArrayExtension.swift
//

import Foundation

extension Array where Element: Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        if let index = indexOf(object){
            remove(at: index)
        }
       
    }
    func contains<U: Equatable>(_ object:U) -> Bool {
        return (self.indexOf(object) != nil);
    }
    
    func indexOf<U: Equatable>(_ object: U) -> Int? {
        for (idx, objectToCompare) in self.enumerated() {
            if let to = objectToCompare as? U {
                if object == to {
                    return idx
                }
            }
        }
        return nil
    }
    mutating func move(_ element: Element, to newIndex: Index) {
        if let oldIndex: Int = self.index(of: element) { self.move(from: oldIndex, to: newIndex) }
    }
    
    mutating func removeObject<U: Equatable>(_ object: U) {
        let index = self.indexOf(object)
        if(index != nil) {
            self.remove(at: index!)
        }
    }
}


extension Array
{
    mutating func move(from oldIndex: Index, to newIndex: Index) {
        // Don't work for free and use swap when indices are next to each other - this
        // won't rebuild array and will be super efficient.
        if oldIndex == newIndex { return }
        if abs(newIndex - oldIndex) == 1 { return self.swapAt(oldIndex, newIndex) }
        self.insert(self.remove(at: oldIndex), at: newIndex)
}
}
