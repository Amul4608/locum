//
//  CommonExtension.swift
//  Taaraka
//

import Foundation
import CommonCrypto
import AVFoundation
import Photos
import UIKit

extension UIViewController {
    
    
    
    
    //MARK:- Add/Remove No Internet Connection
    /*func removeNoInternetConnectionView() {
     let lastController = APP_DELEGATE.window?.rootViewController?.inputViewController()
     
     for viewController in lastController!.children {
     // some process
     if viewController.isKind(of:NoInternetConnectionVC.self)
     {
     UIView.animate(withDuration:1.0, delay: 0.0, options:     UIView.AnimationOptions.transitionFlipFromBottom, animations: {
     
     viewController.view.alpha = 0.0
     (viewController as! NoInternetConnectionVC).viewConnection.transform = CGAffineTransform(translationX: 0.0, y: UIScreen.main.bounds.size.height)
     }, completion: {
     (value: Bool) in
     viewController.willMove(toParent: lastController)
     viewController.view.removeFromSuperview()
     viewController.removeFromParent()
     })
     }
     }
     }*/
    /* func addNoInternetConnectionView() {
     let lastController = APP_DELEGATE.window?.rootViewController?.inputViewController()
     let noConnectionVC = NoInternetConnectionVC()
     
     if lastController!.isKind(of:  UIAlertController.self){
     return
     }
     
     lastController!.addChild(noConnectionVC)
     noConnectionVC.didMove(toParent: lastController)
     
     noConnectionVC.view.frame = CGRect(x: 0, y: 0, width: lastController!.view.frame.width, height: lastController!.view.frame.height)
     noConnectionVC.inputView?.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
     lastController!.view.addSubview((noConnectionVC.view)!)
     
     UIView .animate(withDuration:const_Duration_Reachability, delay: 0.0, options: UIView.AnimationOptions.transitionFlipFromBottom, animations: {
     noConnectionVC.view.alpha = 1.0
     noConnectionVC.viewConnection.transform = .identity
     }, completion: {
     (value: Bool) in
     })
     }*/
    
    //MARK:- Get User's All Detail -- All Cards, Fav Cards, Interests
    /* func getUserAllDetail(shouldShowAlert:Bool,completion: ((_ result: JSON?, _ msg:String,_ isSuccess:Bool) -> Void)?) {
     APIManager.callAPIRequest(Method: .get, url: api_getAllDetail, parameters: nil, headers: const_dictHeaderWithToken, showAlert: shouldShowAlert, completion: { (jsonResult, msg) in
     if completion != nil {
     completion!(jsonResult,msg,true)
     }
     }) { (response, errorMsg) in
     if completion != nil {
     completion!(nil,errorMsg,false)
     }
     }
     }*/
    
    //MARK:- Get star point
    /* func callAPItoGetStarPoints(completion:  @escaping (_ points: Int) -> Void) {
     APIManager.callAPIRequest(Method: .get, url: api_getStarPoints, parameters: nil, headers: const_dictHeaderWithToken, showAlert: true, completion: { (jsonResult, msg) in
     
     completion(jsonResult["star_points"].intValue)
     
     }) { (httpResponse, errorMsg) in
     
     completion(0)
     }
     }*/
    
    //MARK:- Get Top Most ViewController
    /* func topMostVC() -> UIViewController {
     if let presented = self.presentedViewController {
     return presented.topMostViewController()
     }
     
     if let navigation = self as? UINavigationController {
     return navigation.visibleViewController?.topMostViewController() ?? navigation
     }
     
     if let tab = self as? UITabBarController {
     return tab.selectedViewController?.topMostViewController() ?? tab
     }
     
     return self
     }*/
    
    
    
    
}


//MARK:- Other Extensions
extension UIView {
    func changeColor(color: UIColor) {
        for vw in self.subviews {
            if vw.isKind(of: UITextField.self) || vw.isKind(of: UIButton.self) {
                vw.backgroundColor = UIColor.white
            }else if vw.isKind(of: UIView.self) {
                vw.backgroundColor = color
            }
        }
    }
    
    /* func removeLootieView() {
     for vw in self.subviews {
     if vw.isKind(of: LOTAnimationView.self) {
     vw.removeFromSuperview()
     }
     }
     }*/
    
    func takeScreenshot() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return  image
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

extension Collection where Index == Int {
    func pickRandomElement() -> Iterator.Element? {
        return isEmpty ? nil : self[Int(arc4random_uniform(UInt32(endIndex)))]
    }
}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}

extension UILabel {
    
    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0, font: UIFont = UIFont(name: "Helvetica Neue", size: 15)!) {
        
        guard let labelText = self.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        paragraphStyle.alignment = .center
        
        let attributedString:NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }
        
        // (Swift 4.2 and above) Line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        attributedString.addAttribute(NSAttributedString.Key.font, value:font, range:NSMakeRange(0, attributedString.length))
        
        // (Swift 4.1 and 4.0) Line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        
        self.attributedText = attributedString
    }
}

extension Dictionary {
    
    func JSONDescription() -> String {
        
        let data = self as AnyObject
        var jsonString = "Error parsing dictionary"
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            
        } catch let error as NSError {
            print(error.description)
        }
        return jsonString
    }
    
}

//MARK:- Extension for get URL From PHAsset
extension PHAsset {
    
    func getURL(completionHandler : @escaping ((_ responseURL : URL?) -> Void)){
        if self.mediaType == .image {
            let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
            options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
                return true
            }
            self.requestContentEditingInput(with: options, completionHandler: {(contentEditingInput: PHContentEditingInput?, info: [AnyHashable : Any]) -> Void in
                completionHandler(contentEditingInput!.fullSizeImageURL as URL?)
            })
        } else if self.mediaType == .video {
            let options: PHVideoRequestOptions = PHVideoRequestOptions()
            options.version = .original
            PHImageManager.default().requestAVAsset(forVideo: self, options: options, resultHandler: {(asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) -> Void in
                if let urlAsset = asset as? AVURLAsset {
                    let localVideoUrl: URL = urlAsset.url as URL
                    completionHandler(localVideoUrl)
                } else {
                    completionHandler(nil)
                }
            })
        }
    }
}

class TextField: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
//Date Compare Extension
extension NSDate {
    
    func isGreaterThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    func addDays(daysToAdd: Int) -> NSDate {
        let secondsInDays: TimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: NSDate = self.addingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    func addHours(hoursToAdd: Int) -> NSDate {
        let secondsInHours: TimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: NSDate = self.addingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

extension UIImage {
    func fixOrientation() -> UIImage
    {
        
        if self.imageOrientation == UIImage.Orientation.up {
            return self
        }
        
        var transform = CGAffineTransform.identity
        
        switch self.imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: self.size.width, y: self.size.height)
            transform = transform.rotated(by: CGFloat(M_PI));
            
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: self.size.width, y: 0);
            transform = transform.rotated(by: CGFloat(M_PI_2));
            
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: self.size.height);
            transform = transform.rotated(by: CGFloat(-M_PI_2));
            
        case .up, .upMirrored:
            break
        }
        
        
        switch self.imageOrientation {
            
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
            
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: self.size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1);
            
        default:
            break;
        }
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx = CGContext(
            data: nil,
            width: Int(self.size.width),
            height: Int(self.size.height),
            bitsPerComponent: self.cgImage!.bitsPerComponent,
            bytesPerRow: 0,
            space: self.cgImage!.colorSpace!,
            bitmapInfo: UInt32(self.cgImage!.bitmapInfo.rawValue)
        )
        
        
        
        ctx!.concatenate(transform);
        
        switch self.imageOrientation {
            
        case .left, .leftMirrored, .right, .rightMirrored:
            // Grr...
            ctx?.draw(self.cgImage!, in: CGRect(x:0 ,y: 0 ,width: self.size.height ,height:self.size.width))
            
        default:
            ctx?.draw(self.cgImage!, in: CGRect(x:0 ,y: 0 ,width: self.size.width ,height:self.size.height))
            break;
        }
        
        // And now we just create a new UIImage from the drawing context
        let cgimg = ctx!.makeImage()
        
        let img = UIImage(cgImage: cgimg!)
        
        return img;
        
    }
}
