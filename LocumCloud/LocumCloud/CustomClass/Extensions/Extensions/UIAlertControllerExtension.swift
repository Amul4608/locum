//
//  UIAlertControllerExtension.swift
//  Taaraka
//

import Foundation
import UIKit

extension UIAlertController {
    /*------------- 9SPL Updates Start---------------*/
    public convenience init(title1: String? = "Alert", message1: String) {
        self.init(title: title1, message: message1, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        addAction(defaultAction)
        //self.show(animated: true, vibrate: false, completion: nil)
        UIAlertController.topViewController( (AppDelegate.sharedAppDelegate.window?.rootViewController)!).present(self, animated: true, completion: nil)
    }
    
    public convenience init(title: String?, message:String, okTitle:String?, cancelTitle:String?, okCompletion: ((UIAlertAction) -> Void)?,cancelCompletion:((UIAlertAction) -> Void)?) {
        self.init(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: okTitle != nil ? okTitle: "Ok", style: UIAlertAction.Style.default, handler: okCompletion)
        let cancelAction = UIAlertAction(title: cancelTitle != nil ? cancelTitle: "Cancel", style: UIAlertAction.Style.default, handler: cancelCompletion)
        
        if (cancelCompletion != nil) {
            self.addAction(cancelAction)
        }
        
        if (okCompletion != nil) {
            self.addAction(OKAction)
        }
        UIAlertController.topViewController( (AppDelegate.sharedAppDelegate.window?.rootViewController)!).present(self, animated: true, completion: nil)
       // self.show(animated: true, vibrate: false, completion: nil)
    }
    
    //MARK: - Present View Controller (Currently At Top)
    class func topViewController(_ rootViewController: UIViewController) -> UIViewController {
        if rootViewController.presentedViewController == nil {
            return rootViewController
        }
        if (rootViewController.presentedViewController is UINavigationController) {
            let navigationController = (rootViewController.presentedViewController as! UINavigationController)
            let lastViewController = navigationController.viewControllers.last!
            return self.topViewController(lastViewController)
        }
        let presentedViewController = (rootViewController.presentedViewController)
        return self.topViewController(presentedViewController!)
    }
    /*------------- 9SPL Updates End---------------*/
}
