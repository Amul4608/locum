//
//  UISlider.swift
//

import Foundation
class AMSlider: UISlider {

    @IBInspectable var thumbImage: UIImage?

    // MARK: Lifecycle
    
    @IBInspectable open var trackWidth:CGFloat = 1 {
           didSet {setNeedsDisplay()}
       }

       override open func trackRect(forBounds bounds: CGRect) -> CGRect {
           let defaultBounds = super.trackRect(forBounds: bounds)
           return CGRect(
               x: defaultBounds.origin.x,
               y: defaultBounds.origin.y + defaultBounds.size.height/2 - trackWidth/2,
               width: defaultBounds.size.width,
               height: trackWidth
           )
       }

    override func awakeFromNib() {
        super.awakeFromNib()

        if let thumbImage = thumbImage {
            self.setThumbImage(thumbImage, for: .normal)
        }
    }
}
