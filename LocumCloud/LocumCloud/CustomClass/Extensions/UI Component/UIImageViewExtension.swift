//
//  UIImageViewExtension.swift
//

import Foundation
import SDWebImage
import UIKit

extension UIImageView {
    
    
    //==========================================
    //MARK: - Apply Corner Radius
    //==========================================
    
    func applyCornerRadius () {
        
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.frame.size.width / 2.0
        self.layer.masksToBounds = true
        
    }
    
    open override func layoutSubviews() {
        
        let activityView : UIActivityIndicatorView? = self.viewWithTag(10) as? UIActivityIndicatorView
        if activityView != nil {
            activityView?.center = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        }
        
    }
    func setImageWithActivityWithplaceholder(_ url :String? , _ style : UIActivityIndicatorView.Style , placeHolderIamge:UIImage){
        if url != nil {
            
            var activityView : UIActivityIndicatorView? = self.viewWithTag(10) as? UIActivityIndicatorView
            if activityView == nil {
                
                activityView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                activityView?.tag = 10
            }
            activityView?.style = style
            activityView?.center = self.center//CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
           // self.addSubview(activityView!)
            activityView?.startAnimating()
            
            self.sd_setImage(with: URL(string:url!), placeholderImage: placeHolderIamge, options: .refreshCached, completed: { (image, erroe, catchType, url) in
                if image != nil
                {
                    
                    self.image = image//?.resize(CGSize(width:UIScreen.main.bounds.width , height: UIScreen.main.bounds.height / 2 + 50 ))
                    
                   // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ImageLoaded"), object: nil, userInfo: ["Isavail":true] )
                }
                activityView?.removeFromSuperview()
            })
            
            
        }
    }
    
    func setImageWithActivityWithplaceholderDetail(_ url :String? , _ style : UIActivityIndicatorView.Style , placeHolderIamge:UIImage){
        if url != nil {
            
            var activityView : UIActivityIndicatorView? = self.viewWithTag(10) as? UIActivityIndicatorView
            if activityView == nil {
                
                activityView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                activityView?.tag = 10
            }
            activityView?.style = style
            activityView?.center = self.center//CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
           // self.addSubview(activityView!)
            activityView?.startAnimating()
            
            self.sd_setImage(with: URL(string:url!), placeholderImage: placeHolderIamge, options: .refreshCached, completed: { (image, erroe, catchType, url) in
                if image != nil
                {
                    
                    self.image = image?.resize(CGSize(width:UIScreen.main.bounds.width , height: UIScreen.main.bounds.height / 2))
                    
                    // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ImageLoaded"), object: nil, userInfo: ["Isavail":true] )
                }
                activityView?.removeFromSuperview()
            })
            
            
        }
    }
    func setImageWithActivity(_ url :String? , _ style : UIActivityIndicatorView.Style){
        if url != nil {
            
            var activityView : UIActivityIndicatorView? = self.viewWithTag(10) as? UIActivityIndicatorView
            if activityView == nil {
                
                activityView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                activityView?.tag = 10
            }
            activityView?.style = style
            activityView?.center = self.center//CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
            self.addSubview(activityView!)
            activityView?.startAnimating()
            
            self.sd_setImage(with: URL(string:url!), placeholderImage: nil, options: .refreshCached, completed: { (image, erroe, catchType, url) in
                self.image = image
                activityView?.removeFromSuperview()
            })
            
        }
    }
    func imageSnap(text: String?,
                   color: UIColor,
                   circular: Bool,
                   textAttributes: [NSAttributedString.Key: Any]?) -> UIImage? {
        
        let scale = Float(UIScreen.main.scale)
        var size = bounds.size
        if contentMode == .scaleToFill || contentMode == .scaleAspectFill || contentMode == .scaleAspectFit || contentMode == .redraw {
            size.width = CGFloat(floorf((Float(size.width) * scale) / scale))
            size.height = CGFloat(floorf((Float(size.height) * scale) / scale))
        }
        
        UIGraphicsBeginImageContextWithOptions(size, false, CGFloat(scale))
        let context = UIGraphicsGetCurrentContext()
        if circular {
            let path = CGPath(ellipseIn: bounds, transform: nil)
            context?.addPath(path)
            context?.clip()
        }
        
        // Fill
        context?.setFillColor(color.cgColor)
        context?.fill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        
        // Text
        if let text = text {
            let attributes = textAttributes ?? [NSAttributedString.Key.foregroundColor: UIColor.white,
                                                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15.0)]
            
            let textSize = text.size(withAttributes: attributes)
            let bounds = self.bounds
            let rect = CGRect(x: bounds.size.width/2 - textSize.width/2, y: bounds.size.height/2 - textSize.height/2, width: textSize.width, height: textSize.height)
            
            text.draw(in: rect, withAttributes: attributes)
        }
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
}

extension UIImageView {
    func loadImageFromURLWithName(_ url :String?, fname: String , lname: String, fontSize: CGFloat = 10.0, withRandomColor: Bool = false) {
        if url != nil && url != "" {
            let image = #imageLiteral(resourceName: "profileThumb")
            self.sd_setImage(with: URL(string:url!), placeholderImage: image, options: .refreshCached, completed: { (image, error, catchType, url) in
                
                if ((error) != nil) {
                    self.image = image
                }
                self.borderWidth = 1
                self.cornerRadius = self.frame.height / 2
                self.borderColor = UIColor.white
                
                //self.layer.borderWidth = 2.0
                //self.layer.borderColor = UIColor.white.cgColor
            })
        }
        else {
            //self.image = UIImage.init(named: "ico_profile_default.png")
            
            let lblNameInitialize = UILabel()
            lblNameInitialize.frame.size = CGSize(width: self.frame.width, height: self.frame.height)  //100.0 //100.0
            //lblNameInitialize.text = String(IBtxtFieldName.text!.characters.first!) + String(IBtxtFieldSurname.text!.characters.first!)
            
            if fname.count > 0 && lname.count > 0 {
                
                lblNameInitialize.text = (fname.first?.uppercased())! +  (lname.first?.uppercased())!
            }else if fname.count > 0{
                 lblNameInitialize.text = fname.first?.uppercased()
            }else if lname.count > 0{
                 lblNameInitialize.text = lname.first?.uppercased()
            }
            else {
                lblNameInitialize.text = "TN"
            }
            
            lblNameInitialize.font = UIFont(name: "Roboto-Medium", size: fontSize)!
            lblNameInitialize.textAlignment = NSTextAlignment.center
            lblNameInitialize.textColor = UIColor.white
            if withRandomColor {
                //lblNameInitialize.backgroundColor = UIColor.lightGray   //UIColor.black
                let color:[UIColor] = [ .gray , .red , .purple , .orange,.brown,.magenta]
                lblNameInitialize.backgroundColor = color.randomElement()
            }
            else {
                lblNameInitialize.backgroundColor = UIColor.init(hexString: "0467fe")
            }
            //lblNameInitialize.layer.cornerRadius = self.frame.width/2   //50.0
            lblNameInitialize.layer.cornerRadius = 0.0
            
            self.borderWidth = 0.0
            
            //UIGraphicsBeginImageContext(lblNameInitialize.frame.size)
            UIGraphicsBeginImageContextWithOptions(lblNameInitialize.frame.size, false, 0)
            lblNameInitialize.layer.render(in: UIGraphicsGetCurrentContext()!)
            self.image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
    }
}



let imageCache = NSCache<NSString, AnyObject>()

extension UIImageView {
    func loadImageUsingCache(withUrl urlString : String) {
        let url = URL(string: urlString)
        self.image = nil

        // check cached image
        if let cachedImage = imageCache.object(forKey: urlString as NSString) as? UIImage {
            self.image = cachedImage
            return
        }

        // if not, download image from url
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }

            DispatchQueue.main.async {
                if let image = UIImage(data: data!) {
                    imageCache.setObject(image, forKey: urlString as NSString)
                    self.image = image
                }
            }

        }).resume()
    }
}
