//
//  Constants.swift
//


import Foundation
import UIKit



struct Constants {
    
    
        
        static let kAppDelegate = UIApplication.shared.delegate as! AppDelegate
        static let kMainStoryBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
    
    static func alertShowDialog(message : String, inViewController vc:UIViewController) {
        let appName = AppName
        let alert = UIAlertController(title: appName, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        
        vc.present(alert, animated: true)
    }
    
    
    
    
}
//MARK:- document Selection Type
enum documentSelectionType{
    case CV
    case GDC_Registration
    case Indemnity_Cert
    case Blood_Report
    case Add_References
    case Current_DBS
    
    func getStatus() -> String
    {
        switch self {
        case .CV:
            return "CV"
        case .GDC_Registration:
            return "GDC Registyration"
        case .Indemnity_Cert:
            return "Indemnity Cert"
        case .Blood_Report:
            return "Blood Report"
        case .Add_References:
            return "Add References"
        case .Current_DBS:
            return "Current DBS"
        }
    }
}


struct ConstColor {
    static let appLiteSky:UIColor = UIColor(hexString: "#58C8DD")
    static let appLiteBlue:UIColor = UIColor(hexString: "#665EFF")
    static let appFontRegular:UIColor = UIColor(hexString: "#454F63")
    static let AppGreen:UIColor = UIColor(hexString: "#9AD34D")
    static let AppGray:UIColor = UIColor(hexString: "#524F4F")
}
