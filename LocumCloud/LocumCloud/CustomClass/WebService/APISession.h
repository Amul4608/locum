//
//  APISession.h
//
//
//  Created by i-Phone4 on 07/12/15.
//  Copyright © 2015 Mubin Mall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface APISession : NSURLSession <NSURLSessionDownloadDelegate,NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate>
{
}

@property (nonatomic, copy) void (^onCompletion)(BOOL isDownloaded,UIImage *imgDownloaded);
@property (nonatomic, copy) void (^getProgress)(float progress);


//+(void)apiCallSharedSessionDataPOST:(NSString *)strURL withReqParam:(NSString *)strPostparams withCompletionHandlar:(void (^) (NSDictionary *dicResult,
//                                                                                                                           NSError *error, int status))completionBlock;

/*
 * HTTP METHOD : GET
 */
+(void)apiCallSharedSessionGet:(NSString *)strURL
         withCompletionHandlar:(void (^) (NSDictionary *dicResult, NSError *error, int status)) completionBlock;

/*
 * HTTP METHOD : POST
 */
+(void)apiCallSharedSessionPOST:(NSString *)strURL withReqParam:(NSDictionary *)postVars withCompletionHandlar:(void (^) (NSDictionary *dicResult,
                                                                                                                          NSError *error, int status))completionBlock;

+(void)apiCallSharedSessionDataPOST:(NSString *)strURL withReqParam:(NSString *)strPostparams withCompletionHandlar:(void (^) (NSDictionary *dicResult,
                                                                                                                               NSError *error, int status))completionBlock;
-(id)initWithDownloadingFile:(NSString *)URL
               withImageView:(UIImageView *)imgViewOriginal
               withIndicator:(UIActivityIndicatorView *)indicator;


+(void)cancelRequest;

@property (nonatomic,strong) NSMutableData *responseData;

@end
