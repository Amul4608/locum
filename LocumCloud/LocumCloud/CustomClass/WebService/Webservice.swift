//
//  Webservice.swift
//

import Foundation


internal struct WebService {
    
    //MARK: - BaseURL
    static let baseURL = "https://locumcloudapi.mgcdevelopment.co.uk/public/api/"
    
    static let login:String     = "login"
    static let forgotpassword:String = "forgotpassword"
    static let practice_my_job:String = "practice/my_job"
    static let JobTitles:String = "jobtitles"
    static let JobType:String = "jobtypes"
    static let Register:String = "customer/registerorlogin"
    static let JobCreate:String = "practice/postjob"
    static let job_details:String = "practice/job_details"
    static let job_Duplicate:String = "practice/job/duplicate/"
    static let jobs_delete:String = "practice/job/delete/"
    static let jobStatus:String  = "practice/job/status"
    static let jobEdit:String = "practice/job/edit/"
    static let practice_register:String = "register"
    static let user_secret_code:String = "user/secret-code/update"
    
    static let job_reject:String = "practice/job/reject"
    static let job_accept:String = "practice/job/accept"
    
    static let professional_job_apply:String = "professional/job_apply"
    static let professional_job_list:String = "professional/job/list"
    static let professional_job_complete:String = "professional/job/complete"
   
    
    //To Create URL for Web Service
    internal static func createURLForWebService(_ webServiceName : String) -> String {
        let URL:String = String(format: "\(WebService.baseURL)\(webServiceName)");
        return URL;
    }
}

internal struct Request {
    
    static let email : String                       = "email"
    static let password : String                    = "password"
    static let confirm_password : String            = "c_password"
    static let name : String                        = "name"
    static let first_name : String                  = "first_name"
    static let last_name : String                   = "last_name"
    static let facebook_id : String                 = "facebook_id"
    static let mobile_no : String                   = "mobile_no"
    static let query : String                       = "query"
    static let newPassword:String                   = "new_password"
    static let fcmId:String                         = "fcm_id"
    static let deviceToken : String                 = "device_token"
    static let social_type : String                 = "social_type"
    static let google_id : String                   = "google_id"
    static let user_id: String                      = "user_id"
    static let profile_url : String                 = "profile_url"
    static let text : String                        = "text"
    static let video : String                       = "video"
    static let videos : String                      = "video[]"
    static let video_thumbs : String                = "video_thumb[]"
    static let file : String                        = "file"
    static let comment : String                     = "comment"
    static let description : String                 = "description"
    static let GCM_KEY: String                      = "GCM_KEY"
    static let DEVICE_UDID: String                  = "DEVICE_UDID"
}

internal struct Response {
    
    static let status_code : String         = "StatusCode"
    static let message : String             = "message"
    static let data : String                = "data"
    static let result : String              = "results"
}

internal struct Header {
    
    struct Key {
        static let kClientKey       = "clientkey"
        static let kClientSecret    = "clientsecret"
        static let kToken           = "token"
        static let kAccept           = "Accept"
        static let kAuthorization    = "Authorization"
    }
    
    struct Value {
        
    }
    
    //To Set Header Fields
    static func createHeader(_ token:Bool) -> [String:String] {
        
        var headers : [String:String]? = [:]
        
        headers![Key.kAccept] = "application/json"
        
        if appUser != nil{
            if token == true{
            if let tok = appUser?.token , tok != ""{
                headers!["Authorization"] = "Bearer " + tok
            }
            }
            
        }
        print(headers!)
        return headers!
    }
}

struct WebServiceCallErrorMessage {
    
    static let ErrorInvalidDataFormatMessage = "Please try again , server not reachable";
    static let ErrorServerNotReachableMessage = "Server Not Reachable";
    static let ErrorInternetConnectionNotAvailableMessage = "Opss, looks like you don't have internet access at the moment...";
    static let ErrorTitleInternetConnectionNotAvailableMessage = "Network Error";
    static let ErrorNoDataFoundMessage = "No Data Available";
}

