//
//  NetworkManager.swift
//

import UIKit
import Alamofire
import Photos
//import SVProgressHUD

final class NetworkManager: NSObject {
    
    static var shared : NetworkManager = sharedManager()
    private static var showLog = false
    
    static func sharedManager() -> NetworkManager {
        
        let sharedInstance = NetworkManager()
        return sharedInstance
    }
    
    // MARK:- Request WithMethodType
    func requestWithMethodType(withObject method: HTTPMethod, url : String, parameters: Parameters?, responseKey : String = Response.data , isToken : Bool, successHandler: @escaping (_ response:Any)->(), failureHandler: @escaping (_ errorMessage : String)->()) -> DataRequest? {
        
        //If Internet is not Reachable
        if(MTReachabilityManager.isReachable() == false) {
            
            failureHandler(WebServiceCallErrorMessage.ErrorInternetConnectionNotAvailableMessage)
            return nil
        }
        if (NetworkManager.showLog) {
            print("url \(url)")
            print("parameters\n \(self.paramterString(parameters))")
            print("Header\n \(self.paramterString(Header.createHeader(isToken)))")
        }
        
        return Alamofire.request(url, method: method, parameters: parameters,
                                 encoding: JSONEncoding.default,
                                 headers: Header.createHeader(isToken)).responseJSON(completionHandler: { (response) in
                                    
                                    
                                    if (NetworkManager.showLog) {
                                        print("respnse \(response.data?.toString ?? "")")
                                    }
                                    switch response.result {
                                        
                                    case .success(let result):
                                        if let resultDict = result as? [String:Any]{
                                            successHandler(resultDict)
                                        }
                                        else
                                        {
                                            failureHandler("Server Error")
                                        }
                                        break
                                        
                                    case .failure(let error):
                                        failureHandler(error.localizedDescription)
                                        break
                                    }
                                 })
    }
    
    // MARK:- Request WithMethodType
    func requestWithMethodTypeWithArrayParam(withObject method: HTTPMethod, url : String, parameters: [Parameters], responseKey : String = Response.data , isToken : Bool, successHandler: @escaping (_ response:Any)->(), failureHandler: @escaping (_ errorMessage : String)->()) -> DataRequest? {
        
        //If Internet is not Reachable
        if(MTReachabilityManager.isReachable() == false) {
            
            failureHandler(WebServiceCallErrorMessage.ErrorInternetConnectionNotAvailableMessage)
            return nil
        }
        let parameters : [Parameters] = parameters.map( { $0 })
        
        return Alamofire.request(url, method: .post, encoding: JSONArrayEncoding(array: parameters), headers: Header.createHeader(isToken)).responseJSON { (response) in
            
            if (NetworkManager.showLog) {
                print("respnse \(response.data?.toString ?? "")")
            }
            switch response.result {
                
            case .success(let result):
                if let resultDict = result as? [String:Any]{
                    successHandler(resultDict)
                }
                else
                {
                    failureHandler("Server Error")
                }
                break
                
            case .failure(let error):
                failureHandler(error.localizedDescription)
                break
            }
        }
        
       
    }
    
    
    
    // MARK:- Request WithMethodType
    func requestWithMethodTypeRegister(withObject method: HTTPMethod, url : String, parameters: Parameters?, responseKey : String = Response.data , isToken : Bool, successHandler: @escaping (_ response:Any)->(), failureHandler: @escaping (_ errorMessage : String)->()) -> DataRequest? {
        
        //If Internet is not Reachable
        if(MTReachabilityManager.isReachable() == false) {
            
            failureHandler(WebServiceCallErrorMessage.ErrorInternetConnectionNotAvailableMessage)
            return nil
        }
        if (NetworkManager.showLog) {
            print("url \(url)")
            print("parameters\n \(self.paramterString(parameters))")
            print("Header\n \(self.paramterString(Header.createHeader(isToken)))")
        }
        
        return Alamofire.request(url, method: method, parameters: parameters,
                                 encoding: URLEncoding.httpBody,
                                 headers: Header.createHeader(isToken)).responseJSON(completionHandler: { (response) in
                                    
                                    
                                    if (NetworkManager.showLog) {
                                        print("respnse \(response.data?.toString)")
                                    }
                                    switch response.result {
                                        
                                    case .success(let result):
                                        
                                        if let resultDict = result as? [String:Any]{
                                            successHandler(resultDict)
                                        }
                                        else
                                        {
                                            failureHandler("Server Error")
                                        }
                                        break
                                        
                                    case .failure(let error):
                                        
                                        //     print("response \(response.data?.toString)")
                                        failureHandler(error.localizedDescription)
                                        break
                                    }
                                 })
    }
    
    // MARK:- Request WithMethodType
    func requestWithMethodTypeRegisterEncoding(withObject method: HTTPMethod, url : String, parameters: Parameters?, responseKey : String = Response.data , isToken : Bool, successHandler: @escaping (_ response:Any)->(), failureHandler: @escaping (_ errorMessage : String)->()) -> DataRequest? {
        
        //If Internet is not Reachable
        if(MTReachabilityManager.isReachable() == false) {
            
            failureHandler(WebServiceCallErrorMessage.ErrorInternetConnectionNotAvailableMessage)
            return nil
        }
        if (NetworkManager.showLog) {
            print("url \(url)")
            print("parameters\n \(self.paramterString(parameters))")
            print("Header\n \(self.paramterString(Header.createHeader(isToken)))")
        }
        
        return Alamofire.request(url, method: method, parameters: parameters,
                                 encoding: JSONEncoding.default,
                                 headers: Header.createHeader(isToken)).responseJSON(completionHandler: { (response) in
                                    
                                    
                                    if (NetworkManager.showLog) {
                                        print("respnse \(response.data?.toString)")
                                    }
                                    switch response.result {
                                        
                                    case .success(let result):
                                        
                                        if let resultDict = result as? [String:Any]{
                                            successHandler(resultDict)
                                        }
                                        else
                                        {
                                            failureHandler("Server Error")
                                        }
                                        break
                                        
                                    case .failure(let error):
                                        
                                        //     print("response \(response.data?.toString)")
                                        failureHandler(error.localizedDescription)
                                        break
                                    }
                                 })
    }
    
   
    
    func multipartRequestWithMethodType(_ method: HTTPMethod, url : String, parameters: Parameters?, responseKey : String = Response.data , isToken:Bool, successHandler: @escaping (_ response:Any)->(), progressHandler: @escaping (_ progress : Float)->() ,failureHandler: @escaping (_ errorMessage : String)->()) -> Request?{
        
        //If Internet is not Reachable
        if(MTReachabilityManager.isReachable() == false) {
            
            failureHandler(WebServiceCallErrorMessage.ErrorInternetConnectionNotAvailableMessage)
            return nil
        }
        
        if (NetworkManager.showLog) {
            print("url \(url)")
            print("\nparameters\n \(self.paramterString(parameters))")
            print("\nHeader\n \(self.paramterString(Header.createHeader(isToken)))")
        }
        
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            
            for (key, value) in parameters! {
                
                if value is String {
                    multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
                }
                
                if value is UIImage {
                    
                    let imgData = (value as! UIImage).jpegData(compressionQuality: 1)//UIImageJPEGRepresentation(value as! UIImage, 1)!
                    multipartFormData.append(imgData!, withName: key ,fileName: "file.jpg", mimeType: "image/jpg")
                }
                
                
                if value is Array<UIImage> {
                    let arrayValue = value as! [UIImage]
                    
                    for image in arrayValue {
                        
                              print("image upload for key  \(key)")
                        
                        let fileName = "file\(arc4random()%1000).jpg"
                        let mimeType = fileName.mimeTypeForPath ?? "image/jpg"
                        let data = image.jpegData(compressionQuality: 1)//UIImageJPEGRepresentation(image, 1)
                        
                        multipartFormData.append(data!, withName: "\(key)", fileName:fileName, mimeType: mimeType)
                    }
                    
                }
                
            }
        },to:url,method: .post,  headers: Header.createHeader(isToken))
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    progressHandler(Float(progress.fractionCompleted))
                })
                
                upload.responseJSON { response in
                    if (NetworkManager.showLog) {
                        print("respnse \(response.data?.toString)")
                    }
                    
                    switch response.result {
                        
                    case .success(let result):
                        
                        if let resultDict = result as? [String:Any]{
                            successHandler(resultDict)
                        }
                        else
                        {
                            failureHandler("Server Error")
                        }
                        break
                        
                    case .failure(let error):
                        failureHandler(error.localizedDescription)
                        break
                    }
                    
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
        return nil;
    }
    
    func multipartRequestWithAttachmentMethodType(_ method: HTTPMethod,attechment:[attachmentModel] , url : String, parameters: Parameters?, responseKey : String = Response.data , isToken:Bool, successHandler: @escaping (_ response:Any)->(), progressHandler: @escaping (_ progress : Float)->() ,failureHandler: @escaping (_ errorMessage : String)->()) -> Request?{
        
        //If Internet is not Reachable
        if(MTReachabilityManager.isReachable() == false) {
            
            failureHandler(WebServiceCallErrorMessage.ErrorInternetConnectionNotAvailableMessage)
            return nil
        }
        
        if (NetworkManager.showLog) {
            print("url \(url)")
            print("\nparameters\n \(self.paramterString(parameters))")
            print("\nHeader\n \(self.paramterString(Header.createHeader(isToken)))")
        }
        
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            if parameters != nil {
            for (key, value) in parameters! {
                
                if value is String {
                    multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
                }
            }
            }
            if attechment.count > 0{
                for val in attechment{
                    let name_randome = (arc4random()%1000)
                   
                    if val.type?.lowercased() == "image"{
                        let fileName = "\(val.name ?? "\(name_randome)").jpg"
                        let mimeType = fileName.mimeTypeForPath ?? "image/jpg"
                        let data = val.val
                        multipartFormData.append(data!, withName: "\(val.key ?? "jpg")", fileName:fileName, mimeType: mimeType)
                    }else if val.type?.lowercased() == "doc"{
                        let fileName = "\(val.name ?? "\(name_randome)").doc"
                        let mimeType = fileName.mimeTypeForPath ?? "application/msword"
                        let data = val.val
                        multipartFormData.append(data!, withName: "\(val.key ?? "doc")", fileName:fileName, mimeType: mimeType)
                    }else if val.type?.lowercased() == "pdf"{
                        let fileName = "\(val.name ?? "\(name_randome)").pdf"
                        let mimeType = fileName.mimeTypeForPath ?? "application/pdf"
                        let data = val.val
                        multipartFormData.append(data!, withName: "\(val.key ?? "pdf")", fileName:fileName, mimeType: mimeType)
                    }else if val.type?.lowercased() == "ppt"{
                        let fileName = "\(val.name ?? "\(name_randome)").ppt"
                        let mimeType = fileName.mimeTypeForPath ?? "application/vnd.ms-powerpoint"
                        let data = val.val
                        multipartFormData.append(data!, withName: "\(val.key ?? "ppt")", fileName:fileName, mimeType: mimeType)
                    }
                }
            }
        },to:url,method: .post,  headers: Header.createHeader(isToken))
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    progressHandler(Float(progress.fractionCompleted))
                })
                
                upload.responseJSON { response in
                    if (NetworkManager.showLog) {
                        print("respnse \(response.data?.toString)")
                    }
                    
                    switch response.result {
                        
                    case .success(let result):
                        
                        if let resultDict = result as? [String:Any]{
                            successHandler(resultDict)
                        }
                        else
                        {
                            failureHandler("Server Error\(result)")
                        }
                        break
                        
                    case .failure(let error):
                        failureHandler(error.localizedDescription)
                        break
                    }
                    
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
        }
        return nil;
    }
    
    func requestAddress(_ method: HTTPMethod, url : String, parameters: Parameters?, successHandler: @escaping (_ response:Any)->(), failureHandler: @escaping (_ errorMessage : String)->()){
        
        //If Internet is not Reachable
        if(MTReachabilityManager.isReachable() == false) {
            
            failureHandler(WebServiceCallErrorMessage.ErrorInternetConnectionNotAvailableMessage)
            return
        }
        
        Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseJSON(completionHandler: { (response) in
            
            switch response.result {
                
            case .success(let result):
                
                if let resultDict = result as? [String:Any]{
                    if let result = resultDict[Response.result] as? [[String:Any]]{
                        
                        if result.count > 0{
                            successHandler(result)
                            
                        }else
                        {
                            failureHandler("Server Error")
                        }
                    }
                    else
                    {
                        failureHandler("Server Error")
                    }
                }
                else
                {
                    failureHandler("Server Error")
                }
                break
                
            case .failure(let error):
                failureHandler(error.localizedDescription)
                break
            }
        })
    }
    
    static func cancelAllTask(){
        let sessionManager = Alamofire.SessionManager.default
        sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
            
            dataTasks.forEach {
                print("dataTasks \(String(describing: $0.originalRequest?.url))")
                $0.cancel()
            }
            
            uploadTasks.forEach {
                print("uploadTasks \(String(describing: $0.originalRequest?.url))")
                $0.cancel()
            }
            
            downloadTasks.forEach {
                print("downloadTasks \(String(describing: $0.originalRequest?.url))")
                $0.cancel()
            }
        }
    }
    
    func paramterString(_ parameter : [String:Any]?) -> String {
        
        if parameter == nil {
            return "There is no parameter"
        }
        
        var strParameter = ""
        
        for (key, value) in parameter! {
            
            if value is String {
                strParameter += "\n\(key) : \(value)"
            }
            if value is Int {
                strParameter += "\n\(key) : \(value)"
            }
            if value is UIImage {
                strParameter += "\n\(key) : \(value)"
            }
            if value is Array<UIImage> {
                strParameter += "\n\(key) : Element \(value)"
            }
        }
        return strParameter
    }
    
    func queryString(_ parameter : [String:Any]?) -> String {
        
        if parameter == nil {
            return ""
        }
        
        var arrQuery:[String] = []
        
        for (key, value) in parameter! {
            
            if value is String {
                arrQuery.append("\(key)=\(value)")
            }
            
        }
        return arrQuery.joined(separator: "&")
    }
    
    
    static func showNetworkLog() {
        showLog = true
    }
    
    static func hideNetworkLog() {
        showLog = false
    }
}


class attachmentModel:NSObject{
    var type:String?
    var key:String?
    var val:Data?
    var name:String?
    
    init(name:String , type:String , key:String , val:Data){
        self.type = type
        self.name = name
        self.key = key
        self.val = val
    }
}

struct JSONArrayEncoding: ParameterEncoding {
    private let array: [Parameters]

    init(array: [Parameters]) {
        self.array = array
    }

    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var urlRequest = try urlRequest.asURLRequest()

        let data = try JSONSerialization.data(withJSONObject: array, options: [])

        if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }

        urlRequest.httpBody = data

        return urlRequest
    }
}

