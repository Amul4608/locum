//
//  WebServiceConstant.swift
//
import Foundation
import UIKit
var apnsToken:String?
var fcmID:String = "Not Done Yet"
struct LocationKey {
    static let kLocationName            = "LocationName"
    static let kLocation                = "Location"
    static let kLatitude                = "Latitude"
    static let kLongitude               = "Longitude"
    static let kCity                    = "City"
    static let kCountry                 = "Country"
    static let kZipCode                 = "zipCode"
}
struct ThirdPartyAPI {
  
    static let KEY_GOOGLE_PLACE_API   = "AIzaSyA2rrXRMzIt3ZQYZfN5jWJwXxWmUfAGXSI"
    
}
struct UserDefaultsKey {
    static let user = "UserKey"
    static let city = "City"
}

struct DateFormat {
    static let DateOfBirth = "yyyy-MM-dd"
}

enum TypeData: String {
    case Video     = "video"
    case Podcast   = "podcast"
    case Blog      = "blog"
}
enum Premium: String {
    case Yes   =  "yes"
    case No    =  "no"
}

enum YouTubeLink: String {
    case Yes   =  "yes"
    case No    =  "no"
}
enum NextPage:String
{
    case NoNext   =  "false"
    case YesGo    =  "true"
}

struct  ContentType
{
    static let HomeContent:String       = ""
    static let VideoContent:String      = "video"
    static let PodcastContent:String    = "podcast"
    static let BlogContent:String       = "blog"
    static let ExclusiveContent:String  = "exclusive"
    
}
struct NotificationDissmiss
{
    static let PopupScreen:String           = "PopupScreen"
    static let LoginScreen:String           = "LoginScreen"
    static let RegisterScreen:String        = "RegisterScreen"
    static let ForgottPassScreen:String     = "ForgottPassScreen"
    static let SubcribeScreen:String        = "SubcribeScreen"
}
struct  Static_PageType {
    static let privacyPolicy   : String     = "Privacy Policy"
    static let terms_Condition : String     = "Terms and Conditions"
    static let help_FAQ        : String     = "Help / FAQ"
    static let Subscription    : String     = "Subscription"
}




