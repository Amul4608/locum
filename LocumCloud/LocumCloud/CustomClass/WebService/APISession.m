//
//  APISession.m
//  ELogBookFM
//
//  Created by i-Phone4 on 07/12/15.
//  Copyright © 2015 Mubin Mall. All rights reserved.
//

#import "APISession.h"
@implementation APISession

+(void)apiCallSharedSessionPOST:(NSString *)strURL withReqParam:(NSString *)strPostparams withCompletionHandlar:(void (^) (NSDictionary *dicResult,
                                                                                                                        NSError *error, int status))completionBlock
{
#warning Here Need to set alert setting
//    if (SharedInstance.isNetAvailable == NO)
//    {
//        [@"No internet connection!" showAlertSimpleWithTitle:APP_NAME inViewController:[[[[UIApplication sharedApplication] delegate] window] rootViewController]];
//        return;
//    }

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strURL]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField: @"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];

    [request setTimeoutInterval:30.0];
    
    NSError *err;
    NSData *body = [NSJSONSerialization dataWithJSONObject:strPostparams options:NSJSONWritingPrettyPrinted error:&err];
    //For Printing purpose Only
    NSString* jsonString = [[NSString alloc] initWithData:body encoding:NSUTF8StringEncoding];
    NSLog(@"Parameter JSON: %@", jsonString);
    //
    [request setHTTPBody:body];
    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)body.length] forHTTPHeaderField: @"Content-Length"];

    NSLog(@"\nPOST URL \n%@\n\n Request Data\n %@", [NSURL URLWithString:strURL],jsonString);

    NSURLSession *session = [NSURLSession sharedSession];
//    ShowNetworkActivityIndicator();
    __block NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      dispatch_async(dispatch_get_main_queue(), ^{
//                                          HideNetworkActivityIndicator();
                                      });
                                      
                                      if (error!=nil)
                                      {
                                          completionBlock(nil,error,0);
                                          [task suspend];
                                      }
                                      else
                                      {
                                          NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                          completionBlock(dic,error,1);
                                          [task suspend];
                                      }
                                  }];
    [task resume];
}



+(void)apiCallSharedSessionDataPOST:(NSString *)strURL withReqParam:(NSString *)strPostparams withCompletionHandlar:(void (^) (NSDictionary *dicResult,
                                                                                                                               NSError *error, int status))completionBlock
{
    NSData *postData = [strPostparams dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strURL]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];

    [request setTimeoutInterval:30.0];
    [request setHTTPBody:postData];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[postData length]] forHTTPHeaderField:@"Content-Length"];
    
    NSLog(@"\nPOST URL \n%@\n\n Request Data\n %@", [NSURL URLWithString:strURL],strPostparams);
    
    NSURLSession *session = [NSURLSession sharedSession];
//    ShowNetworkActivityIndicator();
    __block NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
//                                      HideNetworkActivityIndicator();
                                      if (error!=nil)
                                      {
                                          completionBlock(nil,error,0);
                                          [task suspend];
                                      }
                                      else
                                      {
                                          NSString *strDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                          
                                          NSData *objectData = [strDic dataUsingEncoding:NSUTF8StringEncoding];
                                          NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:objectData options:NSJSONReadingMutableContainers                      error:&error];
                                          completionBlock(dic,error,1);
                                          [task suspend];
                                      }
                                  }];
    [task resume];

}
+(void)apiCallSharedSessionGet:(NSString *)strURL withCompletionHandlar:(void (^) (NSDictionary *dicResult, NSError *error, int status))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strURL]];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField: @"Content-Type"];
    NSLog(@"\nGET URL \n%@ \n", [NSURL URLWithString:strURL]);
    
    
    NSURLSession *session = [NSURLSession sharedSession];
//    ShowNetworkActivityIndicator();
    __block NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
//                                      HideNetworkActivityIndicator();
                                      if (error!=nil)
                                      {
                                          completionBlock(nil,error,0);
                                          [task suspend];
                                      }
                                      else
                                      {
                                          NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                                          completionBlock(dic,error,1);
                                          [task suspend];
                                      }
                                  }];
    
    [task resume];
}

#pragma mark - Download Image From URL
-(id)initWithDownloadingFile:(NSString *)URL withImageView:(UIImageView *)imgViewOriginal withIndicator:(UIActivityIndicatorView *)indicator
{
    self = [super init];
    if (self)
    {
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL]];
        [request addValue:@"" forHTTPHeaderField:@"Accept-Encoding"];
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
        NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithRequest:request];
        [downloadTask resume];
    }
    return self;
}
-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    NSData *data = [NSData dataWithContentsOfURL:location];
    if (data!=nil) {
        UIImage *image = [UIImage imageWithData:data];
        if (image!=nil) {
            if (self.onCompletion) {
                self.onCompletion(YES,image);
            }
        }
        else
        {
            if (self.onCompletion) {
                self.onCompletion(NO,image);
            }
        }
    }
    
}
-(void)URLSession:(NSURLSession *)session
     downloadTask:(NSURLSessionDownloadTask *)downloadTask
     didWriteData:(int64_t)bytesWritten
totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    float progress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
//    NSLog(@"%f",progress);
    if(progress>0)
    {
        self.getProgress(progress);
    }
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes {
    
}
+(void)cancelRequest
{
    [self cancelPreviousPerformRequestsWithTarget:self];
}
@end
