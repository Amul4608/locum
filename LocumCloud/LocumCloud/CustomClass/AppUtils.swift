//
//  AppUtils.swift
//


import Foundation
import AVFoundation
import AVKit
import MapKit
import CoreLocation
import Alamofire
import IQKeyboardManagerSwift


//MARK: - DateTime

let dateTimeFormatDefault       = "yyyy-MM-dd HH:mm:ss"
let dateTimeFormatDisplay       = "dd/MM/yy - hh:mm a"
let dateFormatMonthYear         = "dd/MM/yy"
let dateFormatMonthYear1         = "dd-MM-yy"
let dateFormatMonthYearfull     = "MM/yyyy"

let dateFormatDisplayUS         = "MM-dd-yyyy"
let timeFormatDefault           = "HH:mm:ss"
let timeFormatDisplay           = "hh:mm a"     //12:00 AM
let dateTimeFormatNotification  = "MM-dd-yyyy | hh:mm a"    //"dd:MM:yyyy hh:mm:ss"
let dateFormatShort             = "dd MMM"
let dateFormatMessgeList        = "dd MMM yyyy"
let dateFormatMessgeSection     = "MMMM dd, yyyy"
let dateFormatAvailability      = "EEE,MM,dd,yyyy"
let dateFormatMonthYearfullNew  = "MM-yyyy"
let dateFormatPicker            = "EEEE\ndd/MM"
let dateFormatExamSchedule      = "dd/MM/yyyy\nEEEE"
let dateFormatDiplayIND         = "dd/MM/yyyy"
let dateFormatAttendance        = "MM/dd/yy"
let dateFormatOnlyDayMonth          = "dd/MM"
let dateTimeFormatDefaultTimeZone   = "yyyy-MM-dd HH:mm:ss Z"
let dateFormateTimeZone             = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z"

let dateFormatDefault               = "yyyy-MM-dd"
let dateFormatMonthName             = "yyyy-MMM-dd"
let dateFormatTime                  = "HH:mm"
let dateFormatTime24Hour            = "HH:mm:ss"


let datetimeFormatShort             = "dd MMM,hh:mm a"
let dateFormateDisplayFav           = "dd-MMM-yy"

let dateFormateDisplaydash          = "dd-MM-yyyy"
let dateFormatFeed                  = "dd MM yyyy"

let dateTimeFromateDisplaySlash     = "dd/MM/yyyy, HH:mm a"
let dateFormateFullMonthYear        = "MMMM yyyy"
let dateFormatedateFullMonthYear    = "dd MMMM, yyyy"



//MARK: - Log(Print) Utility
func logD(_ message:Any,
          file: String = #file, line: Int = #line,function: String = #function) {
    let str  : NSString = file as NSString
    //   #if DEBUG
    print("[\(str.lastPathComponent)][\(line)][\(function)]\n💜\(message)💜\n")
    //  #endif
}

//MARK: - UIView
//Border
func setDefaultBorder(_ view : UIView, color : UIColor, width : CGFloat)
{
    view.layer.borderColor = color.cgColor
    view.layer.borderWidth = width
}

//Add Shadow To View
func addShadow(views: [UIView], opacity: CGFloat, radius: CGFloat, color: UIColor)
{
    for view in views
    {
        view.layer.shadowOpacity = Float(opacity) //0.7
        view.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
        view.layer.shadowRadius = radius //5.0
        view.layer.shadowColor = color.cgColor
    }
}

//Set Borders
func setBorders(_ arrViews: [UIView], color : UIColor, radius : CGFloat, width : CGFloat)
{
    for view in arrViews
    {
        view.layer.borderWidth = width
        view.layer.borderColor = color.cgColor
        view.layer.masksToBounds = true
        view.layer.cornerRadius = radius
    }
}

func setCornerRadius(_ arrViews : [UIView], radius : CGFloat)
{
    for view in arrViews
    {
        view.layer.masksToBounds = true
        view.layer.cornerRadius = radius
    }
}

func setTextFieldsIndicator(_ txtFields : UITextField, image:UIImage, position: Int)
{
    //position = 0 = left side, position = 1 = right side
    let imgView = UIImageView(image: image)
    imgView.contentMode = UIView.ContentMode.scaleAspectFit
    if (position==1)
    {
        let v = UIView(frame: CGRect(x: 10, y: 0, width: txtFields.frame.size.height, height: txtFields.frame.size.height))
        imgView.frame = CGRect(x: v.bounds.size.height-24, y: 15, width: v.bounds.size.height-30, height: v.bounds.size.height-30)

        v.addSubview(imgView)
        txtFields.rightViewMode = UITextField.ViewMode.always;
        txtFields.rightView = v;
    }
    else
    {
        let v = UIView(frame: CGRect(x: 0, y: 0, width: txtFields.frame.size.height, height: txtFields.frame.size.height))
        imgView.frame = CGRect(x: 0, y: 18, width: v.bounds.size.height-30, height: v.bounds.size.height-30);

        v.addSubview(imgView)
        txtFields.leftViewMode = UITextField.ViewMode.always;
        txtFields.leftView = v;
    }
}

func setTextFieldModification(arrTextField:[UITextField],arrImages :[String],position:Int)
{
    for textField in arrTextField
    {
        setTextFieldsIndicator(textField , image: UIImage(named: arrImages[arrTextField.index(of: textField)!] )! , position: position);
        /*if showIndicatorImage {
            setTextFieldsIndicator(textField , image: UIImage(named: arrImages[arrTextField.index(of: textField)!] as! String)! , position: position);
        }*/
    }
}

//Right Indicator
func setTextFieldIndicator(_ txtField : UITextField, image : UIImage)
{
    let imageView = UIImageView(image: image)
    txtField.rightViewMode = UITextField.ViewMode.always
    imageView.frame = CGRect(x: 0.0, y: 0.0, width: 15.0, height: 15.0)
    imageView.contentMode = UIView.ContentMode.scaleAspectFit
    imageView.clipsToBounds = true
    txtField.rightView = imageView;
}

//Right Indicator ArrTextFields
func setArrTextFieldIndicator(_ arrTextFields: [UITextField], arrImages : [AnyObject])
{
    for txtField in arrTextFields
    {
        setTextFieldIndicator(txtField, image: UIImage(named: arrImages[arrTextFields.index(of: txtField)!] as! String)!)
    }
}

//User Interaction
func setUserInterAction(_ arrViews: [UIView], isOn : Bool)
{
    for view in arrViews
    {
        view.isUserInteractionEnabled = isOn
    }
}

//Label Color
func setLabelColor(_ arrLables: [UILabel], color : UIColor)
{
    for label in arrLables
    {
        label.textColor = color
    }
}

//Text Color
func setTextColor(_ arrtxtflds: [UITextField], color : UIColor)
{
    for txtfld in arrtxtflds
    {
        txtfld.textColor = color
    }
}

//Left Margin
func setLeftPadding(_ txtField: UITextField)
{
    let view = UIView()
    view.frame = CGRect(x: 0.0, y: 0.0, width: 10, height: 20)
    txtField.leftViewMode = UITextField.ViewMode.always
    txtField.leftView = view
}

//Add Gradient
func addGradientToView(x:CGFloat,y:CGFloat,width:CGFloat,height:CGFloat) -> CAGradientLayer {

    let mGradient = CAGradientLayer()
    mGradient.frame = (CGRect(x: x, y: y, width: width, height:height))
    var colors = [CGColor]()
    colors.append(UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor)
    colors.append(UIColor(red: 0, green: 0, blue: 0, alpha: 0).cgColor)
    mGradient.startPoint = CGPoint(x: (UIScreen.main.bounds.size.width), y: 0.9)
    mGradient.endPoint = CGPoint(x: (UIScreen.main.bounds.size.width), y: 0.1)
    mGradient.colors = colors

    return mGradient

}

//MARK: - Set Borders
func addTextFieldsBottomBorder(_ arrTextFields: [UITextField], color: UIColor)
{
    for textfields in arrTextFields
    {
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: textfields.bounds.size.height - 1, width: textfields.bounds.size.width, height: 1.0)
        bottomLine.backgroundColor = color.cgColor
        textfields.borderStyle = UITextField.BorderStyle.none
        textfields.layer.addSublayer(bottomLine)
    }
}

//Add Border to View
func addTopBorderWithColor(_ objView : UIView, color: UIColor, width: CGFloat) {
    let border = CALayer()
    border.backgroundColor = color.cgColor
    border.frame = CGRect(x: 0, y: 0, width: objView.frame.size.width, height: width)
    objView.layer.addSublayer(border)
}

func addBottomBorderWithColor(_ objView : UIView, color: UIColor, width: CGFloat) {
    let border = CALayer()
    border.backgroundColor = color.cgColor
    border.frame = CGRect(x: 0, y: objView.frame.size.height - width, width: objView.frame.size.width, height: width)
    objView.layer.addSublayer(border)
}

func addLeftBorderWithColor(_ objView : UIView, color: UIColor, width: CGFloat) {
    let border = CALayer()
    border.backgroundColor = color.cgColor
    border.frame = CGRect(x: 0, y: 0, width: width, height: objView.frame.size.height)
    objView.layer.addSublayer(border)
}

func addRightBorderWithColor(_ objView : UIView, color: UIColor, width: CGFloat) {
    let border = CALayer()
    border.backgroundColor = color.cgColor
    border.frame = CGRect(x: objView.frame.size.width, y: 0, width: width, height: objView.frame.size.height)
    objView.layer.addSublayer(border)
}

func addTextViewBottomBorder(_ arrTextViews: [IQTextView])
{
    for textView in arrTextViews
    {
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: textView.bounds.height - 1, width: textView.bounds.width, height: 1.0)
        bottomLine.backgroundColor = UIColor.black.cgColor
        textView.layer.addSublayer(bottomLine)
    }
}

//MARK: - Currency Formattion
func getCurrencyFormat(localeIdentifier: String, price: NSNumber) -> String
{
    let currencyFormatter = NumberFormatter()
    currencyFormatter.usesGroupingSeparator = true
    currencyFormatter.numberStyle = NumberFormatter.Style.currency
    // localize to your grouping and decimal separator
    let locale = NSLocale(localeIdentifier: localeIdentifier)
    locale.displayName(forKey:  NSLocale.Key.currencySymbol, value: localeIdentifier)
    currencyFormatter.locale = locale as Locale
    let priceString = currencyFormatter.string(from: price)
    return priceString!

}


//MARK: - Show Map Directions
func getMapDirection(mapView: MKMapView,lattitude: Double, longitude: Double)
{
    let url = "http://maps.apple.com/?saddr=\(mapView.userLocation.coordinate.latitude),\(mapView.userLocation.coordinate.longitude)&daddr=\(lattitude),\(longitude)"
    if #available(iOS 10.0, *) {
        UIApplication.shared.open((NSURL(string: url)! as URL), options: [:], completionHandler: nil)
    } else {
        // Fallback on earlier versions
    }
}



//Convert Json to Data
func jsonToData(json: Any) -> Data? {
    do {
        return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
    } catch let myJSONError {
        print(myJSONError)
    }
    return nil;
}

//Convert from NSData to JSON Object
func dataToJSON(data: Data) -> Any? {
    do {
        return try JSONSerialization.jsonObject(with: data, options: .allowFragments)
    } catch let myJSONError {
        print(myJSONError)
    }
    return nil
}

//MARK: - Get Document Directory Path
func getDocumentDirectoryPath() -> URL{
    return try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
}
//MARK: - Move File
func moveFile(path:String,destinationPath:String) -> (Bool,Error?){
    let fileManager = FileManager.default
    do{
        try fileManager.moveItem(atPath: path, toPath: destinationPath)
        return (true,nil)
    }catch let err as Error{
        return (false,err)
    }
}

//MARK: - Remove File
func removeFile(path:String) {

    let fileManager = FileManager.default

    if fileManager.fileExists(atPath: path) {
        do {
            try fileManager.removeItem(atPath: path)
        }catch {

        }
    }
}

//MARK: - Image Fuctions
//Save image to Directory
func saveImageToDirectory(folderName: String, image: UIImage, imageName: String)
{
    var documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    if folderName != ""
    {
        // Create a new path for the new images folder
        documentsDirectoryURL = documentsDirectoryURL.appendingPathComponent(folderName)
        //var objcBool:ObjCBool = true
        let isExist = FileManager.default.fileExists(atPath: documentsDirectoryURL.path)
        //fileExistsAtPath(imagesDirectoryPath, isDirectory: &objcBool)
        // If the folder with the given path doesn't exist already, create it
        if isExist == false{
            do{
                try FileManager.default.createDirectory(atPath: documentsDirectoryURL.path, withIntermediateDirectories: true, attributes: nil)
            }catch{
                print("Something went wrong while creating a new folder")
            }
        }
    }
    // create a name for your image
    let fileURL = documentsDirectoryURL.appendingPathComponent(imageName)
    if !FileManager.default.fileExists(atPath: fileURL.path) {
        do {
            // UIImagePNGRepresentation(self.imgview.image!)!
            try image.jpegData(compressionQuality: 1.0)?.write(to: fileURL)
            print("Image Added Successfully")

        } catch {
            print(error)
        }
    } else {
        print("Error! Image Not Added \nImage with same identifire is exist")
    }
}

//Get image to Directory
func getImageFromDirectory(folderName: String, imageName: String) -> UIImage?
{
    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
    let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
    let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
    if let dirPath          = paths.first
    {
        var imageURL = URL(fileURLWithPath: "")
        imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(folderName)
        /*
         if folderName != "" {
         imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(folderName)
         }*/
        imageURL = URL(fileURLWithPath: imageURL.path).appendingPathComponent(imageName)
        //print(dirPath)
        let image    = UIImage(contentsOfFile: imageURL.path)
        if image != nil{
            // Do whatever you want with the image
            return image
        }
        else{
            print("Image Not Found")
            return nil
        }
    }

    return nil
}

//Make ImageView Rounded
func roundedImageView(imgView: UIImageView, borderWidth: Float, borderColor: UIColor)
{
    imgView.layer.cornerRadius = imgView.frame.size.width / 2
    imgView.clipsToBounds = true
    imgView.layer.borderWidth = CGFloat(borderWidth)
    imgView.layer.borderColor = borderColor.cgColor
}

//Set tint Color
func setTintColor(imgView : UIImageView, color : UIColor)
{
    imgView.image = imgView.image!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
    imgView.tintColor = color
}

//Resize image
func resizeImage(_ image:UIImage) -> UIImage {
    let maxImageSize = CGFloat(500)
    let originalSize = image.size
    if  originalSize.height>maxImageSize || originalSize.width>maxImageSize
    {
        var newSize = originalSize
        var ratio = originalSize.width/originalSize.height
        if  originalSize.height>originalSize.width
        {
            ratio = originalSize.width/originalSize.height
            newSize = CGSize(width: ratio*maxImageSize, height: maxImageSize)
        }
        else
        {
            ratio = originalSize.height/originalSize.width;
            newSize = CGSize(width: maxImageSize,height: ratio*maxImageSize);
        }
        UIGraphicsBeginImageContext(newSize);
        image.draw(in: CGRect(x: 0,y: 0,width: newSize.width,height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    else
    {
        return image
    }
}


//MARK:- UITableView and UICollectionView configurations
//Configure TableView
func configureTableView(forVC vc:UIViewController, estimatedHeigth : CGFloat, tableView: UITableView)
{
    tableView.dataSource = vc as? UITableViewDataSource
    tableView.delegate = vc as? UITableViewDelegate
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = estimatedHeigth
}

//Configure CollectionView
func configureCollectionView(forVC vc: UIViewController, collectionView:UICollectionView, classname:AnyClass, space:CGFloat)
{
  
    collectionView.dataSource = vc as? UICollectionViewDataSource
    collectionView.delegate = vc as? UICollectionViewDelegate
    collectionView.alwaysBounceVertical = true

    collectionView.register(classname, forCellWithReuseIdentifier: "\(classname)")

    //collectionView.register(cellWithClass: CollectionViewCell.self)
    collectionView.register(UINib(nibName: "\(classname)", bundle: nil), forCellWithReuseIdentifier: "\(classname)")
    //collectionView.register(nib: UINib(nibName: "\(CollectionViewCell.self)", bundle: nil), forCellWithClass: CollectionViewCell.self)

    let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    layout.sectionInset = UIEdgeInsets(top: space, left: space, bottom: space, right: space)
    layout.minimumInteritemSpacing = space
    layout.minimumLineSpacing = space
    collectionView.collectionViewLayout = layout
}

// Add Loading Cell
func showLoadingCell(_ indicatorColor:UIColor) -> UITableViewCell
{
    let cell = UITableViewCell(style: .default, reuseIdentifier: "LoadingCell")
    cell.backgroundColor = UIColor.clear
    cell.selectionStyle = .none
    cell.isUserInteractionEnabled = false

    //cell.textLabel?.text = msg_LoadingMore

    let actIndicator = UIActivityIndicatorView(style: .gray)
    actIndicator.color = indicatorColor
    //actIndicator.center = CGPoint(x: (UIScreen.mainScreen().bounds.size.width/2)-(actIndicator.bounds.size.width/2), y: cell.center.y)
    actIndicator.frame = CGRect(x: 20.0, y: 20.0, width: 20.0, height: 20.0)
    cell.contentView.addSubview(actIndicator)
    actIndicator.startAnimating()
    actIndicator.hidesWhenStopped = true

    //let lblLoading: UILabel     = UILabel(frame: CGRectMake(50, 0, cell.bounds.size.width-70.0, cell.bounds.size.height))
    let lblLoading: UILabel     = UILabel(frame: CGRect(x: 50, y: actIndicator.frame.origin.y, width: cell.bounds.size.width-70.0, height: 20.0))
    lblLoading.text             = "Load more."
    lblLoading.numberOfLines    = 0
    lblLoading.lineBreakMode    = NSLineBreakMode.byWordWrapping
    lblLoading.textColor        = UIColor.lightGray
    lblLoading.textAlignment    = .left   //.Center
    cell.contentView.addSubview(lblLoading)

    return cell
}

//// Add Pull to Refresh
//func addPullToRefresh(toView view: UIView, ctrlRefresh: inout UIRefreshControl?, targetController: UIViewController, refreshMethod: Selector)
//{
//    if ctrlRefresh == nil {
//        ctrlRefresh = UIRefreshControl()
//    }
//    ctrlRefresh!.backgroundColor = UIColor.white
//    ctrlRefresh!.tintColor = UIColor.gray //const_Color_Primary
//    ctrlRefresh!.attributedTitle = NSAttributedString(string: msg_PullToRefersh, attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray/*const_Color_Border_Default*/])
//    ctrlRefresh!.addTarget(targetController, action: refreshMethod, for: .valueChanged)
//    view.addSubview(ctrlRefresh!)
//}
//

//Check Records Available
func checkRecordAvailable(for view: UIView, count: Int,  ctrlRefresh: UIRefreshControl?, targetController: UIViewController, displayMessage: String)
{
    if ctrlRefresh != nil {
        let updateString = "Last Updated at \(Date().dateTimeString(ofStyle: .medium))"
        ctrlRefresh!.attributedTitle = NSAttributedString(string: updateString)
        ctrlRefresh!.endRefreshing()
    }
    
    if count > 0 {
        if view.isKind(of: UITableView.self) {
            (view as! UITableView).reloadData()
            (view as! UITableView).backgroundView = nil
        }
        else if view.isKind(of: UICollectionView.self) {
            (view as! UICollectionView).reloadData()
            (view as! UICollectionView).backgroundView = nil
        }
        else {
            view.viewWithTag(-111)?.removeFromSuperview()
        }
    }
    else {
        let v = UIView(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height))
        v.translatesAutoresizingMaskIntoConstraints = true
        v.tag = -111
        let lblNoData: UILabel     = UILabel(frame: CGRect(x: 15, y: 0, width: v.bounds.size.width-30, height: v.bounds.size.height))
        lblNoData.clipsToBounds = true
        
        lblNoData.text             = displayMessage
        lblNoData.numberOfLines    = 0
        lblNoData.lineBreakMode    = NSLineBreakMode.byWordWrapping
        lblNoData.textColor        = UIColor.darkGray
        lblNoData.textAlignment    = .center
//        lblNoData.tag = -111
        v.addSubview(lblNoData)
        if view.isKind(of: UITableView.self) {
            (view as! UITableView).reloadData()
            (view as! UITableView).backgroundView = v
            (view as! UITableView).separatorStyle = .none
        }
        else if view.isKind(of: UICollectionView.self) {
            (view as! UICollectionView).reloadData()
            (view as! UICollectionView).backgroundView = lblNoData
        }
        else {
            view.addSubview(lblNoData)
        }
    }
}

//MARK: - Calling Number
func callNumber(_ phoneNumber:String) {
    if let phoneCallURL:URL = URL(string:"tel://\(phoneNumber)") {
        let application:UIApplication = UIApplication.shared
        if (application.canOpenURL(phoneCallURL)) {
            if #available(iOS 10.0, *) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }
    }
}

//MARK: - Play Audio
func playAudio(_ fileName: String)
{
    let resourcePath = Bundle.main.resourcePath!
    let filePath = "\(resourcePath)/" + "\(fileName)"
    print(filePath)
    let url: URL = URL(fileURLWithPath: filePath)
    let playerObject = AVPlayer(url: url)
    let playerController = AVPlayerViewController()
    playerController.player = playerObject
    playerObject.play()
}

//MARK:- Open Translate Url
func openTranslateUrl(withText: String)
{
    let txtAppend = withText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
    let url = "https://translate.google.com/#auto/en/\(txtAppend!)"
    let openUrl = NSURL(string: url)
    if #available(iOS 10.0, *) {
        UIApplication.shared.open(openUrl! as URL, options: [:], completionHandler: nil)
    } else {
        UIApplication.shared.openURL(openUrl! as URL)
    }
}


//MARK:- Show Massage
func showMessage( title : String, viewController: UIViewController)
{
    let alert = UIAlertController(title: "",
                                  message: title,
                                  preferredStyle: .alert)

    let cancelAction = UIAlertAction(title: "OK",
                                     style: .default)
    alert.addAction(cancelAction)
    viewController.present(alert, animated: true, completion: nil)
}

//MARK:- Generate Random String/Password
func randomAlphaNumericString(length: Int) -> String {
    let allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    let allowedCharsCount = UInt32(allowedChars.count)
    var randomString = ""

    for _ in 0..<length {
        let randomNum = Int(arc4random_uniform(allowedCharsCount))
        let randomIndex = allowedChars.index(allowedChars.startIndex, offsetBy: randomNum)
        let newCharacter = allowedChars[randomIndex]
        randomString += String(newCharacter)
    }
    return randomString
}

//MARK: - User Default
//Set Value
func setToUserDefaultForKey(_ value:AnyObject?,key:String)
{
    UserDefaults.standard.set(value, forKey: key)
    UserDefaults.standard.synchronize()
}

//Set Archive Value
func setToUserDefaultForKeyByArchive(_ value:AnyObject?,key:String)
{
    UserDefaults.standard.set(value == nil ? nil : NSKeyedArchiver.archivedData(withRootObject: value!), forKey: key)
    UserDefaults.standard.synchronize()
}

//Get Value
func getFromUserDefaultForKey(_ key:String)->AnyObject?
{
    return UserDefaults.standard.object(forKey: key) as AnyObject?
}

//Get UnArchive Value
func getFromUserDefaultForKeyByUnArchive(_ key:String)->AnyObject?
{
    return UserDefaults.standard.object(forKey: key) == nil ? nil :NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: key) as! Data) as AnyObject?
}

//Remove Value
func removeFromUserDefaultForKey(_ key:String)
{
    UserDefaults.standard.removeObject(forKey: key)
}

//MARK: - Date-Time Formattion

func getDisplayDate(date: String, dateFormat : String, displayFormat : String) -> String {
    let datetimeFormatOriginal = DateFormatter()
    datetimeFormatOriginal.dateFormat = dateFormat
    let datetimeFormatDisplay = DateFormatter()
    datetimeFormatDisplay.dateFormat = displayFormat
    if date == "" || date == "-" {
        return ""
    }
    else {
        guard let tempDate = datetimeFormatOriginal.date(from: date)else {
            return "-"
        }
        return datetimeFormatDisplay.string(from: tempDate)
        }
}


func getDateString(_ datetime: Date, format : String) -> String {
    //let currentdate: NSDate = NSDate()
    let dateFormat: DateFormatter = DateFormatter()
    dateFormat.dateFormat = format
    // format
    return dateFormat.string(from: datetime)
}

func getDefaultDate(_ datetime: String, format : String) -> Date {
    let dateFormat: DateFormatter = DateFormatter()
    dateFormat.dateFormat = format
    
    return dateFormat.date(from: datetime)!
}

func getDateFromTimeStamp(_ unixtimeInterval: Double, format : String) -> String {
    let date = Date(timeIntervalSince1970: unixtimeInterval)
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format //Specify your format that you want
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    let strDate = dateFormatter.string(from: date)
    return strDate
}
//UTC to Local timezone
func UTCToLocal(date:String,formate:String,displayFormate:String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = formate
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    
    let dt = dateFormatter.date(from: date)
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = displayFormate//"h:mm a"
    
    return dateFormatter.string(from: dt!)
}

//Local to UTC timezone
func localToUTC(date:String,Localeformate:String,UTCFormate:String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = UTCFormate //"h:mm a"
    dateFormatter.calendar = NSCalendar.current
    dateFormatter.timeZone = TimeZone.current
    
    let dt = dateFormatter.date(from: date)
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    dateFormatter.dateFormat = Localeformate//"H:mm:ss"
    
    return dateFormatter.string(from: dt!)
}
//MARK: Date Difference
func timeGapBetweenDates(previousDate : String,currentDate : String)
{
    let dateString1 = previousDate
    let dateString2 = currentDate
    
    let Dateformatter = DateFormatter()
    Dateformatter.dateFormat = dateTimeFormatDefaultTimeZone    //dateTimeFormatDefault
    
    let date1 = Dateformatter.date(from: dateString1)
    let date2 = Dateformatter.date(from: dateString2)
    
    let distanceBetweenDates: TimeInterval? = date2?.timeIntervalSince(date1!)
    let secondsInAnHour: Double = 3600
    let minsInAnHour: Double = 60
    let secondsInDays: Double = 86400
    let secondsInWeek: Double = 604800
    let secondsInMonths : Double = 2592000
    let secondsInYears : Double = 31104000
    
    let minBetweenDates = Int((distanceBetweenDates! / minsInAnHour))
    let hoursBetweenDates = Int((distanceBetweenDates! / secondsInAnHour))
    let daysBetweenDates = Int((distanceBetweenDates! / secondsInDays))
    let weekBetweenDates = Int((distanceBetweenDates! / secondsInWeek))
    let monthsbetweenDates = Int((distanceBetweenDates! / secondsInMonths))
    let yearbetweenDates = Int((distanceBetweenDates! / secondsInYears))
    let secbetweenDates = Int(distanceBetweenDates!)
    
    if yearbetweenDates > 0 {
        print(yearbetweenDates,"years")//0 years
    }
    else if monthsbetweenDates > 0 {
        print(monthsbetweenDates,"months")//0 months
    }
    else if weekBetweenDates > 0 {
        print(weekBetweenDates,"weeks")//0 weeks
    }
    else if daysBetweenDates > 0 {
        print(daysBetweenDates,"days")//5 days
    }
    else if hoursBetweenDates > 0 {
        print(hoursBetweenDates,"hours")//120 hours
    }
    else if minBetweenDates > 0 {
        print(minBetweenDates,"minutes")//7200 minutes
    }
    else if secbetweenDates > 0 {
        print(secbetweenDates,"seconds")//seconds
    }
}
func isPastDate(previousDate : String,currentDate : String) -> Bool
{
    let dateString1 = previousDate
    let dateString2 = currentDate
    
    let Dateformatter = DateFormatter()
    Dateformatter.dateFormat = dateTimeFormatDefaultTimeZone    //dateTimeFormatDefault
    
    let date1 = Dateformatter.date(from: dateString1)
    let date2 = Dateformatter.date(from: dateString2)
    
    if date1 == nil || date2 == nil {
        return true
    }
    
    let distanceBetweenDates: TimeInterval? = date2?.timeIntervalSince(date1!)
    
    let secondsInDays: Double = 86400
    
    let daysBetweenDates = Int((distanceBetweenDates! / secondsInDays))
    
    
    if daysBetweenDates > 0 {
        return true
    }
    return false
}

//MARK:- App Functions
//MARK: Rate The Application
func rateTheApp(_ appId: String) {
    //http://itunes.apple.com/app/id284815942
    if let url = URL(string: "itms-apps://itunes.apple.com/app/id"+appId),
        UIApplication.shared.canOpenURL(url)
    {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}
//MARK: Open The Application
func openTheApp(_ appId: String) {
    //let urlStr = "itms://itunes.apple.com/app/apple-store/id375380948?mt=8"
    let urlStr = "itms://itunes.apple.com/app/apple-store/id"+appId+"?mt=8"
    if #available(iOS 10.0, *) {
        UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
    } else {
        UIApplication.shared.openURL(URL(string: urlStr)!)
    }
}


func convertDateToCustomFormat(datetime: String, format : String) -> String
{
    let date = getDefaultDate(datetime,format: format) //dateTimeFormatDefault
    let timeDifference = Date().timeIntervalSince(date)

    let minutes = timeDifference / 60
    let hours = minutes / 60
    let days = minutes / 1440

    
    if (days > 7) {
        
         return getDateString(date, format: dateFormatDiplayIND)
    }
   if(days >= 1 && days <= 7) {
        if (Int(days) == 1) {
             return "\(Int(days)) day ago"
        }
        else {
             return "\(Int(days)) days ago"
        }
    }
    else if(hours>=1) {
        if (Int(hours) == 1) {
            return "\(Int(hours)) hour ago"
        }
        else {
            return "\(Int(hours)) hours ago"
        }
    }
    else if(minutes >= 1) {
        if (Int(minutes) == 1) {
            return "\(Int(minutes)) minute ago"
        }
        else {
             return "\(Int(minutes)) minutes ago"
        }
    }
    else {
        return "just now"
    }
    
}
//MARK: - Modify Text Style in WebView
func modifyTextStyleInLoadURL(into webView: UIWebView, fontFamily: String, fontStyle:String, fontSize: String ,colorHexCode: String ) {
    let cssString = "body {font-family: \(fontFamily); font-style: \(fontStyle); font-size: \(fontSize); color: #\(colorHexCode) }"
    let jsString = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style);"
    webView.stringByEvaluatingJavaScript(from: jsString)
}

////MARK: - Get Network Time
//func getNetworkTime() -> String {
//    let formate = DateFormatter()
//    formate.dateFormat = dateFormatDateMonthYearFull
//    return formate.string(from: NetworkClock.shared().networkTime)
//}

//MARK: - Get File size
func getFileSize(filePath:String) -> Float64 {
    do {
        let attributes = try FileManager.default.attributesOfItem(atPath: filePath)
        let fileSize = attributes[FileAttributeKey.size] as! Float64
        return fileSize / 1000000
    }catch {
        return 0.0
    }
}

//MARK: - Get Video file duration
func getDuration(fileUrl:URL) -> Float64 {
    let asset = AVAsset(url: fileUrl)
    let duration = asset.duration
    return CMTimeGetSeconds(duration)
}
//Default App-Rating
/*if #available(iOS 10.3, *) {
    SKStoreReviewController.requestReview()
} else {
    // Fallback on earlier versions
}*/

//MARK: - Check for Valid Email
func isValidEmail(strEmail:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: strEmail)
}
//MARK: - Check for Valid Name
//check for at least one alphabetic character
func isValidName(strName:String) -> Bool {
    //Only Capital letters ==  .*[A-Z]+.*
    //Support Special character ====== .*[!&^%$#@()/]+.*
    let NameRegEx = "(?s)[^A-Za-z]*[A-Za-z].*"
    let NameTest = NSPredicate(format:"SELF MATCHES %@", NameRegEx)
    return NameTest.evaluate(with: strName)
}
//MARK: - Check for Valid Mobile Number
func isValidMobileNumber(strMobileNumber:String) -> Bool {
    //Only Capital letters ==  .*[A-Z]+.*
    //Support Special character ====== .*[!&^%$#@()/]+.*
    let MobileNumberRegEx = "^[0-9]*$"
    let MobileNumberTest = NSPredicate(format:"SELF MATCHES %@", MobileNumberRegEx)
    return MobileNumberTest.evaluate(with: strMobileNumber)
}
//MARK:- DATE SUFFIX
func daySuffix(from date: Date) -> [String] {
    let calendar = Calendar.current
    let dayOfMonth = calendar.component(.day, from: date)
    switch dayOfMonth {
    case 1, 21, 31: return ["\(dayOfMonth)" ,"st"]
    case 2, 22: return ["\(dayOfMonth)", "nd"]
    case 3, 23: return ["\(dayOfMonth)","rd"]
    default: return ["\(dayOfMonth)","th"]
    }
}

//MARK:- Get a diaplay Date With suffix format
func getDisplaySuffixDate(date : String, dateFormat:String, displayFormat:String) -> NSAttributedString{
    let fontSuper:UIFont? = UIFont.systemFont(ofSize: 10)
    var tempDate = daySuffix(from: getDefaultDate(date, format: dateFormat))
    print(tempDate)
    let location = tempDate[0].count
    let attString:NSMutableAttributedString = NSMutableAttributedString(string: "\(tempDate[0])\(tempDate[1])")
    attString.setAttributes([.font:fontSuper!,.baselineOffset:10], range: NSRange(location:location,length:2))
    let monthYear = NSMutableAttributedString(string: " \(getDisplayDate(date: date, dateFormat: dateFormat, displayFormat: displayFormat))")
    let str = NSAttributedString(attributedString: attString)
    let str1 = NSAttributedString(attributedString: monthYear)
    let strf = "\(str) \(str1)"
    return NSAttributedString(string: strf)
}

//Set Button Shadow
func btnShadowSet(btn:UIButton) {
    btn.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
    btn.layer.shadowOffset = CGSize(width: 0.0, height: 10.0)
    btn.layer.shadowOpacity = 0.38
 //   btn.layer.shadowRadius = 0.0
    btn.layer.masksToBounds = false
}
