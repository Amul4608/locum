//
//  NHrange.swift
//  247 Zay
//
//  Created by AmulCGM on 02/06/21.
//

import Foundation
import UIKit

/// enum for label positions
public enum NHSliderLabelStyle : Int {
    /// lower and upper labels stick to the left and right of slider
    case STICKY
    /// lower and upper labels follow position of lower and upper thumbs
    case FOLLOW
}

/// delegate for changed value
public protocol NHRangeSliderViewDelegate: class {
    /// slider value changed
    func sliderValueChanged(slider: NHRangeSlider?)
}

/// optional implementation
public extension NHRangeSliderViewDelegate{
    func sliderValueChanged(slider: NHRangeSlider?){}
}

/// Range slider with labels for upper and lower thumbs, title label and configurable step value (optional)
open class NHRangeSliderView: UIView {
    //MARK: properties
    open var delegate: NHRangeSliderViewDelegate? = nil
    /// Range slider
    open var rangeSlider : NHRangeSlider? = nil
    /// Display title
    open var titleLabel : UILabel? = nil
    // lower value label for displaying selected lower value
    open var lowerLabel : UILabel? = nil
    /// upper value label for displaying selected upper value
    open var upperLabel : UILabel? = nil
    /// display format for lower value. Default to %.0f to display value as Int
    open var lowerDisplayStringFormat: String = "%.0f" {
        didSet {
            updateLabelDisplay()
        }
    }
    /// display format for upper value. Default to %.0f to display value as Int
    open var upperDisplayStringFormat: String = "%.0f" {
        didSet {
            updateLabelDisplay()
        }
    }
    /// vertical spacing
    open var spacing: CGFloat = 4.0
    /// position of thumb labels. Set to STICKY to stick to left and right positions. Set to FOLLOW to follow left and right thumbs
    open var thumbLabelStyle: NHSliderLabelStyle = .STICKY
    /// minimum value
    @IBInspectable open var minimumValue: Double = 0.0 {
        didSet {
            self.rangeSlider?.minimumValue = minimumValue
        }
    }
    /// max value
    @IBInspectable open var maximumValue: Double = 100.0 {
        didSet {
            self.rangeSlider?.maximumValue = maximumValue
        }
    }
    /// value for lower thumb
    @IBInspectable open var lowerValue: Double = 0.0 {
        didSet {
            self.rangeSlider?.lowerValue = lowerValue
            self.updateLabelDisplay()
        }
    }
    /// value for upper thumb
    @IBInspectable open var upperValue: Double = 100.0 {
        didSet {
            self.rangeSlider?.upperValue = upperValue
            self.updateLabelDisplay()
        }
    }
    /// stepValue. If set, will snap to discrete step points along the slider . Default to nil
    open var stepValue: Double? = nil {
        didSet {
            self.rangeSlider?.stepValue = stepValue
        }
    }
    /// minimum distance between the upper and lower thumbs.
    open var gapBetweenThumbs: Double = 2.0 {
        didSet {
            self.rangeSlider?.gapBetweenThumbs = gapBetweenThumbs
        }
    }
    /// tint color for track between 2 thumbs
    @IBInspectable open var trackTintColor: UIColor = UIColor(white: 0.9, alpha: 1.0) {
        didSet {
            self.rangeSlider?.trackTintColor = trackTintColor
        }
    }
    /// track highlight tint color
    @IBInspectable open var trackHighlightTintColor: UIColor = UIColor(red: 0.0, green: 0.45, blue: 0.94, alpha: 1.0) {
        didSet {
            self.rangeSlider?.trackHighlightTintColor = trackHighlightTintColor
        }
    }
    /// thumb tint color
    @IBInspectable open var thumbTintColor: UIColor = UIColor.white {
        didSet {
            self.rangeSlider?.thumbTintColor = thumbTintColor
        }
    }
    /// thumb border color
    @IBInspectable open var thumbBorderColor: UIColor = UIColor.gray {
        didSet {
            self.rangeSlider?.thumbBorderColor = thumbBorderColor
        }
    }
    /// thumb border width
    @IBInspectable open var thumbBorderWidth: CGFloat = 0.5 {
        didSet {
            self.rangeSlider?.thumbBorderWidth = thumbBorderWidth
            
        }
    }
    /// set 0.0 for square thumbs to 1.0 for circle thumbs
    @IBInspectable open var curvaceousness: CGFloat = 1.0 {
        didSet {
            self.rangeSlider?.curvaceousness = curvaceousness
        }
    }
    /// thumb width and height
    @IBInspectable open var thumbSize: CGFloat = 32.0 {
        didSet {
            if let slider = self.rangeSlider {
                var oldFrame = slider.frame
                oldFrame.size.height = thumbSize
                slider.frame = oldFrame
            }
        }
    }
    //MARK: init
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    /// setup
    open func setup() {
        self.autoresizingMask = [.flexibleWidth]
        self.titleLabel = UILabel(frame: .zero)
        self.titleLabel?.numberOfLines = 1
        self.titleLabel?.font = UIFont.systemFont(ofSize: 15.0)
        self.titleLabel?.text = ""
        self.addSubview(self.titleLabel!)
        self.lowerLabel = UILabel(frame: .zero)
        self.lowerLabel?.numberOfLines = 1
        self.lowerLabel?.font = UIFont.systemFont(ofSize: 15.0)
        self.lowerLabel?.text = ""
        self.lowerLabel?.textAlignment = .center
        self.addSubview(self.lowerLabel!)
        self.upperLabel = UILabel(frame: .zero)
        self.upperLabel?.numberOfLines = 1
        self.upperLabel?.font = UIFont.systemFont(ofSize: 15.0)
        self.upperLabel?.text = ""
        self.upperLabel?.textAlignment = .center
        self.addSubview(self.upperLabel!)
        self.rangeSlider = NHRangeSlider(frame: .zero)
        self.addSubview(self.rangeSlider!)
        self.updateLabelDisplay()
        self.rangeSlider?.addTarget(self, action: #selector(self.rangeSliderValueChanged(_:)), for: .valueChanged)
    }
    
    //MARK: range slider delegage
    /// Range slider change events. Upper / lower labels will be updated accordingly.
    /// Selected value for filterItem will also be updated
    ///
    /// - Parameter rangeSlider: the changed rangeSlider
    @objc open func rangeSliderValueChanged(_ rangeSlider: NHRangeSlider) {
        delegate?.sliderValueChanged(slider: rangeSlider)
        self.updateLabelDisplay()
    }
    
    //MARK: -
    // update labels display
    open func updateLabelDisplay() {
        self.lowerLabel?.text = String(format: self.lowerDisplayStringFormat, rangeSlider!.lowerValue )
        self.upperLabel?.text = String(format: self.upperDisplayStringFormat, rangeSlider!.upperValue )
        if self.lowerLabel != nil {
            // for stepped value we animate the labels
            if self.stepValue != nil && self.thumbLabelStyle == .FOLLOW {
                UIView.animate(withDuration: 0.1, animations: {
                    self.layoutSubviews()
                })
            } else {
                self.setNeedsLayout()
                self.layoutIfNeeded()
            }
        }
    }
    
    /// layout subviews
    override open func layoutSubviews() {
        super.layoutSubviews()
        if let titleLabel = self.titleLabel , let lowerLabel = self.lowerLabel ,
            let upperLabel = self.upperLabel , let rangeSlider = self.rangeSlider {
            let commonWidth = self.bounds.width
            var titleLabelMaxY : CGFloat = 0
            if !titleLabel.isHidden && titleLabel.text != nil && titleLabel.text!.count > 0 {
                titleLabel.frame = CGRect(x: 0,
                                          y: 0,
                                          width: commonWidth  ,
                                          height: (titleLabel.font.lineHeight + self.spacing ) )
                titleLabelMaxY = titleLabel.frame.origin.y + titleLabel.frame.size.height
            }
            rangeSlider.frame = CGRect(x: 0,
                                       y: titleLabelMaxY + lowerLabel.font.lineHeight + self.spacing,
                                       width: commonWidth ,
                                       height: thumbSize )
            
            let lowerWidth = self.estimatelabelSize(font: lowerLabel.font, string: lowerLabel.text!, constrainedToWidth: Double(commonWidth)).width
            let upperWidth = self.estimatelabelSize(font: upperLabel.font, string: upperLabel.text!, constrainedToWidth: Double(commonWidth)).width
            var lowerLabelX : CGFloat = 0
            var upperLabelX : CGFloat = 0
            if self.thumbLabelStyle == .FOLLOW {
                lowerLabelX = rangeSlider.lowerThumbLayer.frame.midX  - lowerWidth / 2
                upperLabelX = rangeSlider.upperThumbLayer.frame.midX  - upperWidth / 2
            } else {
                // fix lower label to left and upper label to right
                lowerLabelX = rangeSlider.frame.origin.x + self.spacing
                upperLabelX = rangeSlider.frame.origin.x + rangeSlider.frame.size.width - thumbSize + self.spacing
            }
            lowerLabel.frame = CGRect(      x: lowerLabelX,
                                            y: titleLabelMaxY,
                                            width: lowerWidth ,
                                            height: lowerLabel.font.lineHeight + self.spacing )
            
            upperLabel.frame = CGRect(      x: upperLabelX,
                                            y: titleLabelMaxY,
                                            width: upperWidth ,
                                            height: upperLabel.font.lineHeight + self.spacing )
        }
    }
    
    // return the best size that fit within the box
    open override func sizeThatFits(_ size: CGSize) -> CGSize {
        if let titleLabel = self.titleLabel , let lowerLabel = self.lowerLabel {
            var height : CGFloat = 0
            var titleLabelMaxY : CGFloat = 0
            if !titleLabel.isHidden && titleLabel.text != nil && titleLabel.text!.count > 0 {
                titleLabelMaxY = titleLabel.font.lineHeight + self.spacing
            }
            height = titleLabelMaxY + lowerLabel.font.lineHeight + self.spacing + thumbSize
            return CGSize(width: size.width, height: height)
        }
        return size
    }
    
    /// get size for string of this font
    ///
    /// - parameter font: font
    /// - parameter string: a string
    /// - parameter width:  constrained width
    ///
    /// - returns: string size for constrained width
    private func estimatelabelSize(font: UIFont,string: String, constrainedToWidth width: Double) -> CGSize{
        return string.boundingRect(with: CGSize(width: width, height: .greatestFiniteMagnitude),
                                   options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                   attributes: [NSAttributedString.Key.font: font],
                                   context: nil).size
    }
}

import UIKit
import QuartzCore

/// Range slider track layer. Responsible for drawing the horizontal track
public class RangeSliderTrackLayer: CALayer {
    /// owner slider
    weak var rangeSlider: NHRangeSlider?
    /// draw the track between 2 thumbs
    ///
    /// - Parameter ctx: current graphics context
    override open func draw(in ctx: CGContext) {
        guard let slider = rangeSlider else {
            return
        }
        // Clip
        let cornerRadius = bounds.height * slider.curvaceousness / 2.0
        let path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        ctx.addPath(path.cgPath)
        // Fill the track
        ctx.setFillColor(slider.trackTintColor.cgColor)
        ctx.addPath(path.cgPath)
        ctx.fillPath()
        // Fill the highlighted range
        ctx.setFillColor(slider.trackHighlightTintColor.cgColor)
        let lowerValuePosition = CGFloat(slider.positionForValue(slider.lowerValue))
        let upperValuePosition = CGFloat(slider.positionForValue(slider.upperValue))
        let rect = CGRect(x: lowerValuePosition, y: 0.0, width: upperValuePosition - lowerValuePosition, height: bounds.height)
        ctx.fill(rect)
    }
}

/// the thumb for upper , lower bounds
public class RangeSliderThumbLayer: CALayer {
    /// owner slider
    weak var rangeSlider: NHRangeSlider?
    /// whether this thumb is currently highlighted i.e. touched by user
    public var highlighted: Bool = false {
        didSet {
            setNeedsDisplay()
        }
    }
    /// stroke color
    public var strokeColor: UIColor = UIColor.gray {
        didSet {
            setNeedsDisplay()
        }
    }
    /// line width
    public var lineWidth: CGFloat = 0.5 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    /// draw the thumb
    ///
    /// - Parameter ctx: current graphics context
    override open func draw(in ctx: CGContext) {
        guard let slider = rangeSlider else {
            return
        }
        let thumbFrame = bounds.insetBy(dx: 2.0, dy: 2.0)
        let cornerRadius = thumbFrame.height * slider.curvaceousness / 2.0
        let thumbPath = UIBezierPath(roundedRect: thumbFrame, cornerRadius: cornerRadius)
        // Fill
        ctx.setFillColor(slider.thumbTintColor.cgColor)
        ctx.addPath(thumbPath.cgPath)
        ctx.fillPath()
        // Outline
        ctx.setStrokeColor(strokeColor.cgColor)
        ctx.setLineWidth(lineWidth)
        ctx.addPath(thumbPath.cgPath)
        ctx.strokePath()
        if highlighted {
            ctx.setFillColor(UIColor(white: 0.0, alpha: 0.1).cgColor)
            ctx.addPath(thumbPath.cgPath)
            ctx.fillPath()
        }
    }
}


/// Range slider view with upper, lower bounds
@IBDesignable
open class NHRangeSlider: UIControl {
    //MARK: properties
    /// minimum value
    @IBInspectable open var minimumValue: Double = 0.0 {
        willSet(newValue) {
            assert(newValue < maximumValue, "NHRangeSlider: minimumValue should be lower than maximumValue")
        }
        didSet {
            updateLayerFrames()
        }
    }
    /// max value
    @IBInspectable open var maximumValue: Double = 100.0 {
        willSet(newValue) {
            assert(newValue > minimumValue, "NHRangeSlider: maximumValue should be greater than minimumValue")
        }
        didSet {
            updateLayerFrames()
        }
    }
    /// value for lower thumb
    @IBInspectable open var lowerValue: Double = 0.0 {
        didSet {
            if lowerValue < minimumValue {
                lowerValue = minimumValue
            }
            updateLayerFrames()
        }
    }
    /// value for upper thumb
    @IBInspectable open var upperValue: Double = 100.0 {
        didSet {
            if upperValue > maximumValue {
                upperValue = maximumValue
            }
            updateLayerFrames()
        }
    }
    /// stepValue. If set, will snap to discrete step points along the slider . Default to nil
    open var stepValue: Double? = nil {
        willSet(newValue) {
            if newValue != nil {
                assert(newValue! > 0, "NHRangeSlider: stepValue must be positive")
            }
        }
        didSet {
            if let val = stepValue {
                if val <= 0 {
                    stepValue = nil
                }
            }
            updateLayerFrames()
        }
    }
    
    /// minimum distance between the upper and lower thumbs.
    @IBInspectable open var gapBetweenThumbs: Double = 2.0
    /// tint color for track between 2 thumbs
    @IBInspectable open var trackTintColor: UIColor = UIColor(white: 0.9, alpha: 1.0) {
        didSet {
            trackLayer.setNeedsDisplay()
        }
    }
    /// track highlight tint color
    @IBInspectable open var trackHighlightTintColor: UIColor = UIColor(red: 0.0, green: 0.45, blue: 0.94, alpha: 1.0) {
        didSet {
            trackLayer.setNeedsDisplay()
        }
    }
    /// thumb tint color
    @IBInspectable open var thumbTintColor: UIColor = UIColor.white {
        didSet {
            lowerThumbLayer.setNeedsDisplay()
            upperThumbLayer.setNeedsDisplay()
        }
    }
    /// thumb border color
    @IBInspectable open var thumbBorderColor: UIColor = UIColor.gray {
        didSet {
            lowerThumbLayer.strokeColor = thumbBorderColor
            upperThumbLayer.strokeColor = thumbBorderColor
        }
    }
    /// thumb border width
    @IBInspectable open var thumbBorderWidth: CGFloat = 0.5 {
        didSet {
            lowerThumbLayer.lineWidth = thumbBorderWidth
            upperThumbLayer.lineWidth = thumbBorderWidth
        }
    }
    /// set 0.0 for square thumbs to 1.0 for circle thumbs
    @IBInspectable open var curvaceousness: CGFloat = 1.0 {
        didSet {
            if curvaceousness < 0.0 {
                curvaceousness = 0.0
            }
            if curvaceousness > 1.0 {
                curvaceousness = 1.0
            }
            trackLayer.setNeedsDisplay()
            lowerThumbLayer.setNeedsDisplay()
            upperThumbLayer.setNeedsDisplay()
        }
    }
    /// previous touch location
    fileprivate var previouslocation = CGPoint()
    /// track layer
    fileprivate let trackLayer = RangeSliderTrackLayer()
    /// lower thumb layer
    public let lowerThumbLayer = RangeSliderThumbLayer()
    /// upper thumb layer
    public let upperThumbLayer = RangeSliderThumbLayer()
    /// thumb width
    fileprivate var thumbWidth: CGFloat {
        return CGFloat(bounds.height)
    }
    /// frame
    override open var frame: CGRect {
        didSet {
            updateLayerFrames()
        }
    }
    //MARK: init methods
    override public init(frame: CGRect) {
        super.init(frame: frame)
        initializeLayers()
    }
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        initializeLayers()
    }
    //MARK: layers
    /// layout sub layers
    ///
    /// - Parameter of: layer
    override open func layoutSublayers(of: CALayer) {
        super.layoutSublayers(of:layer)
        updateLayerFrames()
    }
    
    /// init layers
    fileprivate func initializeLayers() {
        layer.backgroundColor = UIColor.clear.cgColor
        trackLayer.rangeSlider = self
        trackLayer.contentsScale = UIScreen.main.scale
        layer.addSublayer(trackLayer)
        lowerThumbLayer.rangeSlider = self
        lowerThumbLayer.contentsScale = UIScreen.main.scale
        layer.addSublayer(lowerThumbLayer)
        upperThumbLayer.rangeSlider = self
        upperThumbLayer.contentsScale = UIScreen.main.scale
        layer.addSublayer(upperThumbLayer)
    }
    
    /// update layer frames
    open func updateLayerFrames() {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        trackLayer.frame = bounds.insetBy(dx: 0.0, dy: bounds.height/3)
        trackLayer.setNeedsDisplay()
        let lowerThumbCenter = CGFloat(positionForValue(lowerValue))
        lowerThumbLayer.frame = CGRect(x: lowerThumbCenter - thumbWidth/2.0, y: 0.0, width: thumbWidth, height: thumbWidth)
        lowerThumbLayer.setNeedsDisplay()
        let upperThumbCenter = CGFloat(positionForValue(upperValue))
        upperThumbLayer.frame = CGRect(x: upperThumbCenter - thumbWidth/2.0, y: 0.0, width: thumbWidth, height: thumbWidth)
        upperThumbLayer.setNeedsDisplay()
        CATransaction.commit()
    }
    
    /// thumb x position for new value
    open func positionForValue(_ value: Double) -> Double {
        if (maximumValue == minimumValue) {
            return 0
        }
        return Double(bounds.width - thumbWidth) * (value - minimumValue) / (maximumValue - minimumValue)
            + Double(thumbWidth/2.0)
    }
    
    /// bound new value within lower and upper value
    ///
    /// - Parameters:
    ///   - value: value to set
    ///   - lowerValue: lower value
    ///   - upperValue: upper value
    /// - Returns: current value
    open func boundValue(_ value: Double, toLowerValue lowerValue: Double, upperValue: Double) -> Double {
        return min(max(value, lowerValue), upperValue)
    }
    
    // MARK: - Touches
    /// begin tracking
    override open func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        previouslocation = touch.location(in: self)
        // set highlighted positions for lower and upper thumbs
        if lowerThumbLayer.frame.contains(previouslocation) {
            lowerThumbLayer.highlighted = true
        }
        if upperThumbLayer.frame.contains(previouslocation) {
            upperThumbLayer.highlighted = true
        }
        return lowerThumbLayer.highlighted || upperThumbLayer.highlighted
    }
    
    /// update positions for lower and upper thumbs
    override open func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        let location = touch.location(in: self)
        // Determine by how much the user has dragged
        let deltaLocation = Double(location.x - previouslocation.x)
        var deltaValue : Double = 0
        if (bounds.width != bounds.height) {
            deltaValue = (maximumValue - minimumValue) * deltaLocation / Double(bounds.width - bounds.height)
        }
        previouslocation = location
        // if both are highlighted. we need to decide which direction to drag
        if lowerThumbLayer.highlighted && upperThumbLayer.highlighted {
            if deltaLocation > 0 {
                // left to right
                upperValue = boundValue(upperValue + deltaValue, toLowerValue: lowerValue + gapBetweenThumbs, upperValue: maximumValue)
            }
            else {
                // right to left
                lowerValue = boundValue(lowerValue + deltaValue, toLowerValue: minimumValue, upperValue: upperValue - gapBetweenThumbs)
            }
        } else {
            // Update the values
            if lowerThumbLayer.highlighted {
                lowerValue = boundValue(lowerValue + deltaValue, toLowerValue: minimumValue, upperValue: upperValue - gapBetweenThumbs)
            } else if upperThumbLayer.highlighted {
                upperValue = boundValue(upperValue + deltaValue, toLowerValue: lowerValue + gapBetweenThumbs, upperValue: maximumValue)
            }
        }
        // only send changed value if stepValue is not set. We will trigger this later in endTracking
        if stepValue == nil {
            sendActions(for: .valueChanged)
        }
        return true
    }
    
    /// end touch tracking. Unhighlight the two thumbs
    override open func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        lowerThumbLayer.highlighted = false
        upperThumbLayer.highlighted = false
        // let slider snap after user stop dragging
        if let stepValue = stepValue {
            lowerValue = Darwin.round(lowerValue / stepValue) * stepValue
            upperValue = Darwin.round(upperValue / stepValue) * stepValue
            sendActions(for: .valueChanged)
        }
    }
}
