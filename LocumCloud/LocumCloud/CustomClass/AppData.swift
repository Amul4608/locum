//
//  AppDate.swift
//  

import UIKit
import SVProgressHUD
import SDWebImage
import ObjectMapper


var appUser : UserModel?

final class AppData: NSObject {
    
    
    static let shared = AppData()
    
    class func showProgress(){
        SVProgressHUD.show()
        
    }
    
    class func hideProgress(){
        SVProgressHUD.dismiss()
    }
    
    class func drawBlackBorder(view: UIView) {
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = 2.5
        view.layer.cornerRadius = view.frame.size.width/2.0
        view.backgroundColor = UIColor.brown
        
    }
    
    
    
    //==========================================
    //MARK: - Helper Methods
    //==========================================
    
    func saveModel(_ model : AnyObject, forKey key : String) {
        
        let encodedObject = NSKeyedArchiver.archivedData(withRootObject: model)
        UserDefaults.standard.set(encodedObject, forKey: key)
        UserDefaults.standard.synchronize()
        
    }
    
    func getModelForKey(_ key : String) -> AnyObject? {
        
        let encodedObject = UserDefaults.standard.object(forKey: key) as? Data
        let savedModel = encodedObject != nil ? NSKeyedUnarchiver.unarchiveObject(with: encodedObject!) : nil
        return savedModel as AnyObject?
    }
    
    func removeModelForKey(_ key : String) {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
        // appUser = nil
        
    }
}

 extension AppData {
 
 func saveUserInfo(_ userInfo : [String : Any]) {
 print(userInfo)
 if let user = Mapper<UserModel>().map(JSON: userInfo) {
 
 saveModel(user, forKey: UserDefaultsKey.user)
 }
 }
 
 func getUserInfo() -> UserModel? {
 return getModelForKey(UserDefaultsKey.user) as? UserModel
 }
 
 }
 
