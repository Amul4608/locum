//
//  Utilities.swift
//

import UIKit
import MobileCoreServices
import CoreLocation
import AVFoundation

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


let kUserDetail = "UserDetail";
let kUserSearchSettings = "searchSettings";
let kAccessToken = "accessToken"
private let conversionValForCmToInch = 0.393701;
let dateTimeFormat = "yyyy-MM-dd"
let StringdateTimeFormat = "dd-MM-yyyy"

enum AlertType : Int {
    
    case success = 0
    case failure = 1
    case internet_ERROR = 2
}

class Utilities: NSObject
{
    
    
    //=======================================================
    //MARK: - To Save/Retrieve Objects to/from NSUserDefaults
    //=======================================================
    
    class func saveObjectToUserDefaults(_ value:AnyObject?, forKey key:String)
    {
        if value != nil
        {
            UserDefaults.standard.set(value, forKey: key);
            UserDefaults.standard.synchronize();
        }
    }
    
    
    class func getObjectFromUserDefaultsForKey(_ key:String) -> AnyObject?
    {
        return UserDefaults.standard.object(forKey: key) as AnyObject?;
    }
    
    
    class func removeObjectFromUserDefaultsForKey(_ key:String)
    {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize();
    }
    
    
    class func queryString (_ dic : NSDictionary) -> String{
        
        var queryString : String = ""
        var i = 0;
        
        for (key,value) in dic {
            
            if (i == 0){
                queryString += "\(key)=\(value)"
            }else{
                queryString += "&\(key)=\(value)"
            }
            i += 1
        }
        return queryString
    }
    
    class func classNameAsString(_ obj: Any) -> String {
        
        //prints more readable results for dictionaries, arrays, Int, etc
        return String(describing: type(of: (obj) as AnyObject)).components(separatedBy: "__").last!
    }
    
    class func stringFromAny(_ value:Any?) -> String {
        if let nonNil = value, !(nonNil is NSNull) {
            return String(describing: nonNil)
        }
        return ""
    }
    
    
    class func readFile(_ fileURL : URL) -> Data?
    {
        do
        {
            let strData = try Data(contentsOf: fileURL)
            return strData;
        }catch
        {
            //print("Error in file reading \(fileURL)")
        }
        
        return nil;
    }
    
    class func makeJsonData(_ object:AnyObject) -> Data?
    {
        do
        {
            let data = try JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions())
            return data;
        }catch
        {
            print("can not make json of \(object)");
        }
        
        return nil;
    }
    
    
    class func convertDateString(_ dateStr:String?, fromDateFormat currentFormat:String, toDateFormat newFormat:String) -> String?
    {
        if dateStr == nil
        {
            return nil;
        }
        
        let formatter:DateFormatter = DateFormatter();
        formatter.dateFormat = currentFormat;
        let date = formatter.date(from: dateStr!);
        if date == nil
        {
            return "";
        }
        formatter.dateFormat = newFormat;
        let newStr : String = formatter.string(from: date!);
        return newStr;
    }
    
    
    
    
    class func convertGMTStringToDate(_ strDate:String, dateFormat:String) -> Date?
    {
        let formatter:DateFormatter = DateFormatter();
        formatter.dateFormat = dateFormat;
        
        formatter.timeZone = TimeZone(identifier: "GMT-2")
        let date = formatter.date(from: strDate);
        return date;
    }
    
    
    class func convertUTCDateToString(_ date:Date, dateFormat:String) -> String?
    {
        let formatter:DateFormatter = DateFormatter();
        formatter.dateFormat = dateFormat;
        formatter.timeZone = TimeZone(identifier: "UTC")
        let str = formatter.string(from: date);
        return str;
    }
    class func convertCMToInch(_ inch : Float) -> Float{
        return  Float(conversionValForCmToInch) * Float(inch);
    }
    
    class func getDayOfWeek(_ date:Date)-> String {
        
        let formatter:DateFormatter = DateFormatter();
        formatter.dateFormat = "EEEE";
        formatter.timeZone = TimeZone.autoupdatingCurrent
        let str = formatter.string(from: date);
        return str;
    }
    
    class func floatingValueOfTime(_ strTime : String) -> Float{
        let arr = strTime.components(separatedBy: ":") as NSArray;
        let hour = Float(arr.object(at: 0) as! String)!;
        let minute = Float(arr.object(at: 1) as! String)!;
        
        let time = hour + (minute / Float(60))
        return time
    }
    
    class func localize(_ string:String) -> String
    {
        return Utilities.localize(string, fileName: nil)
    }
    
    class func localize(_ string:String,fileName file:String?) -> String
    {
        if file == nil
        {
            return NSLocalizedString(string, comment: "")
        }
        else
        {
            return NSLocalizedString(string, tableName: file!, bundle: Bundle.main, value:"", comment: "")
        }
    }
    
    class func doubleRoundNumber(_ number : Double, roundTo : Int) -> Double{
        let division = pow(Double(10),Double(roundTo))
        let num1 = Int(division * number);
        let num2 = Double(Double(num1) / division);
        
        return num2;
    }
    
    class func floatRoundNumber(_ number : Float, roundTo : Int) -> Float{
        let division = pow(Double(10),Double(roundTo))
        let num1 = Int(division * Double(number));
        let num2 = Float(Double(num1) / division);
        
        return num2;
    }
    class func intRoundNumber(_ number : Float) -> Int{
        let division = pow(Double(10),Double(0))
        let num1 = Int(division * Double(number));
        let num2 = Int(Double(num1) / division);
        
        return num2;
    }
    
    
    
    class func timeIntervalInString(_ dateTime1 : Date,dateTime2 : Date) -> String{
        var timeInterval = dateTime1.timeIntervalSince(dateTime2)
        
        if timeInterval < 0 {
            timeInterval = -timeInterval;
        }
        timeInterval -= (2 * 60 * 60);
        
        if timeInterval < 0 {
            timeInterval = 0;
        }
        
        let totalMinute = Int(timeInterval / 60);
        let hours = totalMinute / 60;
        let minute = totalMinute - (hours * 60);
        
        let str = "\(hours):\(minute)";
        return str;
        
    }
    //==========================================
    //MARK: - To Convert Date To String
    //==========================================
    
    class func convertDateToStringWithFormat(_ date:Date, format:String)->String?
    {
        let formatter:DateFormatter = DateFormatter();
        formatter.dateFormat = format;
        formatter.timeZone = TimeZone.autoupdatingCurrent
        let str = formatter.string(from: date);
        return str;
    }
    class func changeDate(_ date : String, fromDateFormat : String , toDateFormat : String) -> String?{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromDateFormat
        
        let yourDate = dateFormatter.date(from: date)
        
        dateFormatter.dateFormat =  toDateFormat  //"dd MMM,yyyy"
        return dateFormatter.string(from: yourDate!)
        
    }
    
    
    
    class func convertStringToDate(_ strDate:String, dateFormat:String) -> Date?
    {
        let formatter:DateFormatter = DateFormatter();
        formatter.dateFormat = dateFormat;
        // formatter.timeZone = TimeZone.autoupdatingCurrent
        let date = formatter.date(from: strDate);
        
        return date;
    }
    
    
    class func changeDateFormatTo12Hour(_ time : String?) -> String {
        
        if(time != nil){
            
            let dateFormatter  :DateFormatter = DateFormatter()
            
            //24 Hour Date Format
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let time  = dateFormatter.date(from: time!)
            
            //Set 12 Hour Format
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
            
            let  timeToReturn = dateFormatter.string(from: time!)
            
            //print("12 Hour Format: \(timeToReturn)")
            return timeToReturn
        }
        return ""
    }
    
    class func changeTimeFormatTo12Hour(_ time : String?) -> String {
        
        if(time != nil){
            
            let dateFormatter  :DateFormatter = DateFormatter()
            
            //24 Hour Date Format
            dateFormatter.dateFormat = "HH:mm:ss"
            
            let time  = dateFormatter.date(from: time!)
            
            //Set 12 Hour Format
            dateFormatter.dateFormat = "hh:mm a"
            
            let  timeToReturn = dateFormatter.string(from: time!)
            
            //print("12 Hour Format: \(timeToReturn)")
            return timeToReturn
        }
        return ""
    }
    class func hmsFrom(seconds: Int, completion: @escaping ( _ hours: Int, _ minutes: Int, _ seconds: Int)->()) {
        
        completion(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
        
    }
    class func hmsdFrom(seconds: Int, completion: @escaping ( _ days:Int ,_ hours: Int, _ minutes: Int, _ seconds: Int)->()) {
        
        completion(seconds/86400 , (seconds % 86400) / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
        
    }
    class func secondToDayHoursMimutes(_ Seconds: Int) -> String {
        let sec = Seconds
        
        if sec <= 0
        {
            
            return ""
        }
        
        let day = sec/86400
        let hou = (sec % 86400) / 3600
        let min = (sec % 3600) / 60
        //let secnd = (sec % 3600) % 60
        var time:String = ""
        if day > 0
        {
            time = "\(day)d \(hou):\(min) H"
            // time = String(format:"%d %2d:%2d:%2d",day)d \(hou):\(min):\(secnd) H")
        }
        else if hou > 0
        {
            time = "\(hou):\(min) H"
        }
        else if min > 0
        {
            time =  "\(min) M"
        }
        else
        {
            time = ""
        }
        return time
    }
    class func secondToDayHoursMimutesSecond(_ Seconds: Int) -> String {
        let sec = Seconds
        
        if sec <= 0
        {
            
            return ""
        }
        
        let day = sec/86400
        let hou = (sec % 86400) / 3600
        let min = (sec % 3600) / 60
        let secnd = (sec % 3600) % 60
        var time:String = ""
        if day > 0
        {
            time = "\(day)d \(hou):\(min):\(secnd) H"
            // time = String(format:"%d %2d:%2d:%2d",day)d \(hou):\(min):\(secnd) H")
        }
        else if hou > 0
        {
            time = "\(hou):\(min):\(secnd) H"
        }
        else if min > 0
        {
            time =  "\(min):\(secnd) M"
        }
        else if secnd > 1
        {
            time = "\(secnd) S"
        }
        else
        {
            time = ""
        }
        return time
    }
    class func secondToMimutes(_ Seconds: Int) -> String {
        let sec = Seconds
        
        let day = sec/86400
        let hou = (sec % 86400) / 3600
        let min = (sec % 3600) / 60
        let secnd = (sec % 3600) % 60
        var time:String = ""
        time = "\(min)"
        return time
    }
    
    //    class func secondToDayHoursMimutesSecond(_ Seconds: Int) -> String {
    //        let sec = Seconds
    //
    //        let day = sec/86400
    //        let hou = (sec % 86400) / 3600
    //        let min = (sec % 3600) / 60
    //        let secnd = (sec % 3600) % 60
    //
    //        return "\(day)d \(hou):\(min) h"
    //    }
    class func changeTimeFormatTo24Hour(_ time: String) -> String {
        
        let dateFormatter  :DateFormatter = DateFormatter()
        
        //12 Hour Date Format
        dateFormatter.dateFormat = "hh:mm a"
        
        let time  = dateFormatter.date(from: time)
        
        //Set 24 Hour Format
        dateFormatter.dateFormat = "HH:mm:ss"
        
        let  timeToReturn = dateFormatter.string(from: time!)
        
        //print("24 Hour Format: \(timeToReturn)")
        return timeToReturn
        
    }
    
    
    class func convertUTCStringToDate(_ strDate:String, dateFormat:String) -> Date?
    {
        let formatter:DateFormatter = DateFormatter();
        formatter.dateFormat = dateFormat;
        
        formatter.timeZone = TimeZone(abbreviation: "GMT+02:00")
        
        let date = formatter.date(from: strDate);
        return date;
    }
    
    class func convertStringToDate(_ strDate : String, dateForamt : String) -> Date
    {
        
        let formatter:DateFormatter = DateFormatter();
        formatter.dateFormat = dateForamt;
        formatter.timeZone = TimeZone(abbreviation: "GMT") //NSTimeZone.defaultTimeZone()
        
        let myDate = formatter.date(from: strDate);
        return myDate!
        
    }
    
    
    class func UTCToLocal(UTCDateString: String ,dateFormat:String ) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat //Input Format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let UTCDate = dateFormatter.date(from: UTCDateString)
        dateFormatter.dateFormat = dateFormat // Output Format
        dateFormatter.timeZone = TimeZone.current
        let UTCToCurrentFormat = dateFormatter.string(from: UTCDate!)
        return UTCToCurrentFormat
    }
    class func UTCToLocalDate(UTCDateString: String ,dateFormat:String ) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat //Input Format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let UTCDate = dateFormatter.date(from: UTCDateString)
        dateFormatter.dateFormat = dateFormat // Output Format
        dateFormatter.timeZone = TimeZone.current
        let str = dateFormatter.string(from: UTCDate!);
        let UTCToCurrentFormat = dateFormatter.date(from: str)
        return UTCToCurrentFormat!
    }
    class func localToUTC(date:String ,dateFormat:String ) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        //dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = dateFormat
        
        return dateFormatter.string(from: dt!)
    }
    
    
    class func convertLocalStringToUTCDate(_ date:Date, dateFormat:String) -> String?
    {
        let formatter:DateFormatter = DateFormatter();
        formatter.dateFormat = dateFormat;
        formatter.timeZone = TimeZone(abbreviation: "GMT+02:00")
        
        let str = formatter.string(from: date);
        return str;
    }
    
    class func convertDateToString(_ date:Date, dateFormat:String) -> String?
    {
        let formatter:DateFormatter = DateFormatter();
        formatter.dateFormat = dateFormat;
        formatter.timeZone = TimeZone.current
        print(date)
        let str = formatter.string(from: date);
        // let dtr = daySuffix(from: date)
        
        return str;
    }
    class func daySuffix(from date: Date) -> String {
        let calendar = Calendar.current
        let dayOfMonth = calendar.component(.day, from: date)
        print(dayOfMonth)
        switch dayOfMonth {
        case 1, 21, 31: return "st"
        case 2, 22: return "nd"
        case 3, 23: return "rd"
        default: return "th"
        }
    }
    
    class func getDateTimeInLocalTimezone(_ date : String, time : String,fromDateFormat : String , toDateFormat : String) -> (String?, String?){
        
        
        let dateTime = "\(date) \(time)"
        
        let localDate = Utilities.convertUTCStringToDate(dateTime, dateFormat: "yyyy-MM-dd HH:mm:ss")!
        let localDateTime = Utilities.convertDateToStringWithFormat(localDate, format: "yyyy-MM-dd HH:mm:ss")!
        
        let date = localDateTime.components(separatedBy: " ").first! as String
        let time = localDateTime.components(separatedBy: " ").last! as String
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromDateFormat
        
        let yourDate = dateFormatter.date(from: date)
        
        dateFormatter.dateFormat =  toDateFormat  //"dd MMM,yyyy"
        let strDate = dateFormatter.string(from: yourDate!)
        let strTime = Utilities.changeTimeFormatTo12Hour(time)
        
        return(strDate, strTime)
        
    }
    
    class func getDateTimeInUTCTimezone(_ date : String, time : String,fromDateFormat : String , toDateFormat : String) -> (String?, String?){
        
        let dateTime = "\(date) \(time)"
        let localDate = Utilities.convertStringToDate(dateTime, dateForamt: "yyyy-MM-dd HH:mm:ss")
        let strUTCDate = Utilities.convertLocalStringToUTCDate(localDate, dateFormat: "yyyy-MM-dd HH:mm:ss")
        
        let date = strUTCDate!.components(separatedBy: " ").first! as String
        let time = strUTCDate!.components(separatedBy: " ").last! as String
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromDateFormat
        
        let yourDate = dateFormatter.date(from: date)
        
        dateFormatter.dateFormat =  toDateFormat  //"dd MMM,yyyy"
        let strDate = dateFormatter.string(from: yourDate!)
        let strTime = Utilities.changeTimeFormatTo12Hour(time)
        
        return(strDate, strTime)
    }
    
    
    class func geocodeLocationWithlocation(_ location:CLLocation?, withCompletionHandler handler:@escaping ((_ status:Bool,_ address:String?,_ message:String?)->Void))
    {
        if location == nil
        {
            handler(false, nil, "Not able to get User Location, Please provide location access permission to app");
        }
        else if location!.coordinate.latitude == 0.0 && location!.coordinate.longitude == 0.0
        {
            handler(false, nil, "Not able to get User Location, Please provide location access permission to app");
        }
        else
        {
            let geocoder = CLGeocoder();
            
            geocoder.reverseGeocodeLocation(location!, completionHandler: { (placemarks, error) -> Void in
                
                if error != nil
                {
                    handler(false, nil, error?.localizedDescription);
                }
                else
                {
                    if placemarks != nil && placemarks?.count>0
                    {
                        let placeMark:CLPlacemark = placemarks![0];
                        
                        var address = "";
                        
                        if placeMark.name != nil
                        {
                            address = "\((placeMark.name)!)"
                        }
                        if placeMark.locality != nil
                        {
                            address = "\(address),\((placeMark.locality)!)"
                        }
                        if placeMark.subAdministrativeArea != nil
                        {
                            address = "\(address),\((placeMark.subAdministrativeArea)!)"
                        }
                        if placeMark.administrativeArea != nil
                        {
                            address = "\(address),\((placeMark.administrativeArea)!)"
                        }
                        handler(true, address, nil);
                    }
                    else
                    {
                        handler(true, " ", nil);
                    }
                }
            })
        }
    }
    
    class func getCategoryListInString(_ dicHotelDetail : NSMutableDictionary) -> String{
        
        var category = "";
        
        let arrCategory = dicHotelDetail.object(forKey: "categories") as? NSArray;
        var i = 0
        for item in arrCategory!{
            
            let categoryItem = item as? NSArray
            
            if categoryItem?.count > 0 {
                if i == 0 {
                    category = "\(categoryItem!.object(at: 0))"
                }
                else{
                    category = "\(category),\(categoryItem!.object(at: 0))"
                }
                
            }
            i += 1;
        }
        
        return category
    }
    class func ThungNail(_ url : URL) -> UIImage?{
        do {
                      let asset = AVURLAsset(url: url , options: nil)
                      let imgGenerator = AVAssetImageGenerator(asset: asset)
                      imgGenerator.appliesPreferredTrackTransform = true
                   let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
                    let   thumbnail = UIImage(cgImage: cgImage)
                      
                    // btnVideo.setImage(thumbnail, for: .normal)
                  return thumbnail
                      
                      
                  } catch let error {
                      print("*** Error generating thumbnail: \(error.localizedDescription)")
                  }
        return nil
    }
    class func makeMutableArray(_ arrObj : NSArray?) -> NSMutableArray{
        
        if arrObj == nil {
            return NSMutableArray()
        }
        
        let newObj = NSMutableArray()
        
        for i in 0..<arrObj!.count{
            
            let obj = arrObj!.object(at: i);
            
            if  obj is NSArray{
                newObj.add(Utilities.makeMutableArray(obj as? NSArray))
            }
            else if obj is NSDictionary{
                newObj.add(Utilities.makeMutableDictionary(obj as? NSDictionary))
            }
            else{
                newObj.add(obj)
            }
        }
        
        return newObj
    }
    
    class func makeMutableDictionary ( _ dicObj : NSDictionary?) -> NSMutableDictionary{
        
        if dicObj == nil {
            return NSMutableDictionary()
        }
        
        let newObj = NSMutableDictionary()
        let allKeys = dicObj!.allKeys as NSArray;
        
        for i in 0..<allKeys.count{
            let key = allKeys.object(at: i) as! String;
            let obj = dicObj!.object(forKey: key);
            
            if  obj is NSArray {
                
                newObj.setObject(Utilities.makeMutableArray(obj as? NSArray), forKey: key as NSCopying)
            }
            else if obj is NSDictionary{
                newObj.setObject(Utilities.makeMutableDictionary(obj as? NSDictionary), forKey: key as NSCopying)
            }
            else{
                newObj.setObject(obj ?? "", forKey: key as NSCopying)
            }
        }
        
        return newObj
    }
    
    
    class func makeJsonFromObject(_ obj : AnyObject)
    {
        
    }
    
    class func needTestingLocation(_ location : CLLocation) -> Bool{
        let latitude = Utilities.floatRoundNumber(Float(location.coordinate.latitude), roundTo: 0)
        let longitude = Utilities.floatRoundNumber(Float(location.coordinate.longitude), roundTo: 0)
        
        if latitude == 23 && longitude == 72 {
            return true;
        }
        return false;
    }
    
    class func getHeightForTextView(_ textView: UITextView, forText strText: String) -> CGFloat {
        let temp: UITextView = UITextView(frame: textView.frame)
        temp.font = textView.font
        temp.text = strText
        let fixedWidth: CGFloat = temp.frame.size.width
        let newSize: CGSize = temp.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat(MAXFLOAT)))
        return (newSize.height)
    }
    class func getHeightForLabel(_ label: UILabel) -> CGFloat {
        let temp: UILabel = UILabel(frame: label.frame)
        temp.font = label.font
        temp.text = label.text
        let fixedWidth: CGFloat = temp.frame.size.width
        let newSize: CGSize = temp.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat(MAXFLOAT)))
        return (newSize.height)
    }
    
    
    
    class func randomStringWithLength (_ len : Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for _ in 0..<len{
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString
    }
    
    class func randomNumberWithLength (_ len : Int) -> NSString {
        
        let letters : NSString = "123456789"
        
        let randomString : NSMutableString = NSMutableString(capacity: len)
        
        for _ in 0..<len{
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString
    }
    
    
    
    
    class func getTimeFromDateTime(_ strDate : String, strTime : String) -> String {
        let strDateTime = "\(strDate) \(strTime)"
        let date = Utilities.convertStringToDate(strDateTime, dateFormat: dateTimeFormat)
        let time = Utilities.convertDateToString(date!, dateFormat: "HH:mm")
        return time!;
    }
    
    class func getDayFromDateTime(_ strDate : String, strTime : String) -> String {
        let strDateTime = "\(strDate) \(strTime)"
        let date = Utilities.convertStringToDate(strDateTime, dateFormat: dateTimeFormat)
        var time = Utilities.convertDateToString(date!, dateFormat: "EEE")
        if time?.count > 3 {
            time = time!.substring(to: time!.index(time!.startIndex, offsetBy: 3))
        }
        return time!;
    }
    class func getMonthFromDate(_ strDate : String) -> String {
        let strDateTime = "\(strDate)"
        let date = Utilities.convertStringToDate(strDateTime, dateFormat: dateTimeFormat)
        var time = Utilities.convertDateToString(date!, dateFormat: "MMM")
        if time?.count > 3 {
            time = time!.substring(to: time!.index(time!.startIndex, offsetBy: 3))
        }
        return time!;
    }
    class func getFullMonthFromDate(_ strDate : String , DateFormet:String) -> String {
        let strDateTime = "\(strDate)"
        // print(strDate)
        var dtFormat = StringdateTimeFormat
        if DateFormet != ""{
            dtFormat = DateFormet
        }
        let date = Utilities.convertStringToDate(strDateTime, dateFormat: dtFormat)
        let time = Utilities.convertDateToString(date!, dateFormat: "MMMM")
        
        return time!;
    }
    class func getYearFromDate(_ strDate : String) -> String {
        let strDateTime = "\(strDate)"
        print(strDate)
        let date = Utilities.convertStringToDate(strDateTime, dateFormat: StringdateTimeFormat)
        let time = Utilities.convertDateToString(date!, dateFormat: "yyyy")
        return time!;
    }
    class func getYearFromDate2(_ strDate : String) -> String {
        let strDateTime = "\(strDate)"
        print(strDate)
        let date = Utilities.convertStringToDate(strDateTime, dateFormat: dateTimeFormat)
        let time = Utilities.convertDateToString(date!, dateFormat: "yyyy")
        return time!;
    }
    class func getDateFromDate(_ strDate : String) -> String {
        let strDateTime = "\(strDate)"
        print(strDate)
        let date = Utilities.convertStringToDate(strDateTime, dateFormat: StringdateTimeFormat)
        let time = Utilities.convertDateToString(date!, dateFormat: "dd")
        return time!;
    }
    class func getDateFromDate2(_ strDate : String) -> String {
        let strDateTime = "\(strDate)"
        print(strDate)
        let date = Utilities.convertStringToDate(strDateTime, dateFormat:dateTimeFormat)
        let time = Utilities.convertDateToString(date!, dateFormat: "dd")
        return time!;
    }
    class func getFullMonthFromDate2(_ strDate : String) -> String {
        let strDateTime = "\(strDate)"
        print(strDate)
        let date = Utilities.convertStringToDate(strDateTime, dateFormat: dateTimeFormat)
        let time = Utilities.convertDateToString(date!, dateFormat: "MMMM")
        
        return time!;
    }
    
    
    class func getMonthFromDateTime(_ strDate : String, strTime : String) -> String {
        let strDateTime = "\(strDate) \(strTime)"
        let date = Utilities.convertStringToDate(strDateTime, dateFormat: dateTimeFormat)
        var time = Utilities.convertDateToString(date!, dateFormat: "MMM")
        if time?.count > 3 {
            time = time!.substring(to: time!.index(time!.startIndex, offsetBy: 3))
        }
        return time!;
    }
    
    class func getDateFromDateTime(_ strDate : String, strTime : String) -> String {
        let strDateTime = "\(strDate) \(strTime)"
        let date = Utilities.convertStringToDate(strDateTime, dateFormat: dateTimeFormat)
        let time = Utilities.convertDateToString(date!, dateFormat: "dd")
        return time!;
    }
    class func getRealDateFromDateTime(_ strDate : String, strTime : String) -> Date {
        let strDateTime = "\(strDate) \(strTime)"
        let date = Utilities.convertUTCStringToDate(strDateTime, dateFormat: dateTimeFormat)
        return date!;
    }
   
    
    class func scaleObject(_ view:UIView,scale : CGSize){
        
        UIView.animate(withDuration: 0.1, animations: {
            view.transform = CGAffineTransform( scaleX: scale.width, y: scale.height);
        })
    }
    
    class func makeBounceEffect(_ view : UIView,completion: @escaping () -> Void) {
        
        view.transform = CGAffineTransform( scaleX: 0.9, y: 0.9);
        
        UIView.animate(withDuration: 0.25, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 1, options: UIView.AnimationOptions.allowUserInteraction, animations: { () -> Void in
            
            view.transform = CGAffineTransform.identity
            
        }) { (flag) -> Void in
            completion()
        }
    }
    
    class func moveObjectFirst(_ index : NSInteger, arr : NSMutableArray )-> NSMutableArray {
        let newArr = NSMutableArray(array: arr)
        let obj = newArr.object(at: index)
        newArr.removeObject(at: index)
        newArr.insert(obj, at: 0)
        return newArr;
    }
    
    class func copyAndAppendArray(_ numberOfTime : NSInteger, arr : NSMutableArray )-> NSMutableArray {
        let newArr = NSMutableArray(array: arr)
        
        for _ in 0..<numberOfTime{
            for dic in arr {
                newArr.add(dic);
            }
        }
        
        return newArr;
    }
    
    class func getViewControllerFromStoryboard(_ storyboard : String,identifire : String) -> UIViewController{
        let storyBoard = UIStoryboard(name: storyboard, bundle: nil);
        return storyBoard.instantiateViewController(withIdentifier: identifire);
    }
    class func getDataFrom(url : URL?) -> Data?{
        var data : Data?
        
        if url == nil {
            return nil
        }
        
        do {
            data = try Data(contentsOf: url!)
        }
        catch {
            print("error in fatching data")
        }
        
        return data
        
    }
    class func castStringFromArrayOfDict(arrCurrent : NSArray?, key : String, atIndex : NSInteger) -> String? {
        
        let str = (arrCurrent?.object(at: atIndex) as? NSDictionary)?.value(forKey: key) as? String
        return str
    }
    
    class func castDictionaryFromArrayOfDict(arrCurrent : NSArray?, atIndex : NSInteger) -> NSDictionary? {
        
        let dict = arrCurrent?.object(at: atIndex) as? NSDictionary
        return dict
    }
    
    class func SetupMaskBorer(_ setViewName:UIView , color:UIColor , radius:CGFloat , mask:Bool)
    {
        setViewName.layer.shadowOpacity = 1
        setViewName.layer.shadowRadius = radius
        setViewName.layer.shadowColor = color.cgColor
        setViewName.layer.masksToBounds = mask
    }
    
    class func setCircle( _ Name:UIView , cornerRadius:CGFloat)
    {
        Name.layer.cornerRadius = cornerRadius
        Name.clipsToBounds = true
    }
    
    class func setupMask_And_Corner(_ setViewName:UIView , color:UIColor , radius:CGFloat , mask:Bool , cornerRadius:CGFloat)
    {
        setViewName.layer.shadowOpacity = 1
        setViewName.layer.shadowRadius = radius
        setViewName.layer.cornerRadius = cornerRadius
        setViewName.layer.shadowColor = color.cgColor
        setViewName.layer.masksToBounds = mask
    }
    class func setupMask_And_Corner_And_Border(_ setViewName:UIView , color:UIColor , radius:CGFloat , mask:Bool , cornerRadius:CGFloat , borderColor:UIColor , borderWidth:CGFloat)
    {
        setViewName.layer.shadowOpacity = 1
        setViewName.layer.shadowRadius = radius
        setViewName.layer.cornerRadius = cornerRadius
        setViewName.layer.shadowColor = color.cgColor
        setViewName.layer.borderWidth = borderWidth
        setViewName.layer.borderColor = borderColor.cgColor
        setViewName.layer.masksToBounds = mask
    }
    
    class func resizeImage(_ image: UIImage, newHeight: CGFloat) -> UIImage {
        let scale = newHeight / image.size.height
        let newWidth = image.size.width * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = newImage?.jpegData(compressionQuality: 0.5)
        UIGraphicsEndImageContext()
        return UIImage(data:imageData!)!
    }
    
    class func lineCount(forText text: String? ) -> Int {
        let font = UIFont.systemFont(ofSize: 15.0)
        let width = 180
        let rect: CGRect? = text?.boundingRect(with: CGSize(width: CGFloat(width), height: CGFloat(MAXFLOAT)), options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return Int(ceil((rect?.size.height ?? 0.0) / font.lineHeight))
    }
    
    class func printParameter(_ parameter : [String : Any]?, _ title : String = "Parameter") {
        if parameter == nil {
            return
        }
        
        print("\n\n\n\n-----------------------\(title)----------------------------\n")
        for (key,value) in parameter! {
            print("\(key):\(value)")
        }
        print("\n-----------------------*********----------------------------\n")
    }
    
    class func getHTMLToString(_ resource:String) -> String {
        var html = ""
        
        
        if let htmlPathURL = Bundle.main.url(forResource: resource, withExtension: "html"){
            do {
                html = try String(contentsOf: htmlPathURL, encoding: .utf8)
            } catch  {
                print("Unable to get the file.")
            }
        }
        
        return html
    }
    
    class func lines(label: UILabel) -> Int {
        let textSize = CGSize(width: label.frame.size.width, height: CGFloat(Float.infinity))
        let rHeight = lroundf(Float(label.sizeThatFits(textSize).height))
        let charSize = lroundf(Float(label.font.lineHeight))
        let lineCount = rHeight/charSize
        return lineCount
    }
    class func addTextFieldpadding(txtField : UITextField,paddingLeft : CGFloat,paddingRight : CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: paddingLeft, height: txtField.frame.size.height))
        txtField.leftView = paddingView
        txtField.leftViewMode = .always
        
        let paddingView1 = UIView(frame: CGRect(x: 0, y: 0, width: paddingRight, height: txtField.frame.size.height))
        txtField.rightView = paddingView1
        txtField.rightViewMode = .always
    }
    class func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    class func getDateFromString(format:String,strDate:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: strDate)!
    }
    
    class func getStringFromDate(format:String,date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.string(from: date)
    }
    class func convertTimeToMinute(strTime:String) -> Int{
        let arrTime  = strTime.components(separatedBy: ":")
        
        let strHr    = arrTime[0]
        let strMM = arrTime[1]
        
        if  strHr == "00" {
            return  Int(strMM)!
        }else{
            return Int(strHr)! * 60 + Int(strMM)!
        }
    }
    class func convertMinuteToTime(intTime:Int) -> Int{
        
        return intTime /  60
    }
    
    // MARK: - Toast Methods
    class func toastView(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: UIApplication.shared.keyWindow!.frame.size.width/2 - 150, y: (UIApplication.shared.keyWindow?.frame.size.height)!-120, width:300,  height : 50))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = NSTextAlignment.center;
        //appDelegate.window!.addSubview(toastLabel)
        UIApplication.shared.keyWindow?.addSubview(toastLabel)
        toastLabel.text = message
        toastLabel.numberOfLines = 0
        toastLabel.alpha = 1.0
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 5.0)
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        UIView.animate(withDuration: 6, delay: 0.1, options: UIView.AnimationOptions.curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        })
    }
    
    // MARK: - Trim String
    class func trimString(string : NSString) -> NSString {
        return string.trimmingCharacters(in: NSCharacterSet.whitespaces) as NSString
    }
    
    
    // MARK:- Alert View
    class func showAlertView(_ strAlertTitle : String, strAlertMessage : String, okTitle : String?,vc: UIViewController, okCompletion: ((UIAlertAction) -> Void)?) {
        let alertController = UIAlertController(title: strAlertTitle, message: strAlertMessage, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: okTitle != nil ? okTitle: "Ok", style: UIAlertAction.Style.default, handler: okCompletion)
        // Create the actions
        if (okCompletion != nil) {
            alertController.addAction(OKAction)
        }
        
        // Present the controller
        vc.present(alertController, animated: true, completion: nil)
    }
 
}


extension Int
{
    public static func random(min: Int, max: Int) -> Int {
        assert(min < max)
        return Int(arc4random_uniform(UInt32(max - min + 1))) + min
    }
    
}


